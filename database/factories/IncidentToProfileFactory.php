<?php
/**
 * IncidentToProfileFactory
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.06.2021
 * Time: 16:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Database\Factories;


use App\Models\Incident;
use App\Models\IncidentToProfile;
use App\Models\Profile;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class IncidentToProfileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = IncidentToProfile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            IncidentToProfile::COLUMN_INCIDENT_ID => Incident::factory(),
            IncidentToProfile::COLUMN_PROFILE_ID  => Profile::factory(),
            IncidentToProfile::COLUMN_WARRANT     => false,
        ];
    }
}
