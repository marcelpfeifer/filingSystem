<?php
/**
 * ChargeFactory
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.06.2021
 * Time: 15:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Database\Factories;


use App\Models\Charge;
use App\Models\ChargeGroup;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ChargeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Charge::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            Charge::COLUMN_NAME            => Str::random(20),
            Charge::COLUMN_DESCRIPTION     => Str::random(100),
            Charge::COLUMN_CHARGE_GROUP_ID => ChargeGroup::factory(),
            Charge::COLUMN_PRIORITY        => 0,
            Charge::COLUMN_TIME            => rand(0, 100),
            Charge::COLUMN_MONEY           => rand(0, 1000),
        ];
    }
}
