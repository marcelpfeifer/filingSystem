<?php
/**
 * ListEntryFactory
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.09.2021
 * Time: 11:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Database\Factories;


use App\Models\ListEntry;
use App\Models\ListModel;
use Illuminate\Database\Eloquent\Factories\Factory;

class ListEntryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ListEntry::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            ListEntry::COLUMN_LIST_ID       => ListModel::factory(),
        ];
    }
}
