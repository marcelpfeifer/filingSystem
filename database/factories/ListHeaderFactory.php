<?php
/**
 * ListHeaderFactory
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.09.2021
 * Time: 11:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Database\Factories;


use App\Models\Group;
use App\Models\ListHeader;
use App\Models\ListModel;
use App\Models\ListToGroup;
use Illuminate\Database\Eloquent\Factories\Factory;

class ListHeaderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ListHeader::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            ListHeader::COLUMN_NAME    => $this->faker->sentence,
            ListHeader::COLUMN_LIST_ID => ListModel::factory(),
            ListHeader::COLUMN_LIST    => $this->faker->boolean,
            ListHeader::COLUMN_ORDER   => $this->faker->numberBetween(0, 20),
        ];
    }
}
