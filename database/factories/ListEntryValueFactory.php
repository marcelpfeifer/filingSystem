<?php
/**
 * ListEntryValueFactory
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.09.2021
 * Time: 11:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Database\Factories;


use App\Models\ListEntry;
use App\Models\ListEntryValue;
use App\Models\ListHeader;
use Illuminate\Database\Eloquent\Factories\Factory;

class ListEntryValueFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ListEntryValue::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            ListEntryValue::COLUMN_LIST_ENTRY_ID  => ListEntry::factory(),
            ListEntryValue::COLUMN_LIST_HEADER_ID => ListHeader::factory(),
            ListEntryValue::COLUMN_VALUE          => $this->faker->sentence,
        ];
    }
}
