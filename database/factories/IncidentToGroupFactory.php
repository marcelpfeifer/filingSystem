<?php
/**
 * IncidentToGroupFactory
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 12:31
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Database\Factories;


use App\Models\Group;
use App\Models\Incident;
use App\Models\IncidentToGroup;

class IncidentToGroupFactory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = IncidentToGroup::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            IncidentToGroup::COLUMN_INCIDENT_ID => Incident::factory(),
            IncidentToGroup::COLUMN_GROUP_ID    => Group::factory(),
        ];
    }
}
