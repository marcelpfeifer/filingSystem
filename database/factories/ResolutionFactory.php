<?php
/**
 * ResolutionFactory
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.09.2021
 * Time: 10:12
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Database\Factories;

use App\Libary\View\Resolution\Create\Save;
use App\Models\Resolution;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class ResolutionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Resolution::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $file = UploadedFile::fake()->create('document.pdf', 100, 'application/pdf');
        $path = Save::STORAGE_PATH . DIRECTORY_SEPARATOR;
        $file->storePubliclyAs(
            'public' . $path,
            $file->getClientOriginalName()
        );
        return [
            Resolution::COLUMN_TITLE => $this->faker->sentence(),
            Resolution::COLUMN_PATH  => 'storage' . $path . $file->getClientOriginalName(),
        ];
    }


}
