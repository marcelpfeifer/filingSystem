<?php
/**
 * IncidentToGroupFactory
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 12:31
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Database\Factories;


use App\Models\Group;
use App\Models\ListModel;
use App\Models\ListToGroup;
use Illuminate\Database\Eloquent\Factories\Factory;

class ListToGroupFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ListToGroup::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            ListToGroup::COLUMN_LIST_ID  => ListModel::factory(),
            ListToGroup::COLUMN_GROUP_ID => Group::factory(),
        ];
    }
}
