<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncidentToTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidentToTag', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('incidentId')->index();
            $table->unsignedBigInteger('tagId')->index();
            $table->foreign('incidentId')->references('id')->on('incident')->onDelete('cascade');
            $table->foreign('tagId')->references('id')->on('tag')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidentToTag');
    }
}
