<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncidentDescriptionChangelogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'incidentDescriptionChangelog',
            function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('incidentDescriptionId')->index();
                $table->unsignedBigInteger('userId')->nullable()->index();
                $table->enum(
                    'action',
                    [
                        'INSERT',
                        'UPDATE',
                        'DELETE',
                    ]
                );
                $table->foreign('incidentDescriptionId')->references('id')->on('incidentDescription')->onDelete('cascade');
                $table->foreign('userId')->references('id')->on('users')->onDelete('set null');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidentDescriptionChangelog');
    }
}
