<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilePropertyValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profilePropertyValue', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('profileId')->index();
            $table->foreign('profileId')->references('id')->on('profile')->onDelete('cascade');
            $table->unsignedBigInteger('profilePropertyId')->index();
            $table->foreign('profilePropertyId')->references('id')->on('profileProperty')->onDelete('cascade');
            $table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profilePropertyValue');
    }
}
