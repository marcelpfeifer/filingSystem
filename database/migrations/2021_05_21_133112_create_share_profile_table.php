<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShareProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'shareProfile',
            function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('profileId')->index();
                $table->foreign('profileId')->references('id')->on('profile')->onDelete('cascade');
                $table->string('code')->index();
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shareProfile');
    }
}
