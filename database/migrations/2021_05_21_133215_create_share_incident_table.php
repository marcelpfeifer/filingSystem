<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShareIncidentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shareIncident', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('incidentId')->index();
            $table->foreign('incidentId')->references('id')->on('incident')->onDelete('cascade');
            $table->string('code')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shareIncident');
    }
}
