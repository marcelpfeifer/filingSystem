<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'listHeader',
            function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('listId')->index();
                $table->foreign('listId')->references('id')->on('list')->onDelete('cascade');
                $table->string('name');
                $table->boolean('list');
                $table->integer('order');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listHeader');
    }
}
