<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertPermissionData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $dto = (new \App\Models\Dto\Permission())
            ->setName(\App\Libary\Permission\Enum\Permission::ACCESS_BACKEND)
            ->setDescription('Zugriff auf das Admin')
            ->setParentPermission(null);
        (new \App\Models\Permission())->insertEntry($dto);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $dto = (new \App\Models\Permission())->getEntryByName(\App\Libary\Permission\Enum\Permission::ACCESS_BACKEND);
        if ($dto) {
            (new \App\Models\Permission())->deleteEntry($dto);
        }
    }
}
