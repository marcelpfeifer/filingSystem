<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoggingValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loggingValue', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('loggingId');
            $table->foreign('loggingId')->references('id')->on('logging')->onDelete('cascade');
            $table->string('tableName');
            $table->string('columnName');
            $table->longText('oldValue');
            $table->longText('newValue');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loggingValue');
    }
}
