<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncidentToGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'incidentToGroup',
            function (Blueprint $table) {
                $table->unsignedBigInteger('incidentId')->index();
                $table->unsignedBigInteger('groupId')->index();
                $table->foreign('incidentId')->references('id')->on('incident')->onDelete('cascade');
                $table->foreign('groupId')->references('id')->on('group')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidentToGroup');
    }
}
