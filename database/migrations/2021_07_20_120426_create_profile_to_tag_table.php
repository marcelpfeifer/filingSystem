<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileToTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profileToTag', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('profileId')->index();
            $table->unsignedBigInteger('tagId')->index();
            $table->foreign('profileId')->references('id')->on('profile')->onDelete('cascade');
            $table->foreign('tagId')->references('id')->on('tag')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profileToTag');
    }
}
