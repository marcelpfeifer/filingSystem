<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncidentToProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidentToProfile', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('incidentId')->index();
            $table->unsignedBigInteger('profileId')->index();
            $table->boolean('warrant');
            $table->foreign('incidentId')->references('id')->on('incident')->onDelete('cascade');
            $table->foreign('profileId')->references('id')->on('profile')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidentToProfile');
    }
}
