<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListToGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'listToGroup',
            function (Blueprint $table) {
                $table->unsignedBigInteger('listId')->index();
                $table->unsignedBigInteger('groupId')->index();
                $table->foreign('listId')->references('id')->on('list')->onDelete('cascade');
                $table->foreign('groupId')->references('id')->on('group')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listToGroup');
    }
}
