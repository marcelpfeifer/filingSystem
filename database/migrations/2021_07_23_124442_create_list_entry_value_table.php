<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListEntryValueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'listEntryValue',
            function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('listEntryId')->index();
                $table->foreign('listEntryId')->references('id')->on('listEntry')->onDelete('cascade');
                $table->unsignedBigInteger('listHeaderId')->index();
                $table->foreign('listHeaderId')->references('id')->on('listHeader')->onDelete('cascade');
                $table->text('value');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listEntryValue');
    }
}
