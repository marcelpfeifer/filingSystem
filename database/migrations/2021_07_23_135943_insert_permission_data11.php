<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertPermissionData11 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $parentPermission = null;
        $dto = (new \App\Models\Permission())->getEntryByName(\App\Libary\Permission\Enum\Permission::ACCESS_BACKEND);
        if ($dto) {
            $parentPermission = $dto->getId();
        }
        $dto = (new \App\Models\Dto\Permission())
            ->setName(\App\Libary\Permission\Enum\Permission::ACCESS_BACKEND_LIST)
            ->setDescription('Zugriff auf das Admin-List System')
            ->setParentPermission($parentPermission);
        (new \App\Models\Permission())->insertEntry($dto);

        $dto = (new \App\Models\Permission())->getEntryByName(\App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND);
        if ($dto) {
            $parentPermission = $dto->getId();
        }

        $dto = (new \App\Models\Dto\Permission())
            ->setName(\App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_LIST)
            ->setDescription('Zugriff auf die Frontend - Listen')
            ->setParentPermission($parentPermission);
        $parentPermission = (new \App\Models\Permission())->insertEntry($dto);

        $dto = (new \App\Models\Dto\Permission())
            ->setName(\App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_LIST_EDIT)
            ->setDescription('Zugriff auf die Frontend - Listen - Bearbeiten')
            ->setParentPermission($parentPermission);
        (new \App\Models\Permission())->insertEntry($dto);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $dto = (new \App\Models\Permission())->getEntryByName(
            \App\Libary\Permission\Enum\Permission::ACCESS_BACKEND_LIST
        );
        if ($dto) {
            (new \App\Models\Permission())->deleteEntry($dto);
        }

        $dto = (new \App\Models\Permission())->getEntryByName(
            \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_LIST
        );
        if ($dto) {
            (new \App\Models\Permission())->deleteEntry($dto);
        }

        $dto = (new \App\Models\Permission())->getEntryByName(
            \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_LIST_EDIT
        );
        if ($dto) {
            (new \App\Models\Permission())->deleteEntry($dto);
        }
    }
}
