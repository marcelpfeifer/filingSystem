<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profileProperty', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('profilePropertyGroupId')->index();
            $table->foreign('profilePropertyGroupId')->references('id')->on('profilePropertyGroup')->onDelete('cascade');
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profileProperty');
    }
}
