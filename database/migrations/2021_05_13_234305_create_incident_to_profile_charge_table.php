<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncidentToProfileChargeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'incidentToProfileCharge',
            function (Blueprint $table) {
                $table->unsignedBigInteger('incidentToProfileId')->index();
                $table->unsignedBigInteger('chargeId')->index();
                $table->foreign('incidentToProfileId')->references('id')->on('incidentToProfile')->onDelete('cascade');
                $table->foreign('chargeId')->references('id')->on('charge')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidentToProfileCharge');
    }
}
