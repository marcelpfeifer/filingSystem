<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionToPermissionGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissionToPermissionGroup', function (Blueprint $table) {
            $table->unsignedBigInteger('permissionId');
            $table->unsignedBigInteger('permissionGroupId');
            $table->foreign('permissionId')->references('id')->on('permission')->onDelete('cascade');
            $table->foreign('permissionGroupId')->references('id')->on('permissionGroup')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissionToPermissionGroup');
    }
}
