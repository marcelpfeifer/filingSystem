<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertPermissionData5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $dto = (new \App\Models\Dto\Permission())
            ->setName(\App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND)
            ->setDescription('Zugriff auf das Frontend')
            ->setParentPermission(null);
        (new \App\Models\Permission())->insertEntry($dto);

        $dto = (new \App\Models\Dto\Permission())
            ->setName(\App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_PROFILE)
            ->setDescription('Zugriff auf das Frontend - Profil')
            ->setParentPermission(5);
        (new \App\Models\Permission())->insertEntry($dto);

        $dto = (new \App\Models\Dto\Permission())
            ->setName(\App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_PROFILE_EDIT)
            ->setDescription('Zugriff auf das Frontend - Profil - Bearbeiten')
            ->setParentPermission(6);
        (new \App\Models\Permission())->insertEntry($dto);

        $dto = (new \App\Models\Dto\Permission())
            ->setName(\App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT)
            ->setDescription('Zugriff auf das Frontend - Vorfall')
            ->setParentPermission(5);
        (new \App\Models\Permission())->insertEntry($dto);

        $dto = (new \App\Models\Dto\Permission())
            ->setName(\App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT_EDIT)
            ->setDescription('Zugriff auf das Frontend - Vorfall - Bearbeiten')
            ->setParentPermission(8);
        (new \App\Models\Permission())->insertEntry($dto);

        $dto = (new \App\Models\Dto\Permission())
            ->setName(\App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_WARRANT)
            ->setDescription('Zugriff auf das Frontend - Haftbefehl')
            ->setParentPermission(5);
        (new \App\Models\Permission())->insertEntry($dto);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $dto = (new \App\Models\Permission())->getEntryByName(\App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND);
        if ($dto) {
            (new \App\Models\Permission())->deleteEntry($dto);
        }
        $dto = (new \App\Models\Permission())->getEntryByName(\App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_PROFILE);
        if ($dto) {
            (new \App\Models\Permission())->deleteEntry($dto);
        }
        $dto = (new \App\Models\Permission())->getEntryByName(\App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_PROFILE_EDIT);
        if ($dto) {
            (new \App\Models\Permission())->deleteEntry($dto);
        }
        $dto = (new \App\Models\Permission())->getEntryByName(\App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT);
        if ($dto) {
            (new \App\Models\Permission())->deleteEntry($dto);
        }
        $dto = (new \App\Models\Permission())->getEntryByName(\App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT_EDIT);
        if ($dto) {
            (new \App\Models\Permission())->deleteEntry($dto);
        }
        $dto = (new \App\Models\Permission())->getEntryByName(\App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_WARRANT);
        if ($dto) {
            (new \App\Models\Permission())->deleteEntry($dto);
        }
    }
}
