<?php
/**
 * IncidentSeeder
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.09.2021
 * Time: 10:00
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Database\Seeders;

use App\Models\Incident;
use App\Models\IncidentDescription;
use App\Models\IncidentToProfile;
use Illuminate\Database\Seeder;

class IncidentSeeder extends Seeder
{
    public function run()
    {
        Incident::factory()
            ->has(IncidentDescription::factory()->count(3), 'descriptions')
            ->has(IncidentToProfile::factory()->count(3), 'profilesToIncident')
            ->count(50)
            ->create();
    }
}
