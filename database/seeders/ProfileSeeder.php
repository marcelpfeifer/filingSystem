<?php
/**
 * ProfileSeeder
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 20.04.2021
 * Time: 14:15
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Database\Seeders;


use App\Models\Profile;
use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{

    public function run()
    {
        Profile::factory()
            ->count(50)
            ->create();
    }
}
