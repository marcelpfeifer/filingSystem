<?php
/**
 * ${NAME}
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.04.2021
 * Time: 12:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

Route::middleware('auth')->group(
    function () {
        Route::namespace('Admin')->middleware(
            'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_BACKEND
        )->prefix('admin')->name('admin.')->group(
            function () {
                Route::get('/', [Controllers\Admin\IndexController::class, 'index'])->name('index');

                // Permission
                Route::namespace('Permission')->middleware(
                    'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_BACKEND_PERMISSION
                )->prefix('permission')->name('permission.')->group(
                    function () {
                        Route::get('/', [Controllers\Admin\Permission\IndexController::class, 'index'])->name('index');
                        Route::post('/create', [Controllers\Admin\Permission\IndexController::class, 'create'])->name(
                            'create'
                        );
                        Route::delete(
                            '/delete/{id}',
                            [Controllers\Admin\Permission\IndexController::class, 'delete']
                        )->name('delete');
                        Route::get('/edit/{id}', [Controllers\Admin\Permission\EditController::class, 'index'])->name(
                            'edit'
                        );
                        Route::post(
                            '/edit/save/{id}',
                            [Controllers\Admin\Permission\EditController::class, 'save']
                        )->name('save');
                    }
                );


                // User
                Route::namespace('User')->middleware(
                    'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_BACKEND_USER
                )->prefix('user')->name('user.')->group(
                    function () {
                        Route::get('/', [Controllers\Admin\User\IndexController::class, 'index'])->name('index');
                        Route::get('/search', [Controllers\Admin\User\IndexController::class, 'search'])->name(
                            'search'
                        );
                        Route::prefix('create')->name('create.')->group(
                            function () {
                                Route::get('/', [Controllers\Admin\User\CreateController::class, 'index'])->name(
                                    'index'
                                );
                                Route::post('/save', [Controllers\Admin\User\CreateController::class, 'save'])->name(
                                    'save'
                                );
                            }
                        );
                        Route::prefix('edit')->name('edit.')->group(
                            function () {
                                Route::get('/{id}', [Controllers\Admin\User\EditController::class, 'index'])->name(
                                    'index'
                                );
                                Route::post('/save/{id}', [Controllers\Admin\User\EditController::class, 'save'])->name(
                                    'save'
                                );
                                Route::delete(
                                    '/delete/{id}',
                                    [Controllers\Admin\User\EditController::class, 'delete']
                                )->name(
                                    'delete'
                                );
                            }
                        );
                    }
                );

                // Charge Group
                Route::namespace('ChargeGroup')->middleware(
                    'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_BACKEND_CHARGE
                )->prefix('chargeGroup')->name('chargeGroup.')->group(
                    function () {
                        Route::get('/', [Controllers\Admin\ChargeGroup\IndexController::class, 'index'])->name('index');
                        Route::get('/groups', [Controllers\Admin\ChargeGroup\IndexController::class, 'groups'])->name(
                            'groups'
                        );
                        Route::post('/', [Controllers\Admin\ChargeGroup\IndexController::class, 'save'])->name('save');
                        Route::delete(
                            '/delete/{id}',
                            [Controllers\Admin\ChargeGroup\IndexController::class, 'delete']
                        )->name('delete');
                    }
                );

                // Charge Group
                Route::namespace('Group')->middleware(
                    'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_BACKEND_GROUP
                )->prefix('group')->name('group.')->group(
                    function () {
                        Route::get('/', [Controllers\Admin\Group\IndexController::class, 'index'])->name('index');
                        Route::get('/groups', [Controllers\Admin\Group\IndexController::class, 'groups'])->name(
                            'groups'
                        );
                        Route::post('/', [Controllers\Admin\Group\IndexController::class, 'save'])->name('save');
                        Route::delete(
                            '/delete/{id}',
                            [Controllers\Admin\Group\IndexController::class, 'delete']
                        )->name('delete');
                    }
                );

                // Tag
                Route::namespace('Tag')->middleware(
                    'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_BACKEND_TAG
                )->prefix('tag')->name('tag.')->group(
                    function () {
                        Route::get('/', [Controllers\Admin\Tag\IndexController::class, 'index'])->name('index');
                        Route::get('/search', [Controllers\Admin\Tag\IndexController::class, 'search'])->name('search');
                        Route::post('/', [Controllers\Admin\Tag\IndexController::class, 'save'])->name('save');
                        Route::delete('/delete/{id}', [Controllers\Admin\Tag\IndexController::class, 'delete'])->name(
                            'delete'
                        );
                    }
                );

                // News
                Route::namespace('News')->middleware(
                    'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_BACKEND_NEWS
                )->prefix('news')->name('news.')->group(
                    function () {
                        Route::get('/', [Controllers\Admin\News\IndexController::class, 'index'])->name('index');
                        Route::prefix('create')->name('create.')->group(
                            function () {
                                Route::get('/', [Controllers\Admin\News\CreateController::class, 'index'])->name(
                                    'index'
                                );
                                Route::post('/save', [Controllers\Admin\News\CreateController::class, 'save'])->name(
                                    'save'
                                );
                            }
                        );
                        Route::prefix('edit')->name('edit.')->group(
                            function () {
                                Route::get('/{id}', [Controllers\Admin\News\EditController::class, 'index'])->name(
                                    'index'
                                );
                                Route::post('/save/{id}', [Controllers\Admin\News\EditController::class, 'save'])->name(
                                    'save'
                                );
                            }
                        );
                    }
                );

                // Charge
                Route::namespace('Charge')->middleware(
                    'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_BACKEND_CHARGE
                )->prefix('charge')->name('charge.')->group(
                    function () {
                        Route::get('/', [Controllers\Admin\Charge\IndexController::class, 'index'])->name('index');
                        Route::get('/search', [Controllers\Admin\Charge\IndexController::class, 'search'])->name(
                            'search'
                        );
                        Route::prefix('create')->name('create.')->group(
                            function () {
                                Route::get('/', [Controllers\Admin\Charge\CreateController::class, 'index'])->name(
                                    'index'
                                );
                                Route::post('/save', [Controllers\Admin\Charge\CreateController::class, 'save'])->name(
                                    'save'
                                );
                            }
                        );
                        Route::prefix('edit')->name('edit.')->group(
                            function () {
                                Route::get('/{id}', [Controllers\Admin\Charge\EditController::class, 'index'])->name(
                                    'index'
                                );
                                Route::post(
                                    '/save/{id}',
                                    [Controllers\Admin\Charge\EditController::class, 'save']
                                )->name(
                                    'save'
                                );
                                Route::delete(
                                    '/delete/{id}',
                                    [Controllers\Admin\Charge\EditController::class, 'delete']
                                )->name(
                                    'delete'
                                );
                            }
                        );
                    }
                );

                // Profile Property Group
                Route::namespace('ProfilePropertyGroup')->middleware(
                    'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_BACKEND_PROFILE
                )->prefix('profilePropertyGroup')->name('profilePropertyGroup.')->group(
                    function () {
                        Route::get('/', [Controllers\Admin\ProfilePropertyGroup\IndexController::class, 'index'])->name(
                            'index'
                        );
                        Route::get(
                            '/groups',
                            [Controllers\Admin\ProfilePropertyGroup\IndexController::class, 'groups']
                        )->name(
                            'groups'
                        );
                        Route::post('/', [Controllers\Admin\ProfilePropertyGroup\IndexController::class, 'save'])->name(
                            'save'
                        );
                        Route::delete(
                            '/delete/{id}',
                            [Controllers\Admin\ProfilePropertyGroup\IndexController::class, 'delete']
                        )->name('delete');
                    }
                );


                // Profile Property Group
                Route::namespace('List')->middleware(
                    'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_BACKEND_LIST
                )->prefix('list')->name('list.')->group(
                    function () {
                        Route::get('/', [Controllers\Admin\ListDir\IndexController::class, 'index'])->name('index');
                        Route::get('/lists', [Controllers\Admin\ListDir\IndexController::class, 'lists'])->name(
                            'lists'
                        );
                        Route::post('/', [Controllers\Admin\ListDir\IndexController::class, 'save'])->name('save');
                        Route::delete(
                            '/delete/{id}',
                            [Controllers\Admin\ListDir\IndexController::class, 'delete']
                        )->name('delete');
                        Route::prefix('edit')->name('edit.')->group(
                            function () {
                                Route::get('/{listId}', [Controllers\Admin\ListDir\EditController::class, 'index'])->name(
                                    'index'
                                );
                                Route::post(
                                    '/create/{listId}',
                                    [Controllers\Admin\ListDir\EditController::class, 'create']
                                )->name(
                                    'create'
                                );
                                Route::post(
                                    '/save/{headerId}',
                                    [Controllers\Admin\ListDir\EditController::class, 'save']
                                )->name(
                                    'save'
                                );
                                Route::delete(
                                    '/delete/{headerId}',
                                    [Controllers\Admin\ListDir\EditController::class, 'delete']
                                )->name(
                                    'delete'
                                );
                            }
                        );
                    }
                );

                // Profile Property
                Route::namespace('ProfileProperty')->middleware(
                    'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_BACKEND_PROFILE
                )->prefix('profileProperty')->name('profileProperty.')->group(
                    function () {
                        Route::get('/', [Controllers\Admin\ProfileProperty\IndexController::class, 'index'])->name(
                            'index'
                        );
                        Route::get(
                            '/search',
                            [Controllers\Admin\ProfileProperty\IndexController::class, 'search']
                        )->name(
                            'search'
                        );
                        Route::prefix('create')->name('create.')->group(
                            function () {
                                Route::get(
                                    '/',
                                    [Controllers\Admin\ProfileProperty\CreateController::class, 'index']
                                )->name(
                                    'index'
                                );
                                Route::post(
                                    '/save',
                                    [Controllers\Admin\ProfileProperty\CreateController::class, 'save']
                                )->name(
                                    'save'
                                );
                            }
                        );
                        Route::prefix('edit')->name('edit.')->group(
                            function () {
                                Route::get(
                                    '/{id}',
                                    [Controllers\Admin\ProfileProperty\EditController::class, 'index']
                                )->name(
                                    'index'
                                );
                                Route::post(
                                    '/save/{id}',
                                    [Controllers\Admin\ProfileProperty\EditController::class, 'save']
                                )->name(
                                    'save'
                                );
                                Route::delete(
                                    '/delete/{id}',
                                    [Controllers\Admin\ProfileProperty\EditController::class, 'delete']
                                )->name(
                                    'delete'
                                );
                            }
                        );
                    }
                );
            }
        );
    }
);
