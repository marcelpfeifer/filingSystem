<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth', 'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND])->group(
    function () {
        Route::get('/', [Controllers\IndexController::class, 'index'])->name('index');
        Route::namespace('Profile')->middleware(
            'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_PROFILE
        )->prefix('profile')->name('profile.')->group(
            function () {
                Route::get('/', [Controllers\Profile\IndexController::class, 'index'])->name('index');
                Route::get('/search', [Controllers\Profile\IndexController::class, 'search'])->name('search');
                Route::prefix('create')->name('create.')->group(
                    function () {
                        Route::get('/', [Controllers\Profile\CreateController::class, 'index'])->name('index');
                        Route::post('/save', [Controllers\Profile\CreateController::class, 'save'])->name('save');
                    }
                );

                Route::prefix('show')->name('show.')->group(
                    function () {
                        Route::get('/{id}', [Controllers\Profile\ShowController::class, 'index'])->name('index');
                        Route::get('share/{id}', [Controllers\Profile\ShowController::class, 'share'])->name('share');
                    }
                );

                Route::prefix('edit')->name('edit.')->middleware(
                    'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_PROFILE_EDIT
                )->group(
                    function () {
                        Route::get('/{id}', [Controllers\Profile\EditController::class, 'index'])->name('index');
                        Route::post('/save/{id}', [Controllers\Profile\EditController::class, 'save'])->name('save');
                        Route::delete('/delete/{id}', [Controllers\Profile\EditController::class, 'delete'])->name(
                            'delete'
                        );
                        Route::post('/addTag/{id}', [Controllers\Profile\EditController::class, 'addTag'])->name(
                            'addTag'
                        );
                        Route::delete('/deleteTag/{id}', [Controllers\Profile\EditController::class, 'deleteTag'])
                            ->name('deleteTag');
                    }
                );
            }
        );

        Route::namespace('Incident')->prefix('incident')->name('incident.')->middleware(
            'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT
        )->group(
            function () {
                Route::get('/', [Controllers\Incident\IndexController::class, 'index'])->name('index');
                Route::get('/search', [Controllers\Incident\IndexController::class, 'search'])->name('search');
                Route::prefix('create')->name('create.')->group(
                    function () {
                        Route::get('/', [Controllers\Incident\CreateController::class, 'index'])->name('index');
                        Route::post('/save', [Controllers\Incident\CreateController::class, 'save'])->name('save');
                    }
                );

                Route::prefix('show')->name('show.')->group(
                    function () {
                        Route::get('/{id}', [Controllers\Incident\ShowController::class, 'index'])->name('index');
                        Route::get('share/{id}', [Controllers\Incident\ShowController::class, 'share'])->name('share');
                    }
                );

                Route::prefix('edit')->name('edit.')->middleware(
                    'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT_EDIT
                )->group(
                    function () {
                        Route::get('/{id}', [Controllers\Incident\EditController::class, 'index'])->name('index');
                        Route::post('/save/{id}', [Controllers\Incident\EditController::class, 'save'])->name('save');
                        Route::delete('/delete/{id}', [Controllers\Incident\EditController::class, 'delete'])->name(
                            'delete'
                        );
                        Route::post('/addUser/{id}', [Controllers\Incident\EditController::class, 'addUser'])->name(
                            'addUser'
                        );
                        Route::post('/addTag/{id}', [Controllers\Incident\EditController::class, 'addTag'])->name(
                            'addTag'
                        );
                        Route::post('/warrant/{id}', [Controllers\Incident\EditController::class, 'warrant'])->name(
                            'warrant'
                        );
                        Route::delete('/deleteUser/{id}', [Controllers\Incident\EditController::class, 'deleteUser'])
                            ->name('deleteUser');
                        Route::delete('/deleteTag/{id}', [Controllers\Incident\EditController::class, 'deleteTag'])
                            ->name('deleteTag');
                    }
                );

                Route::prefix('charge')->name('charge.')->middleware(
                    'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT_EDIT
                )->group(
                    function () {
                        Route::get(
                            '/{incidentId}/{profileId}',
                            [Controllers\Incident\ChargeController::class, 'index']
                        )->name('index');
                        Route::post('', [Controllers\Incident\ChargeController::class, 'save'])->name('save');
                    }
                );

                Route::prefix('group')->name('group.')->middleware(
                    'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT_EDIT
                )->group(
                    function () {
                        Route::get(
                            '/{incidentId}',
                            [Controllers\Incident\GroupController::class, 'index']
                        )->name('index');
                        Route::post('/{incidentId}', [Controllers\Incident\GroupController::class, 'save'])->name(
                            'save'
                        );
                    }
                );
            }
        );

        Route::namespace('Warrant')->prefix('warrant')->name('warrant.')->middleware(
            'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_WARRANT
        )->group(
            function () {
                Route::get('/', [Controllers\Warrant\IndexController::class, 'index'])->name('index');
            }
        );

        Route::namespace('ListDir')->prefix('list')->name('list.')->middleware(
            'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_LIST
        )->group(
            function () {
                Route::get('/', [Controllers\ListDir\IndexController::class, 'index'])->name('index');
                Route::namespace('Entry')->prefix('entry')->name('entry.')->group(
                    function () {
                        Route::get('/{listId}', [Controllers\ListDir\Entry\IndexController::class, 'index'])->name('index');
                        Route::get('/search/{listId}', [Controllers\ListDir\Entry\IndexController::class, 'search'])->name('search');
                        Route::prefix('create')->name('create.')->middleware(
                            'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_LIST_EDIT
                        )->group(
                            function () {
                                Route::get('/{listId}', [Controllers\ListDir\Entry\CreateController::class, 'index'])->name('index');
                                Route::post('/save/{listId}', [Controllers\ListDir\Entry\CreateController::class, 'save'])->name('save');
                            }
                        );
                        Route::prefix('show')->name('show.')->group(
                            function () {
                                Route::get('/{entryId}', [Controllers\ListDir\Entry\ShowController::class, 'index'])->name('index');
                            }
                        );
                        Route::prefix('edit')->name('edit.')->middleware(
                            'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_LIST_EDIT
                        )->group(
                            function () {
                                Route::get('/{entryId}', [Controllers\ListDir\Entry\EditController::class, 'index'])->name('index');
                                Route::post('/save/{entryId}', [Controllers\ListDir\Entry\EditController::class, 'save'])->name('save');
                                Route::delete('/delete/{entryId}', [Controllers\ListDir\Entry\EditController::class, 'delete'])->name(
                                    'delete'
                                );
                            }
                        );
                    }
                );
            }
        );

        Route::namespace('Resolution')->prefix('resolution')->name('resolution.')->middleware(
            'permission:' . \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_RESOLUTION
        )->group(
            function () {
                Route::get('/', [Controllers\Resolution\IndexController::class, 'index'])->name('index');
                Route::prefix('create')->name('create.')->group(
                    function () {
                        Route::get('/', [Controllers\Resolution\CreateController::class, 'index'])->name('index');
                        Route::post('/save', [Controllers\Resolution\CreateController::class, 'save'])->name('save');
                    }
                );

                Route::prefix('show')->name('show.')->group(
                    function () {
                        Route::get('/{id}', [Controllers\Resolution\ShowController::class, 'index'])->name('index');
                        Route::delete('/{id}', [Controllers\Resolution\ShowController::class, 'delete'])->name(
                            'delete'
                        );
                    }
                );
            }
        );
    }
);

Route::namespace('Share')->prefix('share')->name('share.')->group(
    function () {
        Route::get('/', [Controllers\Share\IndexController::class, 'index'])->name('index');
        Route::get('/incident/{id}', [Controllers\Share\IndexController::class, 'incident'])->name('incident');
        Route::get('/profile/{id}', [Controllers\Share\IndexController::class, 'profile'])->name('profile');
    }
);
