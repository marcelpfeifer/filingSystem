@servers(['web' => 'deployer@85.214.27.142'])

@setup
    $repository = 'todo';
    $releases_dir = '/var/www/todo.eternityv.de/releases';
    $app_dir = '/var/www/todo.eternityv.de';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    run_npm
    update_symlinks
    clean_old_releases
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_composer')
    echo "Composer ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('run_npm')
    echo "NPM ({{ $release }})"
    cd {{ $new_release_dir }}
    npm install
    npm run production
@endtask

@task('update_symlinks')
    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    cd {{ $new_release_dir }}
    php artisan clear-compiled
    echo "Migrate ({{ $release }})"
    php artisan migrate --force

    echo "Artisan Storage Link"
    php artisan storage:link

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current

    # Reload FPM for changes to take effect
    echo 'Reloading php7.4-fpm';
    sudo service php7.4-fpm restart
@endtask

@task('clean_old_releases')
    # This will list our releases by modification time and delete all but the 3 most recent.
    purging=$(ls -dt {{ $releases_dir }}/* | tail -n +3);

    if [ "$purging" != "" ]; then
    echo Purging old releases: $purging;
    rm -rf $purging;
    else
    echo "No releases found for purging at this time";
    fi
@endtask

