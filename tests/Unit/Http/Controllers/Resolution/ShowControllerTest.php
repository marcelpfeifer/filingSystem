<?php
/**
 * ShowControllerTest
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.09.2021
 * Time: 10:06
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Tests\Unit\Http\Controllers\Resolution;

use App\Models\Resolution;
use Tests\TestCase;
use Tests\Traits\HasUser;

class ShowControllerTest extends TestCase
{

    use HasUser;

    /**
     * @var string[]
     */
    protected $permissions = [
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_RESOLUTION,
    ];

    /**
     * @group Controllers
     * @group Resolution
     */
    public function testIndex()
    {
        $this->addUser();

        // Create Profile
        $entry = Resolution::factory()->create();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get(route('resolution.show.index', $entry->id));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->get(route('resolution.show.index', $entry->id));
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }

}
