<?php
/**
 * CreateControllerTest
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.09.2021
 * Time: 10:06
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Tests\Unit\Http\Controllers\Resolution;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Tests\TestCase;
use Tests\Traits\HasUser;

class CreateControllerTest extends TestCase
{
    use HasUser;

    /**
     * @var string[]
     */
    protected $permissions = [
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_RESOLUTION
    ];

    /**
     * @group Controllers
     * @group Resolution
     */
    public function testIndex()
    {
        $this->addUser();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get(route('resolution.create.index'));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->get(route('resolution.create.index'));
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }

    /**
     * @group Controllers
     * @group Resolution
     */
    public function testSave()
    {
        $file = UploadedFile::fake()->create('document.pdf', 100, 'application/pdf');
        $data = [
            'title' => Str::random(10),
            'file'  => $file,
        ];

        $this->addUser();
        $response = $this
            ->actingAs($this->user)
            ->postJson(route('resolution.create.save'), $data);
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->postJson(route('resolution.create.save'), $data);
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }
}
