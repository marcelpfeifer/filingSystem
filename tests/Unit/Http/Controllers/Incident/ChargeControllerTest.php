<?php
/**
 * ChargeControllerTest
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.06.2021
 * Time: 21:32
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Tests\Unit\Http\Controllers\Incident;


use App\Models\Charge;
use App\Models\Incident;
use App\Models\IncidentDescription;
use App\Models\IncidentToProfile;
use App\Models\Profile;
use Tests\TestCase;
use Tests\Traits\HasUser;

class ChargeControllerTest extends TestCase
{
    use HasUser;

    /**
     * @var string[]
     */
    protected $permissions = [
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT_EDIT,
    ];

    /**
     * @group Controllers
     * @group Profile
     */
    public function testIndex()
    {
        $this->addUser();

        $incident = Incident::factory()
            ->has(IncidentDescription::factory()->count(3), 'descriptions')
            ->create();
        $profile = Profile::factory()->create();

        $route = route(
            'incident.charge.index',
            [
                'incidentId' => $incident->id,
                'profileId'  => $profile->id,
            ]
        );

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get($route);
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->get($route);
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }

    /**
     * @group Controllers
     * @group Profile
     */
    public function testSave()
    {
        $this->addUser();

        $incident = Incident::factory()
            ->has(IncidentDescription::factory()->count(3), 'descriptions')
            ->has(IncidentToProfile::factory()->count(3), 'profilesToIncident')
            ->create();

        $route = route('incident.charge.save');
        $chargesAsArray = [];

        foreach (Charge::getEntriesAsDto(Charge::all()) as $charge) {
            $chargesAsArray[] = [
                'id'          => $charge->getId(),
                'name'        => $charge->getName(),
                'description' => $charge->getDescription(),
                'time'        => $charge->getTime(),
                'money'       => $charge->getMoney(),
            ];
        }

        $data = [
            'charges'    => $chargesAsArray,
            'profileId'  => $incident->profilesToIncident[0]->profileId,
            'incidentId' => $incident->id,
        ];

        $response = $this
            ->actingAs($this->user)
            ->postJson($route, $data);
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->postJson($route, $data);
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }
}
