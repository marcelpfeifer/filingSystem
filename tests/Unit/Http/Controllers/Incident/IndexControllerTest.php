<?php
/**
 * IndexControllerTest
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.06.2021
 * Time: 21:32
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Tests\Unit\Http\Controllers\Incident;

use App\Models\Incident;
use Tests\TestCase;
use Tests\Traits\HasUser;

class IndexControllerTest extends TestCase
{

    use HasUser;

    /**
     * @var string[]
     */
    protected $permissions = [
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT,
    ];

    /**
     * @group Controllers
     * @group Profile
     */
    public function testIndex()
    {
        $this->addUser();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get(route('incident.index'));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->get(route('incident.index'));
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }

    /**
     * @group Controllers
     * @group Profile
     */
    public function testSearch()
    {
        $this->addUser();

        $incidents = Incident::factory()->count(20)->create();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get(route('incident.search'));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->json(
                'GET',
                route('incident.search'),
                [
                    'search' => $incidents[0]->{Incident::COLUMN_TITLE}
                ]
            );
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }
}
