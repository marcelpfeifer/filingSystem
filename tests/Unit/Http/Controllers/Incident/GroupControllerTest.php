<?php
/**
 * ChargeControllerTest
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.06.2021
 * Time: 21:32
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Tests\Unit\Http\Controllers\Incident;

use App\Models\Group;
use App\Models\Incident;
use App\Models\IncidentDescription;
use Tests\TestCase;
use Tests\Traits\HasUser;

class GroupControllerTest extends TestCase
{
    use HasUser;

    /**
     * @var string[]
     */
    protected $permissions = [
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT_EDIT,
    ];

    /**
     * @group Controllers
     * @group Profile
     */
    public function testIndex()
    {
        $this->addUser();

        $incident = Incident::factory()
            ->has(IncidentDescription::factory()->count(3), 'descriptions')
            ->create();

        $route = route(
            'incident.group.index',
            [
                'incidentId' => $incident->id,
            ]
        );

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get($route);
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->get($route);
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }

    /**
     * @group Controllers
     * @group Profile
     */
    public function testSave()
    {
        $this->addUser();

        $incident = Incident::factory()
            ->has(IncidentDescription::factory()->count(3), 'descriptions')
            ->create();

        $groups = Group::factory()->count(10)->create();

        $route = route('incident.group.save', $incident->id);
        $data = [
            'selectedGroups' => [
                $groups[0]->id
            ],
        ];

        $response = $this
            ->actingAs($this->user)
            ->postJson($route, $data);
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->postJson($route, $data);
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }
}
