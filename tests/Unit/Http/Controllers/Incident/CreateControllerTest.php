<?php
/**
 * CreateControllerTest
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.06.2021
 * Time: 21:32
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Tests\Unit\Http\Controllers\Incident;

use Illuminate\Support\Str;
use Tests\TestCase;
use Tests\Traits\HasUser;

class CreateControllerTest extends TestCase
{
    use HasUser;

    /**
     * @var string[]
     */
    protected $permissions = [
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT_EDIT,
    ];

    /**
     * @group Controllers
     * @group Profile
     */
    public function testIndex()
    {
        $this->addUser();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get(route('incident.create.index'));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->get(route('incident.create.index'));
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }

    /**
     * @group Controllers
     * @group Profile
     */
    public function testSave()
    {
        $data = [
            'title'       => Str::random(10),
            'description' => Str::random(100),
        ];

        $this->addUser();
        $response = $this
            ->actingAs($this->user)
            ->postJson(route('incident.create.save'), $data);
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->postJson(route('incident.create.save'), $data);
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }
}
