<?php
/**
 * ShowControllerTest
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.06.2021
 * Time: 21:32
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Tests\Unit\Http\Controllers\Incident;

use App\Models\Incident;
use App\Models\IncidentDescription;
use Tests\TestCase;
use Tests\Traits\HasUser;

class ShowControllerTest extends TestCase
{

    use HasUser;

    /**
     * @var string[]
     */
    protected $permissions = [
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT,
    ];

    /**
     * @group Controllers
     * @group Profile
     */
    public function testIndex()
    {
        $this->addUser();

        $entry = Incident::factory()
            ->has(IncidentDescription::factory()->count(3), 'descriptions')
            ->create();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get(route('incident.show.index', $entry->id));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->get(route('incident.show.index', $entry->id));
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }

    /**
     * @group Controllers
     * @group Profile
     */
    public function testShare()
    {
        $this->addUser();

        // Create Profile
        $entry = Incident::factory()
            ->has(IncidentDescription::factory()->count(3), 'descriptions')
            ->create();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get(route('incident.show.share', $entry->id));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->json(
                'GET',
                route('incident.show.share', $entry->id),
            );
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }
}
