<?php
/**
 * IndexControllerTest
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 20.04.2021
 * Time: 13:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Tests\Unit\Http\Controllers\Incident;

use App\Models\Incident;
use App\Models\IncidentDescription;
use Illuminate\Support\Str;
use Tests\TestCase;
use Tests\Traits\HasUser;

class EditControllerTest extends TestCase
{
    use HasUser;

    /**
     * @var string[]
     */
    protected $permissions = [
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_INCIDENT_EDIT,
    ];

    /**
     * @group Controllers
     * @group Profile
     */
    public function testIndex()
    {
        $this->addUser();

        // Create Incident
        $entry = Incident::factory()
            ->has(IncidentDescription::factory()->count(3), 'descriptions')
            ->create();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get(route('incident.edit.index', $entry->id));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->get(route('incident.edit.index', $entry->id));
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }

    /**
     * @group Controllers
     * @group Profile
     */
    public function testSave()
    {
        $this->addUser();

        // Create Incident
        $entry = Incident::factory()
            ->has(IncidentDescription::factory()->count(3), 'descriptions')
            ->create();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->post(route('incident.edit.save', $entry->id));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->json(
                'POST',
                route('incident.edit.save', $entry->id),
                [
                    'title'        => Str::random(10),
                    // Has to be the same amount as the IncidentDescription created for the entry
                    'descriptions' => [
                        [
                            'description' => Str::random(200),
                        ],
                        [
                            'description' => Str::random(200),
                        ],
                        [
                            'description' => Str::random(200),
                        ],
                    ],
                ]
            );
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }

    /**
     * @group Controllers
     * @group Profile
     */
    public function testDelete()
    {
        $this->addUser();

        // Create Incident
        $entry = Incident::factory()
            ->has(IncidentDescription::factory()->count(3), 'descriptions')
            ->create();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->delete(route('incident.edit.delete', $entry->id));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->json('DELETE', route('incident.edit.delete', $entry->id));
        $response->assertStatus(200);
        $this->deletePermissionGroup();
    }
}
