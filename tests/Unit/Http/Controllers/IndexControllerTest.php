<?php
/**
 * IndexControllerTest
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.05.2021
 * Time: 14:28
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Tests\Unit\Http\Controllers;

use Tests\TestCase;
use Tests\Traits\HasUser;

class IndexControllerTest extends TestCase
{

    use HasUser;

    /**
     * @var string[]
     */
    protected $permissions = [
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND,
    ];

    /**
     * @group Controllers
     */
    public function testIndex()
    {
        $this->addUser();

        $response = $this
            ->actingAs($this->user)
            ->get(route('index'));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->get(route('index'));
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }
}
