<?php
/**
 * CreateControllerTest
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 20.04.2021
 * Time: 13:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Tests\Unit\Http\Controllers\Profile;

use App\Models\Profile;
use Tests\TestCase;
use Tests\Traits\HasUser;

class CreateControllerTest extends TestCase
{
    use HasUser;

    /**
     * @var string[]
     */
    protected $permissions = [
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_PROFILE
    ];

    /**
     * @group Controllers
     * @group Profile
     */
    public function testIndex()
    {
        $this->addUser();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get(route('profile.create.index'));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->get(route('profile.create.index'));
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }

    /**
     * @group Controllers
     * @group Profile
     */
    public function testSave()
    {
        $profile = Profile::factory()->create();
        $data = [
            'name'        => $profile->name,
            'image'       => $profile->image,
            'description' => $profile->description,
        ];

        $this->addUser();
        $response = $this
            ->actingAs($this->user)
            ->postJson(route('profile.create.save'), $data);
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->postJson(route('profile.create.save'), $data);
        $response->assertStatus(200);
        $response->assertJson(
            [
                'messages' => [
                    [
                        'text'  => 'Erfolgreich gespeichert!',
                        'color' => 'green'
                    ]
                ]
            ]
        );

        $this->deletePermissionGroup();
    }
}
