<?php
/**
 * ShowControllerTest
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.06.2021
 * Time: 14:55
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Tests\Unit\Http\Controllers\Profile;

use App\Models\Profile;
use Tests\TestCase;
use Tests\Traits\HasUser;

class ShowControllerTest extends TestCase
{

    use HasUser;

    /**
     * @var string[]
     */
    protected $permissions = [
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_PROFILE,
    ];

    /**
     * @group Controllers
     * @group Profile
     */
    public function testIndex()
    {
        $this->addUser();

        // Create Profile
        $entry = Profile::factory()->create();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get(route('profile.show.index', $entry->id));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->get(route('profile.show.index', $entry->id));
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }

    /**
     * @group Controllers
     * @group Profile
     */
    public function testShare()
    {
        $this->addUser();

        // Create Profile
        $entry = Profile::factory()->create();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get(route('profile.show.share', $entry->id));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->json(
                'GET',
                route('profile.show.share', $entry->id),
            );
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }
}
