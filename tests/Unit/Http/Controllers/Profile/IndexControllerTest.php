<?php
/**
 * IndexControllerTest
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 20.04.2021
 * Time: 13:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Tests\Unit\Http\Controllers\Profile;

use App\Models\Profile;
use Tests\TestCase;
use Tests\Traits\HasUser;

class IndexControllerTest extends TestCase
{

    use HasUser;

    /**
     * @var string[]
     */
    protected $permissions = [
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_PROFILE,
    ];

    /**
     * @group Controllers
     * @group Profile
     */
    public function testIndex()
    {
        $this->addUser();

        Profile::factory()->count(20)->create();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get(route('profile.index'));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->get(route('profile.index'));
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }

    /**
     * @group Controllers
     * @group Profile
     */
    public function testSearch()
    {
        $this->addUser();

        $profiles = Profile::factory()->count(20)->create();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get(route('profile.search'));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->json(
                'GET',
                route('profile.search'),
                [
                    'search' => $profiles[0]->{Profile::COLUMN_NAME}
                ]
            );
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }
}
