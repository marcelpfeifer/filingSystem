<?php
/**
 * IndexControllerTest
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 20.04.2021
 * Time: 13:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Tests\Unit\Http\Controllers\Profile;

use App\Models\Profile;
use Tests\TestCase;
use Tests\Traits\HasUser;

class EditControllerTest extends TestCase
{
    use HasUser;

    /**
     * @var string[]
     */
    protected $permissions = [
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_PROFILE,
        \App\Libary\Permission\Enum\Permission::ACCESS_FRONTEND_PROFILE_EDIT,
    ];

    /**
     * @group Controllers
     * @group Profile
     */
    public function testIndex()
    {
        $this->addUser();

        // Create Profile
        $entry = Profile::factory()->create();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->get(route('profile.edit.index', $entry->id));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->get(route('profile.edit.index', $entry->id));
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }

    /**
     * @group Controllers
     * @group Profile
     */
    public function testSave()
    {
        $this->addUser();

        // Create Profile
        $entry = Profile::factory()->create();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->post(route('profile.edit.save', $entry->id));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->json(
                'POST',
                route('profile.edit.save', $entry->id),
                [
                    'name'        => $entry->name,
                    'image'       => $entry->image,
                    'description' => $entry->description,
                ]
            );
        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }

    /**
     * @group Controllers
     * @group Profile
     */
    public function testDelete()
    {
        $this->addUser();

        // Create Profile
        $entry = Profile::factory()->create();

        // Check Permissions
        $response = $this
            ->actingAs($this->user)
            ->delete(route('profile.edit.delete', $entry->id));
        $response->assertStatus(401);

        $this->addPermissions($this->permissions);

        $response = $this
            ->actingAs($this->user)
            ->json('DELETE', route('profile.edit.delete', $entry->id));

        $response->assertStatus(200);

        $this->deletePermissionGroup();
    }
}
