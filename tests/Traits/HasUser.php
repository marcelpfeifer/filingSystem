<?php
/**
 * HasUser
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 24.05.2021
 * Time: 13:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace Tests\Traits;


use App\Models\Dto\PermissionGroup;
use App\Models\Dto\PermissionToPermissionGroup;
use App\Models\Permission;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

trait HasUser
{

    /**
     * @var User
     */
    protected $user;

    protected function addUser()
    {
        $this->user = User::factory()->create();
    }

    /**
     * @param string[] $permissions
     */
    protected function addPermissions(array $permissions)
    {
        foreach ($permissions as $permission) {
            $this->addPermissionToDatabase($permission);
        }
        \App\Libary\Permission\Permission::refreshAllowedPermissions();
    }

    protected function deletePermissionGroup()
    {
        $dto = (new PermissionGroup())
            ->setId($this->user->{User::COLUMN_PERMISSION_GROUP_ID});
        (new \App\Models\PermissionGroup())->deleteEntry($dto);

        \App\Libary\Permission\Permission::refreshAllowedPermissions();
    }

    /**
     * @param string $permissionName
     */
    protected function addPermissionToDatabase(string $permissionName)
    {
        $permissionEntry = (new Permission())->getEntryByName($permissionName);
        if ($permissionEntry) {
            $dto = (new PermissionToPermissionGroup())
                ->setPermissionGroupId($this->user->{User::COLUMN_PERMISSION_GROUP_ID})
                ->setPermissionId($permissionEntry->getId());
            (new \App\Models\PermissionToPermissionGroup())->insertEntry($dto);
        }
    }
}
