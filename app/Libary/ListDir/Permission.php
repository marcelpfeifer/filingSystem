<?php
/**
 * Permission
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.09.2021
 * Time: 10:39
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\ListDir;


use App\Models\Group;
use App\Models\ListModel;
use App\Models\ListToGroup;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class Permission
{

    /**
     * @param int $listId
     * @return bool
     */
    public static function hasPermission(int $listId): bool
    {
        $groupId = Auth::user()->{User::COLUMN_GROUP_ID};
        if ($groupId && !Auth::user()->group->{Group::COLUMN_SHOW_ALL}) {
            $entry = ListModel::find($listId);
            if (!($entry)) {
                return false;
            }
            foreach ($entry->listToGroup as $listToGroup) {
                $dto = ListToGroup::getEntryAsDto($listToGroup);
                if ($groupId === $dto->getGroupId()) {
                    return true;
                }
            }
            return false;
        }

        return true;
    }
}
