<?php
/**
 * Permission
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.04.2021
 * Time: 12:04
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\Permission\Enum;

class Permission
{
    /**
     * @var string
     */
    public const ACCESS_BACKEND = 'ACCESS_BACKEND';

    /**
     * @var string
     */
    public const ACCESS_BACKEND_PERMISSION = 'ACCESS_BACKEND_PERMISSION';

    /**
     * @var string
     */
    public const ACCESS_BACKEND_USER = 'ACCESS_BACKEND_USER';

    /**
     * @var string
     */
    public const ACCESS_BACKEND_CHARGE = 'ACCESS_BACKEND_CHARGE';

    /**
     * @var string
     */
    public const ACCESS_BACKEND_GROUP = 'ACCESS_BACKEND_GROUP';

    /**
     * @var string
     */
    public const ACCESS_BACKEND_NEWS = 'ACCESS_BACKEND_NEWS';

    /**
     * @var string
     */
    public const ACCESS_BACKEND_TAG = 'ACCESS_BACKEND_TAG';

    /**
     * @var string
     */
    public const ACCESS_BACKEND_PROFILE = 'ACCESS_BACKEND_PROFILE';

    /**
     * @var string
     */
    public const ACCESS_BACKEND_LIST = 'ACCESS_BACKEND_LIST';

    /**
     * @var string
     */
    public const ACCESS_FRONTEND = 'ACCESS_FRONTEND';

    /**
     * @var string
     */
    public const ACCESS_FRONTEND_PROFILE = 'ACCESS_FRONTEND_PROFILE';

    /**
     * @var string
     */
    public const ACCESS_FRONTEND_PROFILE_EDIT = 'ACCESS_FRONTEND_PROFILE_EDIT';

    /**
     * @var string
     */
    public const ACCESS_FRONTEND_INCIDENT = 'ACCESS_FRONTEND_INCIDENT';

    /**
     * @var string
     */
    public const ACCESS_FRONTEND_INCIDENT_EDIT = 'ACCESS_FRONTEND_INCIDENT_EDIT';

    /**
     * @var string
     */
    public const ACCESS_FRONTEND_WARRANT = 'ACCESS_FRONTEND_WARRANT';

    /**
     * @var string
     */
    public const ACCESS_FRONTEND_RESOLUTION = 'ACCESS_FRONTEND_RESOLUTION';

    /**
     * @var string
     */
    public const ACCESS_FRONTEND_LIST = 'ACCESS_FRONTEND_LIST';

    /**
     * @var string
     */
    public const ACCESS_FRONTEND_LIST_EDIT = 'ACCESS_FRONTEND_LIST_EDIT';
}
