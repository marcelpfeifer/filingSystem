<?php
/**
 * Permission
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.04.2021
 * Time: 11:57
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\Permission;

use App\Models\PermissionToPermissionGroup;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Permission
{
    /**
     * @var array|null
     */
    public static $allowedPermissions = null;

    /**
     * @param string $permissionName
     * @return bool
     */
    public static function allowed(string $permissionName): int
    {
        $dto = (new \App\Models\Permission())->getEntryByName($permissionName);

        // Es gibt keinen Permission Eintrag in der Datenbank
        if (!($dto)) {
            return false;
        }
        $permissionId = $dto->getId();

        if (is_null(self::$allowedPermissions)) {
            self::refreshAllowedPermissions();
        }

        return in_array($permissionId, self::$allowedPermissions);
    }


    /**
     * @return mixed
     */
    public static function getPermissionNames()
    {
        if (is_null(self::$allowedPermissions)) {
            self::refreshAllowedPermissions();
        }
        return (new \App\Models\Permission())->getNamesByIds(self::$allowedPermissions);
    }

    public static function refreshAllowedPermissions(): void
    {
        $user = Auth::user();
        if (!($user)) {
            self::$allowedPermissions = [];
            return;
        }
        $groupId = $user->{User::COLUMN_PERMISSION_GROUP_ID};
        if (is_null($groupId)) {
            self::$allowedPermissions = [];
            return;
        }
        self::$allowedPermissions = (new PermissionToPermissionGroup())->getPermissionsIdsByPermissionGroupId(
            $groupId
        );
    }
}
