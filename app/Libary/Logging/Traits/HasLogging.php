<?php
/**
 * Variables
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 10.04.2020
 * Time: 09:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\Logging\Traits;

/**
 * Trait Variables
 * @package App\Libraries\WebAdminLogging\Traits
 */
trait HasLogging
{

    /**
     * @var \ReflectionClass
     */
    private $reflectionClass;

    /**
     * @return array
     */
    public function getVariables(): array
    {
        return get_object_vars($this);
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function getClassName(): string
    {
        return $this->getReflectionClass()->getShortName();
    }

    /**
     * @return \ReflectionClass
     * @throws \ReflectionException
     */
    public function getReflectionClass()
    {
        if (!($this->reflectionClass)) {
            $this->reflectionClass = new \ReflectionClass($this);
        }
        return $this->reflectionClass;
    }
}
