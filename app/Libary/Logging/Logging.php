<?php
/**
 * WebAdminLogging
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 10.04.2020
 * Time: 09:15
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\Logging;

use App\Models\Dto\LoggingValue;
use App\Models\Enum\Logging\Action;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Logging
{
    /**
     * @param string $name
     * @param ILogging $dto
     * @param int|null $loggingId
     * @return int|null
     */
    public static function insert(string $name, ILogging $dto, int $loggingId = null): ?int
    {
        $action = Action::INSERT;
        $loggingValues = [];
        foreach ($dto->getVariables() as $column => $value) {
            if ($value === null) {
                continue;
            }

            $loggingValues[] = (new LoggingValue())
                ->setTableName($dto->getClassName())
                ->setColumnName($column)
                ->setOldValue('')
                ->setNewValue($value ?? '');
        }

        return self::log($name, $action, $loggingValues, $loggingId);
    }

    /**
     * @param string $name
     * @param ILogging $oldDto
     * @param ILogging $newDto
     * @param int|null $loggingId
     * @return int|null
     */
    public static function update(
        string $name,
        ILogging $oldDto,
        ILogging $newDto,
        int $loggingId = null
    ): ?int {
        $action = Action::UPDATE;
        $loggingValues = [];

        $oldValues = $oldDto->getVariables();
        $newValues = $newDto->getVariables();

        $loggingValues[] = (new LoggingValue())
            ->setTableName($newDto->getClassName())
            ->setColumnName('id')
            ->setOldValue($oldValues['id'] ?? '')
            ->setNewValue($newValues['id'] ?? '');

        foreach (array_diff_assoc($oldValues, $newValues) as $column => $value) {
            if ($oldValues[$column] === null && $newValues[$column] === null) {
                continue;
            }
            if ($column == 'id') {
                continue;
            }

            $loggingValues[] = (new LoggingValue())
                ->setTableName($newDto->getClassName())
                ->setColumnName($column)
                ->setOldValue($oldValues[$column] ?? '')
                ->setNewValue($newValues[$column] ?? '');
        }
        return self::log($name, $action, $loggingValues, $loggingId);
    }

    /**
     * @param string $name
     * @param ILogging $dto
     * @param int|null $loggingId
     * @return int|null
     */
    public static function delete(string $name, ILogging $dto, int $loggingId = null): ?int
    {
        $action = Action::DELETE;
        $loggingValues = [];
        foreach ($dto->getVariables() as $column => $value) {
            if ($value === null) {
                continue;
            }

            $loggingValues[] = (new LoggingValue())
                ->setTableName($dto->getClassName())
                ->setColumnName($column)
                ->setOldValue($value ?? '')
                ->setNewValue('');
        }

        return self::log($name, $action, $loggingValues, $loggingId);
    }

    /**
     * @param string $name
     * @param string $action
     * @param LoggingValue[] $loggingValues
     * @param int|null $loggingId
     * @return int|null
     */
    private static function log(
        string $name,
        string $action,
        array $loggingValues,
        int $loggingId = null
    ): ?int {
        if (count($loggingValues) === 0) {
            return null;
        }

        if ($loggingId === null) {
            $loggingDto = (new \App\Models\Dto\Logging())
                ->setName($name)
                ->setAction($action)
                ->setDateTime(Carbon::now())
                ->setUserId(Auth::id());
            $loggingId = (new \App\Models\Logging())->insertEntry($loggingDto);
        }

        $loggingValueDb = new \App\Models\LoggingValue();
        foreach ($loggingValues as $loggingValue) {
            $loggingValue->setLoggingId($loggingId);
            $loggingValueDb->insertEntry($loggingValue);
        }

        return $loggingId;
    }
}
