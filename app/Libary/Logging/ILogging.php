<?php
/**
 * ILogging
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 10.04.2020
 * Time: 09:16
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\Logging;

interface ILogging
{

    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @return array
     */
    public function getVariables(): array;

    /**
     * @return string
     */
    public function getClassName(): string;
}
