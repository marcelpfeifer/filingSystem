<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.06.2021
 * Time: 14:11
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Index;


use App\Models\News;

class Index
{

    public static function getViewDto(): \App\Libary\View\Dto\Index\Index
    {
        $news = (new News())->getNews();
        return (new \App\Libary\View\Dto\Index\Index())
            ->setNews($news);
    }
}
