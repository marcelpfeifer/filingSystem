<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.05.2021
 * Time: 10:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Warrant;


use App\Libary\View\Dto\Warrant\Index\Warrant;
use App\Models\IncidentToProfile;
use App\Models\Profile;

class Index
{
    /**
     * @return \App\Libary\View\Dto\Warrant\Index
     */
    public static function getViewDto(): \App\Libary\View\Dto\Warrant\Index
    {
        $warrants = [];

        $model = new IncidentToProfile();
        foreach ($model->getAllWarrants() as $warrant) {
            $warrants[] = (new Warrant())
                ->setIncidentToProfile(IncidentToProfile::getEntryAsDto($warrant))
                ->setProfile(Profile::getEntryAsDto($warrant->profile));
        }

        return (new \App\Libary\View\Dto\Warrant\Index())
            ->setWarrants($warrants);
    }
}
