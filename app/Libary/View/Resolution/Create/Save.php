<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 30.06.2021
 * Time: 14:15
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Resolution\Create;


use App\Libary\Logging\Logging;
use App\Models\Resolution;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Save
{

    /**
     * @var string
     */
    public const STORAGE_PATH = '/resolution/pdf/';

    /**
     * @param \App\Http\Request\Dto\Resolution\Create\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Resolution\Create\Save $requestDto)
    {
        $dto = (new \App\Models\Dto\Resolution())
            ->setTitle($requestDto->getTitle())
            ->setPath('')
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());

        $resolutionId = (new Resolution())->insertEntry($dto);

        $path = self::STORAGE_PATH . $resolutionId . DIRECTORY_SEPARATOR;

        $requestDto->getFile()->storePubliclyAs(
            'public' . $path,
            $requestDto->getFile()->getClientOriginalName()
        );

        $dto->setId($resolutionId)
            ->setPath('storage' . $path . $requestDto->getFile()->getClientOriginalName());
        (new Resolution())->updateEntry($dto);

        Logging::insert(\App\Models\Resolution::class, $dto);
    }
}
