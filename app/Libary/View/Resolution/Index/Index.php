<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 30.06.2021
 * Time: 12:51
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Resolution\Index;

use App\Models\Dto\Resolution;

class Index
{
    /**
     * @return Resolution[]
     */
    public static function getViewDto(): array
    {
        return \App\Models\Resolution::getEntriesAsDto(\App\Models\Resolution::all());
    }
}
