<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 30.06.2021
 * Time: 13:09
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Resolution\Show;


use App\Models\Dto\Resolution;

class Index
{

    /**
     * @param int $id
     * @return Resolution
     */
    public static function getViewDto(int $id): Resolution
    {
        return \App\Models\Resolution::getEntryAsDto(\App\Models\Resolution::find($id));
    }
}
