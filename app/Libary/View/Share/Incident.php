<?php
/**
 * Incident
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 20:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Share;

use App\Libary\View\Dto\Share\Incident\Incident\Description;
use App\Models\Charge;
use App\Models\IncidentDescription;
use App\Models\IncidentDescriptionChangelog;
use App\Models\IncidentToProfile;
use App\Models\IncidentToProfileCharge;
use App\Models\Profile;
use App\Models\ShareIncident;
use App\Models\User;

class Incident
{
    /**
     * @param string $code
     * @return \App\Libary\View\Dto\Share\Incident|null
     */
    public static function getViewDto(string $code): ?\App\Libary\View\Dto\Share\Incident
    {
        $shareProfile = (new ShareIncident())->getEntryByCode($code);
        if (!$shareProfile) {
            return null;
        }
        $entry = \App\Models\Incident::find($shareProfile->getIncidentId());
        if (!$entry) {
            return null;
        }

        $profiles = [];
        foreach ($entry->profilesToIncident as $incidentToProfile) {
            /**
             * @var $profileDto \App\Models\Dto\Profile
             */
            $profileDto = Profile::getEntryAsDto($incidentToProfile->profile);

            /**
             * @var $incidentToProfileDto \App\Models\Dto\IncidentToProfile
             */
            $incidentToProfileDto = IncidentToProfile::getEntryAsDto($incidentToProfile);

            $charges = [];
            foreach ($incidentToProfile->charges as $incidentToProfileCharge) {
                if (!(isset($charges[$incidentToProfileCharge->{IncidentToProfileCharge::COLUMN_CHARGE_ID}]))) {
                    $chargeDto = Charge::getEntryAsDto($incidentToProfileCharge->charge);
                    $charges[$chargeDto->getId()] = (new \App\Libary\View\Dto\Share\Incident\Profile\Charge())
                        ->setId($chargeDto->getId())
                        ->setName($chargeDto->getName())
                        ->setCount(1);
                } else {
                    $charges[$incidentToProfileCharge->{IncidentToProfileCharge::COLUMN_CHARGE_ID}]
                        ->setCount(
                            $charges[$incidentToProfileCharge->{IncidentToProfileCharge::COLUMN_CHARGE_ID}]->getCount(
                            ) + 1
                        );
                }
            }

            $profiles[] = (new \App\Libary\View\Dto\Share\Incident\Profile())
                ->setId($profileDto->getId())
                ->setName($profileDto->getName())
                ->setImage($profileDto->getImageUrl())
                ->setWarrant($incidentToProfileDto->isWarrant())
                ->setCharges($charges);
        }

        /**
         * @var $incidentDto \App\Models\Dto\Incident
         */
        $incidentDto = \App\Models\Incident::getEntryAsDto($entry);

        $descriptions = [];
        foreach ($entry->descriptions as $description) {
            /**
             * @var $incidentDescriptionDto \App\Models\Dto\IncidentDescription
             */
            $incidentDescriptionDto = IncidentDescription::getEntryAsDto($description);

            $descriptions[] = (new Description())
                ->setDescription(htmlspecialchars_decode($incidentDescriptionDto->getDescription()));
        }

        return (new \App\Libary\View\Dto\Share\Incident())
            ->setIncident(
                (new \App\Libary\View\Dto\Share\Incident\Incident())
                    ->setId($incidentDto->getId())
                    ->setTitle($incidentDto->getTitle())
                    ->setDescriptions($descriptions)
            )
            ->setProfiles($profiles);
    }
}
