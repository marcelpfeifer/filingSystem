<?php
/**
 * Profile
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 20:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Share;


use App\Models\Charge;
use App\Models\IncidentToProfileCharge;
use App\Models\ShareProfile;

class Profile
{
    /**
     * @param string $code
     * @return  \App\Libary\View\Dto\Share\Profile|null
     */
    public static function getViewDto(string $code): ? \App\Libary\View\Dto\Share\Profile
    {
        $shareProfile = (new ShareProfile())->getEntryByCode($code);
        if (!($shareProfile)) {
            return null;
        }
        $entry = \App\Models\Profile::find($shareProfile->getProfileId());
        if (!($entry)) {
            return null;
        }

        /**
         * @var $charges \App\Libary\View\Dto\Share\Profile\Charge[]
         */
        $charges = [];
        foreach ($entry->incidentToProfileCharges as $incidentToProfileCharge) {
            if (!(isset($charges[$incidentToProfileCharge->{IncidentToProfileCharge::COLUMN_CHARGE_ID}]))) {
                $chargeDto = Charge::getEntryAsDto($incidentToProfileCharge->charge);
                $charges[$chargeDto->getId()] = (new \App\Libary\View\Dto\Share\Profile\Charge())
                    ->setId($chargeDto->getId())
                    ->setName($chargeDto->getName())
                    ->setCount(1);
            } else {
                $charges[$incidentToProfileCharge->{IncidentToProfileCharge::COLUMN_CHARGE_ID}]
                    ->setCount(
                        $charges[$incidentToProfileCharge->{IncidentToProfileCharge::COLUMN_CHARGE_ID}]->getCount() + 1
                    );
            }
        }

        $dto = \App\Models\Profile::getEntryAsDto($entry);
        return (new \App\Libary\View\Dto\Share\Profile())
            ->setCharges($charges)
            ->setProfile($dto);
    }
}
