<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 12:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Profile\Show;


use App\Libary\View\Dto\ADto;
use App\Libary\View\Dto\Profile\Show\Index\Charge;
use App\Libary\View\Dto\Profile\Show\Index\PropertyGroup;
use App\Models\Dto\Incident;
use App\Models\Dto\Profile;
use App\Models\Dto\Tag;

class Index extends ADto
{

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var Tag[]
     */
    private $tags = [];

    /**
     * @var Charge[]
     */
    private $charges = [];

    /**
     * @var PropertyGroup[]
     */
    private $propertyGroups = [];

    /**
     * @var Incident[]
     */
    private $latestIncidents = [];

    /**
     * @return Profile
     */
    public function getProfile(): Profile
    {
        return $this->profile;
    }

    /**
     * @param Profile $profile
     * @return Index
     */
    public function setProfile(Profile $profile): Index
    {
        $this->profile = $profile;
        return $this;
    }

    /**
     * @return Tag[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param Tag[] $tags
     * @return Index
     */
    public function setTags(array $tags): Index
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return Charge[]
     */
    public function getCharges(): array
    {
        return $this->charges;
    }

    /**
     * @param Charge[] $charges
     * @return Index
     */
    public function setCharges(array $charges): Index
    {
        $this->charges = $charges;
        return $this;
    }

    /**
     * @return PropertyGroup[]
     */
    public function getPropertyGroups(): array
    {
        return $this->propertyGroups;
    }

    /**
     * @param PropertyGroup[] $propertyGroups
     * @return Index
     */
    public function setPropertyGroups(array $propertyGroups): Index
    {
        $this->propertyGroups = $propertyGroups;
        return $this;
    }

    /**
     * @return Incident[]
     */
    public function getLatestIncidents(): array
    {
        return $this->latestIncidents;
    }

    /**
     * @param Incident[] $latestIncidents
     * @return Index
     */
    public function setLatestIncidents(array $latestIncidents): Index
    {
        $this->latestIncidents = $latestIncidents;
        return $this;
    }
}
