<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.05.2021
 * Time: 10:59
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Profile\Edit;

use App\Libary\View\Dto\ADto;
use App\Libary\View\Dto\Profile\Edit\Index\Charge;
use App\Libary\View\Dto\Profile\Edit\Index\PropertyGroup;
use App\Models\Dto\Profile;
use App\Models\Dto\ProfileProperty;
use App\Models\Dto\Tag;

class Index extends ADto
{

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @var Tag[]
     */
    private $tags = [];

    /**
     * @var Tag[]
     */
    private $profileTags = [];

    /**
     * @var PropertyGroup[]
     */
    private $propertyGroups = [];

    /**
     * @var string[]
     */
    private $properties = [];

    /**
     * @return Profile
     */
    public function getProfile(): Profile
    {
        return $this->profile;
    }

    /**
     * @param Profile $profile
     * @return Index
     */
    public function setProfile(Profile $profile): Index
    {
        $this->profile = $profile;
        return $this;
    }

    /**
     * @return Tag[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param Tag[] $tags
     * @return Index
     */
    public function setTags(array $tags): Index
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return Tag[]
     */
    public function getProfileTags(): array
    {
        return $this->profileTags;
    }

    /**
     * @param Tag[] $profileTags
     * @return Index
     */
    public function setProfileTags(array $profileTags): Index
    {
        $this->profileTags = $profileTags;
        return $this;
    }

    /**
     * @return PropertyGroup[]
     */
    public function getPropertyGroups(): array
    {
        return $this->propertyGroups;
    }

    /**
     * @param PropertyGroup[] $propertyGroups
     * @return Index
     */
    public function setPropertyGroups(array $propertyGroups): Index
    {
        $this->propertyGroups = $propertyGroups;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * @param string[] $properties
     * @return Index
     */
    public function setProperties(array $properties): Index
    {
        $this->properties = $properties;
        return $this;
    }
}
