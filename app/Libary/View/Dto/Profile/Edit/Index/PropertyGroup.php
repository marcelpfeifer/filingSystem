<?php
/**
 * PropertyGroup
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 22.07.2021
 * Time: 16:01
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Profile\Edit\Index;


use App\Libary\View\Dto\Profile\Edit\Index\PropertyGroup\Property;

class PropertyGroup
{

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var Property[]
     */
    private $properties = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return PropertyGroup
     */
    public function setId(int $id): PropertyGroup
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return PropertyGroup
     */
    public function setName(string $name): PropertyGroup
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Property[]
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * @param Property[] $properties
     * @return PropertyGroup
     */
    public function setProperties(array $properties): PropertyGroup
    {
        $this->properties = $properties;
        return $this;
    }
}
