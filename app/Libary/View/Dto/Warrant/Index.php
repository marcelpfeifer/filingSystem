<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.05.2021
 * Time: 10:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Warrant;


use App\Libary\View\Dto\ADto;
use App\Libary\View\Dto\Warrant\Index\Warrant;

class Index extends ADto
{

    /**
     * @var Warrant[]
     */
    private $warrants = [];

    /**
     * @return Warrant[]
     */
    public function getWarrants(): array
    {
        return $this->warrants;
    }

    /**
     * @param Warrant[] $warrants
     * @return Index
     */
    public function setWarrants(array $warrants): Index
    {
        $this->warrants = $warrants;
        return $this;
    }
}
