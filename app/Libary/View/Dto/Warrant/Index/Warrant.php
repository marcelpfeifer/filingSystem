<?php
/**
 * Warrant
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.05.2021
 * Time: 10:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Warrant\Index;


use App\Models\Dto\IncidentToProfile;
use App\Models\Dto\Profile;

class Warrant
{

    /**
     * @var IncidentToProfile
     */
    private $incidentToProfile;

    /**
     * @var Profile
     */
    private $profile;

    /**
     * @return IncidentToProfile
     */
    public function getIncidentToProfile(): IncidentToProfile
    {
        return $this->incidentToProfile;
    }

    /**
     * @param IncidentToProfile $incidentToProfile
     * @return Warrant
     */
    public function setIncidentToProfile(IncidentToProfile $incidentToProfile): Warrant
    {
        $this->incidentToProfile = $incidentToProfile;
        return $this;
    }

    /**
     * @return Profile
     */
    public function getProfile(): Profile
    {
        return $this->profile;
    }

    /**
     * @param Profile $profile
     * @return Warrant
     */
    public function setProfile(Profile $profile): Warrant
    {
        $this->profile = $profile;
        return $this;
    }
}
