<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 16:31
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Incident\Group;


use App\Models\Dto\Group;
use App\Models\Dto\IncidentToGroup;

class Index
{

    /**
     * @var int
     */
    private $incidentId = 0;

    /**
     * @var Group[]
     */
    private $groups = [];

    /**
     * @var IncidentToGroup[]
     */
    private $selectedGroups = [];

    /**
     * @return int
     */
    public function getIncidentId(): int
    {
        return $this->incidentId;
    }

    /**
     * @param int $incidentId
     * @return Index
     */
    public function setIncidentId(int $incidentId): Index
    {
        $this->incidentId = $incidentId;
        return $this;
    }

    /**
     * @return Group[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @param Group[] $groups
     * @return Index
     */
    public function setGroups(array $groups): Index
    {
        $this->groups = $groups;
        return $this;
    }

    /**
     * @return IncidentToGroup[]
     */
    public function getSelectedGroups(): array
    {
        return $this->selectedGroups;
    }

    /**
     * @param IncidentToGroup[] $selectedGroups
     * @return Index
     */
    public function setSelectedGroups(array $selectedGroups): Index
    {
        $this->selectedGroups = $selectedGroups;
        return $this;
    }
}
