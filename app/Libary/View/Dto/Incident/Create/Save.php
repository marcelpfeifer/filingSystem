<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.05.2021
 * Time: 13:40
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Incident\Create;

use App\Http\Response\Dto\Message;
use App\Libary\View\Dto\ADto;
use App\Libary\View\Dto\Incident\Create\Save\Incident;

class Save extends ADto
{
    /**
     * @var Incident
     */
    private $incident;

    /**
     * @var Message[]
     */
    private $messages = [];

    /**
     * @return Incident
     */
    public function getIncident(): Incident
    {
        return $this->incident;
    }

    /**
     * @param Incident $incident
     * @return Save
     */
    public function setIncident(Incident $incident): Save
    {
        $this->incident = $incident;
        return $this;
    }

    /**
     * @return Message[]
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * @param Message[] $messages
     * @return Save
     */
    public function setMessages(array $messages): Save
    {
        $this->messages = $messages;
        return $this;
    }
}
