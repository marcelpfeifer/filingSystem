<?php
/**
 * Changelog
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.06.2021
 * Time: 15:37
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Incident\Create\Save\Incident\Description;

use Carbon\Carbon;

class Changelog
{

    /**
     * @var string
     */
    private $user = '';

    /**
     * @var string
     */
    private $action = '';

    /**
     * @var Carbon
     */
    private $timestamp;

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @param string $user
     * @return Changelog
     */
    public function setUser(string $user): Changelog
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return Changelog
     */
    public function setAction(string $action): Changelog
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getTimestamp(): Carbon
    {
        return $this->timestamp;
    }

    /**
     * @param Carbon $timestamp
     * @return Changelog
     */
    public function setTimestamp(Carbon $timestamp): Changelog
    {
        $this->timestamp = $timestamp;
        return $this;
    }
}
