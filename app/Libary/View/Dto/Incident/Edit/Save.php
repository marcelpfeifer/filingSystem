<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.05.2021
 * Time: 14:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Incident\Edit;


use App\Http\Response\Dto\Message;
use App\Libary\View\Dto\ADto;
use App\Models\Dto\Incident;

class Save extends ADto
{

    /**
     * @var Message[]
     */
    private $messages = [];

    /**
     * @return Message[]
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * @param Message[] $messages
     * @return Save
     */
    public function setMessages(array $messages): Save
    {
        $this->messages = $messages;
        return $this;
    }
}
