<?php
/**
 * Incident
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 17.05.2021
 * Time: 13:40
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Incident\Edit;

use App\Libary\View\Dto\ADto;
use App\Libary\View\Dto\Incident\Edit\Index\Incident;
use App\Models\Dto\Tag;

class Index extends ADto
{

    /**
     * @var Incident
     */
    private $incident;

    /**
     * @var Index\Profile[]
     */
    private $profiles = [];

    /**
     * @var Tag[]
     */
    private $tags = [];

    /**
     * @var Tag[]
     */
    private $incidentTags = [];

    /**
     * @return Incident
     */
    public function getIncident(): Incident
    {
        return $this->incident;
    }

    /**
     * @param Incident $incident
     * @return Index
     */
    public function setIncident(Incident $incident): Index
    {
        $this->incident = $incident;
        return $this;
    }

    /**
     * @return Index\Profile[]
     */
    public function getProfiles(): array
    {
        return $this->profiles;
    }

    /**
     * @param Index\Profile[] $profiles
     * @return Index
     */
    public function setProfiles(array $profiles): Index
    {
        $this->profiles = $profiles;
        return $this;
    }

    /**
     * @return Tag[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param Tag[] $tags
     * @return Index
     */
    public function setTags(array $tags): Index
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return Tag[]
     */
    public function getIncidentTags(): array
    {
        return $this->incidentTags;
    }

    /**
     * @param Tag[] $incidentTags
     * @return Index
     */
    public function setIncidentTags(array $incidentTags): Index
    {
        $this->incidentTags = $incidentTags;
        return $this;
    }
}
