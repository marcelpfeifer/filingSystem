<?php
/**
 * Profile
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 17.05.2021
 * Time: 14:14
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Incident\Edit\Index;


class Profile
{

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $image = '';

    /**
     * @var boolean
     */
    private $warrant = false;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Profile
     */
    public function setId(int $id): Profile
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Profile
     */
    public function setName(string $name): Profile
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Profile
     */
    public function setImage(string $image): Profile
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasWarrant(): bool
    {
        return $this->warrant;
    }

    /**
     * @param bool $warrant
     * @return Profile
     */
    public function setWarrant(bool $warrant): Profile
    {
        $this->warrant = $warrant;
        return $this;
    }
}
