<?php
/**
 * Description
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.06.2021
 * Time: 17:14
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Incident\Edit\Index\Incident;

use App\Libary\View\Dto\Incident\Edit\Index\Incident\Description\Changelog;

class Description
{

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var Changelog[]
     */
    private $changelogs = [];

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Description
     */
    public function setDescription(string $description): Description
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Changelog[]
     */
    public function getChangelogs(): array
    {
        return $this->changelogs;
    }

    /**
     * @param Changelog[] $changelogs
     * @return Description
     */
    public function setChangelogs(array $changelogs): Description
    {
        $this->changelogs = $changelogs;
        return $this;
    }
}
