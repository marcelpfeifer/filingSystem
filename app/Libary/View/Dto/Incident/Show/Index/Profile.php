<?php
/**
 * Profile
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 13:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Incident\Show\Index;


class Profile
{
    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $image = '';

    /**
     * @var boolean
     */
    private $warrant = false;


    /**
     * @var int
     */
    private $time = 0;

    /**
     * @var int
     */
    private $money = 0;

    /**
     * @var Profile\Charge[]
     */
    private $charges = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Profile
     */
    public function setId(int $id): Profile
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Profile
     */
    public function setName(string $name): Profile
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return Profile
     */
    public function setImage(string $image): Profile
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return bool
     */
    public function isWarrant(): bool
    {
        return $this->warrant;
    }

    /**
     * @param bool $warrant
     * @return Profile
     */
    public function setWarrant(bool $warrant): Profile
    {
        $this->warrant = $warrant;
        return $this;
    }

    /**
     * @return int
     */
    public function getTime(): int
    {
        return $this->time;
    }

    /**
     * @param int $time
     * @return Profile
     */
    public function setTime(int $time): Profile
    {
        $this->time = $time;
        return $this;
    }

    /**
     * @return int
     */
    public function getMoney(): int
    {
        return $this->money;
    }

    /**
     * @param int $money
     * @return Profile
     */
    public function setMoney(int $money): Profile
    {
        $this->money = $money;
        return $this;
    }

    /**
     * @return Profile\Charge[]
     */
    public function getCharges(): array
    {
        return $this->charges;
    }

    /**
     * @param Profile\Charge[] $charges
     * @return Profile
     */
    public function setCharges(array $charges): Profile
    {
        $this->charges = $charges;
        return $this;
    }
}
