<?php
/**
 * Incident
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 04.06.2021
 * Time: 17:13
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Incident\Show\Index;

use App\Libary\View\Dto\Incident\Show\Index\Incident\Description;
use Carbon\Carbon;

class Incident
{
    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var Description[]
     */
    private $descriptions = [];

    /**
     * @var Carbon
     */
    private $createdAt;

    /**
     * @var Carbon
     */
    private $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Incident
     */
    public function setId(int $id): Incident
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Incident
     */
    public function setTitle(string $title): Incident
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return Description[]
     */
    public function getDescriptions(): array
    {
        return $this->descriptions;
    }

    /**
     * @param Description[] $descriptions
     * @return Incident
     */
    public function setDescriptions(array $descriptions): Incident
    {
        $this->descriptions = $descriptions;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon $createdAt
     * @return Incident
     */
    public function setCreatedAt(Carbon $createdAt): Incident
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getUpdatedAt(): Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon $updatedAt
     * @return Incident
     */
    public function setUpdatedAt(Carbon $updatedAt): Incident
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
