<?php
/**
 * Share
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 22.05.2021
 * Time: 21:21
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Incident\Show;

use App\Libary\View\Dto\ADto;

class Share extends ADto
{

    /**
     * @var string
     */
    private $code = '';

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Share
     */
    public function setCode(string $code): Share
    {
        $this->code = $code;
        return $this;
    }
}
