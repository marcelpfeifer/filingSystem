<?php
/**
 * Charge
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 17.05.2021
 * Time: 14:46
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Incident\Charge\Index;

class Charge
{

    /**
     * @var string
     */
    private $category = '';

    /**
     * @var \App\Models\Dto\Charge[]
     */
    private $charges = [];

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     * @return Charge
     */
    public function setCategory(string $category): Charge
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return \App\Models\Dto\Charge[]
     */
    public function getCharges(): array
    {
        return $this->charges;
    }

    /**
     * @param \App\Models\Dto\Charge[] $charges
     * @return Charge
     */
    public function setCharges(array $charges): Charge
    {
        $this->charges = $charges;
        return $this;
    }
}
