<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 17.05.2021
 * Time: 14:44
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Incident\Charge;


use App\Libary\View\Dto\ADto;
use App\Models\Dto\Charge;

class Index extends ADto
{

    /**
     * @var int
     */
    private $incidentId = 0;

    /**
     * @var int
     */
    private $profileId = 0;

    /**
     * @var \App\Libary\View\Dto\Incident\Charge\Index\Charge[]
     */
    private $charges = [];

    /**
     * @var Charge[]
     */
    private $selectedCharges = [];

    /**
     * @return int
     */
    public function getIncidentId(): int
    {
        return $this->incidentId;
    }

    /**
     * @param int $incidentId
     * @return Index
     */
    public function setIncidentId(int $incidentId): Index
    {
        $this->incidentId = $incidentId;
        return $this;
    }

    /**
     * @return int
     */
    public function getProfileId(): int
    {
        return $this->profileId;
    }

    /**
     * @param int $profileId
     * @return Index
     */
    public function setProfileId(int $profileId): Index
    {
        $this->profileId = $profileId;
        return $this;
    }

    /**
     * @return Index\Charge[]
     */
    public function getCharges(): array
    {
        return $this->charges;
    }

    /**
     * @param Index\Charge[] $charges
     * @return Index
     */
    public function setCharges(array $charges): Index
    {
        $this->charges = $charges;
        return $this;
    }

    /**
     * @return Charge[]
     */
    public function getSelectedCharges(): array
    {
        return $this->selectedCharges;
    }

    /**
     * @param Charge[] $selectedCharges
     * @return Index
     */
    public function setSelectedCharges(array $selectedCharges): Index
    {
        $this->selectedCharges = $selectedCharges;
        return $this;
    }
}
