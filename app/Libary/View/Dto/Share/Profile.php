<?php
/**
 * Profile
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 20:58
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Share;


use App\Libary\View\Dto\ADto;
use App\Libary\View\Dto\Share\Profile\Charge;

class Profile extends ADto
{
    /**
     * @var \App\Models\Dto\Profile
     */
    private $profile;

    /**
     * @var Charge[]
     */
    private $charges = [];

    /**
     * @return \App\Models\Dto\Profile
     */
    public function getProfile(): \App\Models\Dto\Profile
    {
        return $this->profile;
    }

    /**
     * @param \App\Models\Dto\Profile $profile
     * @return Profile
     */
    public function setProfile(\App\Models\Dto\Profile $profile): Profile
    {
        $this->profile = $profile;
        return $this;
    }

    /**
     * @return Charge[]
     */
    public function getCharges(): array
    {
        return $this->charges;
    }

    /**
     * @param Charge[] $charges
     * @return Profile
     */
    public function setCharges(array $charges): Profile
    {
        $this->charges = $charges;
        return $this;
    }
}
