<?php
/**
 * Incident
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 20:58
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Share;


use App\Libary\View\Dto\ADto;

class Incident extends ADto
{
    /**
     * @var \App\Libary\View\Dto\Share\Incident\Incident
     */
    private $incident;

    /**
     * @var Incident\Profile[]
     */
    private $profiles = [];

    /**
     * @return Incident\Incident
     */
    public function getIncident(): Incident\Incident
    {
        return $this->incident;
    }

    /**
     * @param Incident\Incident $incident
     * @return Incident
     */
    public function setIncident(Incident\Incident $incident): Incident
    {
        $this->incident = $incident;
        return $this;
    }

    /**
     * @return Incident\Profile[]
     */
    public function getProfiles(): array
    {
        return $this->profiles;
    }

    /**
     * @param Incident\Profile[] $profiles
     * @return Incident
     */
    public function setProfiles(array $profiles): Incident
    {
        $this->profiles = $profiles;
        return $this;
    }
}
