<?php
/**
 * Permission
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 25.04.2021
 * Time: 13:17
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Admin\Permission\Edit\Index;

class Permission
{

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var Permission[]
     */
    private $children = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Permission
     */
    public function setId(int $id): Permission
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Permission
     */
    public function setName(string $name): Permission
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Permission
     */
    public function setDescription(string $description): Permission
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Permission[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @param Permission[] $children
     * @return Permission
     */
    public function setChildren(array $children): Permission
    {
        $this->children = $children;
        return $this;
    }
}
