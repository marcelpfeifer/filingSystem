<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 25.04.2021
 * Time: 13:16
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Admin\Permission\Edit;

use App\Libary\View\Dto\Admin\Permission\Edit\Index\Group;
use App\Libary\View\Dto\Admin\Permission\Edit\Index\Permission;
use App\Libary\View\Dto\ADto;

class Index extends ADto
{
    /**
     * @var Group
     */
    private $group;

    /**
     * @var Permission[]
     */
    private $permissions = [];

    /**
     * @var int[]
     */
    private $selectedPermissions = [];

    /**
     * @return Group
     */
    public function getGroup(): Group
    {
        return $this->group;
    }

    /**
     * @param Group $group
     * @return Index
     */
    public function setGroup(Group $group): Index
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return Permission[]
     */
    public function getPermissions(): array
    {
        return $this->permissions;
    }

    /**
     * @param Permission[] $permissions
     * @return Index
     */
    public function setPermissions(array $permissions): Index
    {
        $this->permissions = $permissions;
        return $this;
    }

    /**
     * @return int[]
     */
    public function getSelectedPermissions(): array
    {
        return $this->selectedPermissions;
    }

    /**
     * @param int[] $selectedPermissions
     * @return Index
     */
    public function setSelectedPermissions(array $selectedPermissions): Index
    {
        $this->selectedPermissions = $selectedPermissions;
        return $this;
    }
}
