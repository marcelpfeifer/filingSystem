<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 00:54
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Admin\ProfileProperty\Edit;


use App\Models\Dto\ProfileProperty;
use App\Models\Dto\ProfilePropertyGroup;

class Index
{

    /**
     * @var ProfileProperty
     */
    private $profileProperty;

    /**
     * @var ProfilePropertyGroup[]
     */
    private $groups;

    /**
     * @return ProfileProperty
     */
    public function getProfileProperty(): ProfileProperty
    {
        return $this->profileProperty;
    }

    /**
     * @param ProfileProperty $profileProperty
     * @return Index
     */
    public function setProfileProperty(ProfileProperty $profileProperty): Index
    {
        $this->profileProperty = $profileProperty;
        return $this;
    }

    /**
     * @return ProfilePropertyGroup[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @param ProfilePropertyGroup[] $groups
     * @return Index
     */
    public function setGroups(array $groups): Index
    {
        $this->groups = $groups;
        return $this;
    }
}
