<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 10.05.2021
 * Time: 09:06
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Admin\ProfileProperty\Index;

use App\Libary\View\Dto\ADto;
use App\Models\Dto\ProfileProperty;
use App\Models\Dto\ProfilePropertyGroup;

class Search extends ADto
{

    /**
     * @var ProfileProperty[]
     */
    private $profileProperties = [];

    /**
     * @return ProfileProperty[]
     */
    public function getProfileProperties(): array
    {
        return $this->profileProperties;
    }

    /**
     * @param ProfileProperty[] $profileProperties
     * @return Search
     */
    public function setProfileProperties(array $profileProperties): Search
    {
        $this->profileProperties = $profileProperties;
        return $this;
    }
}
