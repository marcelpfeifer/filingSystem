<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 14:31
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Admin\User\Create;


use App\Models\Dto\Group;
use App\Models\Dto\PermissionGroup;

class Index
{

    /**
     * @var PermissionGroup[]
     */
    private $permissionGroups = [];

    /**
     * @var Group[]
     */
    private $groups = [];

    /**
     * @return PermissionGroup[]
     */
    public function getPermissionGroups(): array
    {
        return $this->permissionGroups;
    }

    /**
     * @param PermissionGroup[] $permissionGroups
     * @return Index
     */
    public function setPermissionGroups(array $permissionGroups): Index
    {
        $this->permissionGroups = $permissionGroups;
        return $this;
    }

    /**
     * @return Group[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @param Group[] $groups
     * @return Index
     */
    public function setGroups(array $groups): Index
    {
        $this->groups = $groups;
        return $this;
    }
}
