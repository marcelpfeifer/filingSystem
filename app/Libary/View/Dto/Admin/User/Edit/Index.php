<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 06.05.2021
 * Time: 14:01
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Admin\User\Edit;


use App\Models\Dto\Group;
use App\Models\Dto\PermissionGroup;
use App\Models\Dto\User;

class Index
{

    /**
     * @var User
     */
    private $user;

    /**
     * @var PermissionGroup[]
     */
    private $permissionGroups = [];

    /**
     * @var Group[]
     */
    private $groups = [];

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Index
     */
    public function setUser(User $user): Index
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return PermissionGroup[]
     */
    public function getPermissionGroups(): array
    {
        return $this->permissionGroups;
    }

    /**
     * @param PermissionGroup[] $permissionGroups
     * @return Index
     */
    public function setPermissionGroups(array $permissionGroups): Index
    {
        $this->permissionGroups = $permissionGroups;
        return $this;
    }

    /**
     * @return Group[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * @param Group[] $groups
     * @return Index
     */
    public function setGroups(array $groups): Index
    {
        $this->groups = $groups;
        return $this;
    }
}
