<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.06.2021
 * Time: 13:21
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Admin\News\Edit;


use App\Models\Dto\News;

class Index
{

    /**
     * @var News
     */
    private $news;

    /**
     * @return News
     */
    public function getNews(): News
    {
        return $this->news;
    }

    /**
     * @param News $news
     * @return Index
     */
    public function setNews(News $news): Index
    {
        $this->news = $news;
        return $this;
    }
}
