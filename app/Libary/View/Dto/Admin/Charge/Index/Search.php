<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 10.05.2021
 * Time: 09:06
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Admin\Charge\Index;

use App\Libary\View\Dto\ADto;
use App\Models\Dto\Charge;
use App\Models\Dto\ChargeGroup;

class Search extends ADto
{

    /**
     * @var Charge[]
     */
    private $charges = [];

    /**
     * @var ChargeGroup[]
     */
    private $chargeGroups = [];

    /**
     * @return Charge[]
     */
    public function getCharges(): array
    {
        return $this->charges;
    }

    /**
     * @param Charge[] $charges
     * @return Search
     */
    public function setCharges(array $charges): Search
    {
        $this->charges = $charges;
        return $this;
    }

    /**
     * @return ChargeGroup[]
     */
    public function getChargeGroups(): array
    {
        return $this->chargeGroups;
    }

    /**
     * @param ChargeGroup[] $chargeGroups
     * @return Search
     */
    public function setChargeGroups(array $chargeGroups): Search
    {
        $this->chargeGroups = $chargeGroups;
        return $this;
    }
}
