<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 00:54
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Admin\Charge\Edit;


use App\Models\Dto\Charge;
use App\Models\Dto\ChargeGroup;

class Index
{

    /**
     * @var Charge
     */
    private $charge;

    /**
     * @var ChargeGroup[]
     */
    private $chargeGroups;

    /**
     * @return Charge
     */
    public function getCharge(): Charge
    {
        return $this->charge;
    }

    /**
     * @param Charge $charge
     * @return Index
     */
    public function setCharge(Charge $charge): Index
    {
        $this->charge = $charge;
        return $this;
    }

    /**
     * @return ChargeGroup[]
     */
    public function getChargeGroups(): array
    {
        return $this->chargeGroups;
    }

    /**
     * @param ChargeGroup[] $chargeGroups
     * @return Index
     */
    public function setChargeGroups(array $chargeGroups): Index
    {
        $this->chargeGroups = $chargeGroups;
        return $this;
    }
}
