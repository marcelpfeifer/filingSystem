<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.06.2021
 * Time: 14:11
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\Index;

use App\Libary\View\Dto\ADto;
use App\Models\Dto\News;

class Index extends ADto
{

    /**
     * @var News[]
     */
    private $news = [];

    /**
     * @return News[]
     */
    public function getNews(): array
    {
        return $this->news;
    }

    /**
     * @param News[] $news
     * @return Index
     */
    public function setNews(array $news): Index
    {
        $this->news = $news;
        return $this;
    }
}
