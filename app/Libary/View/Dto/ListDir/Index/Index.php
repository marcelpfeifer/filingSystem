<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.07.2021
 * Time: 14:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\ListDir\Index;


use App\Libary\View\Dto\ADto;
use App\Models\Dto\ListModel;

class Index extends ADto
{

    /**
     * @var ListModel[]
     */
    private $lists = [];

    /**
     * @return ListModel[]
     */
    public function getLists(): array
    {
        return $this->lists;
    }

    /**
     * @param ListModel[] $lists
     * @return Index
     */
    public function setLists(array $lists): Index
    {
        $this->lists = $lists;
        return $this;
    }
}
