<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.07.2021
 * Time: 13:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\ListDir\Entry\Create;

use App\Libary\View\Dto\ADto;
use App\Models\Dto\ListHeader;

class Index extends ADto
{

    /**
     * @var int
     */
    private $listId = 0;

    /**
     * @var ListHeader[]
     */
    private $headers = [];

    /**
     * @return int
     */
    public function getListId(): int
    {
        return $this->listId;
    }

    /**
     * @param int $listId
     * @return Index
     */
    public function setListId(int $listId): Index
    {
        $this->listId = $listId;
        return $this;
    }

    /**
     * @return ListHeader[]
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param ListHeader[] $headers
     * @return Index
     */
    public function setHeaders(array $headers): Index
    {
        $this->headers = $headers;
        return $this;
    }
}
