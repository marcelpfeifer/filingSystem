<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.07.2021
 * Time: 13:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\ListDir\Entry\Edit;

use App\Libary\View\Dto\ADto;
use App\Models\Dto\ListHeader;

class Index extends ADto
{

    /**
     * @var int
     */
    private $entryId = 0;

    /**
     * @var int
     */
    private $listId = 0;

    /**
     * @var ListHeader[]
     */
    private $headers = [];

    /**
     * @var string[]
     */
    private $values = [];

    /**
     * @return int
     */
    public function getEntryId(): int
    {
        return $this->entryId;
    }

    /**
     * @param int $entryId
     * @return Index
     */
    public function setEntryId(int $entryId): Index
    {
        $this->entryId = $entryId;
        return $this;
    }

    /**
     * @return int
     */
    public function getListId(): int
    {
        return $this->listId;
    }

    /**
     * @param int $listId
     * @return Index
     */
    public function setListId(int $listId): Index
    {
        $this->listId = $listId;
        return $this;
    }

    /**
     * @return ListHeader[]
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param ListHeader[] $headers
     * @return Index
     */
    public function setHeaders(array $headers): Index
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @param string[] $values
     * @return Index
     */
    public function setValues(array $values): Index
    {
        $this->values = $values;
        return $this;
    }
}
