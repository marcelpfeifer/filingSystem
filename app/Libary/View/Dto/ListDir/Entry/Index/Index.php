<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.07.2021
 * Time: 14:59
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\ListDir\Entry\Index;


use App\Libary\View\Dto\ADto;

class Index extends ADto
{

    /**
     * @var int
     */
    private $listId = 0;

    /**
     * @var string[]
     */
    private $headers = [];

    /**
     * @return int
     */
    public function getListId(): int
    {
        return $this->listId;
    }

    /**
     * @param int $listId
     * @return Index
     */
    public function setListId(int $listId): Index
    {
        $this->listId = $listId;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param string[] $headers
     * @return Index
     */
    public function setHeaders(array $headers): Index
    {
        $this->headers = $headers;
        return $this;
    }
}
