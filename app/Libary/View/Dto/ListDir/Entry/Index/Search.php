<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.07.2021
 * Time: 15:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Dto\ListDir\Entry\Index;


use App\Libary\View\Dto\ADto;

class Search extends ADto
{

    /**
     * @var array
     */
    private $entries = [];

    /**
     * @return array
     */
    public function getEntries(): array
    {
        return $this->entries;
    }

    /**
     * @param array $entries
     * @return Search
     */
    public function setEntries(array $entries): Search
    {
        $this->entries = $entries;
        return $this;
    }
}
