<?php
/**
 * AddUser
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 17.05.2021
 * Time: 13:06
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Incident\Edit;


use App\Libary\Logging\Logging;
use App\Models\Dto\IncidentToProfile;
use Carbon\Carbon;

class AddUser
{

    /**
     * @param \App\Http\Request\Dto\Incident\Edit\AddUser $requestDto
     */
    public static function save(\App\Http\Request\Dto\Incident\Edit\AddUser $requestDto)
    {
        $dto = (new IncidentToProfile())
            ->setProfileId($requestDto->getProfileId())
            ->setIncidentId($requestDto->getId())
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now())
            ->setWarrant(false);
        (new \App\Models\IncidentToProfile())->insertEntry($dto);

        Logging::insert(\App\Models\IncidentToProfile::class, $dto);
    }
}
