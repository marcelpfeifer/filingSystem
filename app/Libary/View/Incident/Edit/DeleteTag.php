<?php
/**
 * DeleteTag
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.07.2021
 * Time: 13:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Incident\Edit;


use App\Libary\Logging\Logging;
use App\Models\Dto\IncidentToProfile;
use App\Models\Dto\IncidentToTag;

class DeleteTag
{
    /**
     * @param \App\Http\Request\Dto\Incident\Edit\DeleteTag $requestDto
     */
    public static function delete(\App\Http\Request\Dto\Incident\Edit\DeleteTag $requestDto)
    {
        $dto = (new IncidentToTag())
            ->setTagId($requestDto->getTagId())
            ->setIncidentId($requestDto->getId());
        (new \App\Models\IncidentToTag())->deleteEntryByIncidentIdAndTagId($dto);

        Logging::delete(\App\Models\IncidentToTag::class, $dto);
    }
}
