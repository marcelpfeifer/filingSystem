<?php
/**
 * AddUser
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 17.05.2021
 * Time: 13:06
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Incident\Edit;

use App\Libary\Logging\Logging;
use App\Models\Dto\IncidentToProfile;
use Carbon\Carbon;

class DeleteUser
{

    /**
     * @param \App\Http\Request\Dto\Incident\Edit\DeleteUser $requestDto
     */
    public static function delete(\App\Http\Request\Dto\Incident\Edit\DeleteUser $requestDto)
    {
        $dto = (new IncidentToProfile())
            ->setProfileId($requestDto->getProfileId())
            ->setIncidentId($requestDto->getId());
        (new \App\Models\IncidentToProfile())->deleteEntryByIncidentIdAndProfileId($dto);

        Logging::delete(\App\Models\IncidentToProfile::class, $dto);
    }
}
