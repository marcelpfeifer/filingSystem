<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 17.05.2021
 * Time: 13:40
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Incident\Edit;


use App\Libary\View\Dto\Incident\Edit\Index\Incident\Description;
use App\Models\Incident;
use App\Models\IncidentDescription;
use App\Models\IncidentDescriptionChangelog;
use App\Models\IncidentToProfile;
use App\Models\Profile;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class Index
{

    /**
     * @param int $id
     * @return \App\Libary\View\Dto\Incident\Edit\Index|null
     */
    public static function getViewDto(int $id): ?\App\Libary\View\Dto\Incident\Edit\Index
    {
        $entry = Incident::find($id);
        if (!$entry) {
            return null;
        }

        /**
         * @var $incidentDto \App\Models\Dto\Incident
         */
        $incidentDto = Incident::getEntryAsDto($entry);

        $tags = (new Tag())->search(null, true);

        return (new \App\Libary\View\Dto\Incident\Edit\Index())
            ->setIncident(
                (new \App\Libary\View\Dto\Incident\Edit\Index\Incident())
                    ->setId($incidentDto->getId())
                    ->setTitle($incidentDto->getTitle())
                    ->setDescriptions(self::getDescriptions($entry))
            )
            ->setProfiles(self::getProfiles($entry))
            ->setTags($tags)
            ->setIncidentTags(self::getIncidentTags($entry));
    }

    /**
     * @param Incident $entry
     * @return \App\Models\Dto\Tag[]
     */
    private static function getIncidentTags(Incident $entry): array
    {
        $incidentTags = [];

        foreach ($entry->tags as $incidentTag) {
            $incidentTags[] = Tag::getEntryAsDto($incidentTag->tag);
        }

        return $incidentTags;
    }

    /**
     * @param Incident $entry
     * @return Description[]
     */
    private static function getDescriptions(Incident $entry): array
    {
        $descriptions = [];
        foreach ($entry->descriptions as $description) {
            /**
             * @var $incidentDescriptionDto \App\Models\Dto\IncidentDescription
             */
            $incidentDescriptionDto = IncidentDescription::getEntryAsDto($description);

            $changelogs = [];
            foreach ($description->changelogs as $changelog) {
                /**
                 * @var $incidentDescriptionChangelogDto \App\Models\Dto\IncidentDescriptionChangelog
                 */
                $incidentDescriptionChangelogDto = IncidentDescriptionChangelog::getEntryAsDto($changelog);

                $changelogs[] = (new Description\Changelog())
                    ->setAction($incidentDescriptionChangelogDto->getAction())
                    ->setUser(User::find($incidentDescriptionChangelogDto->getUserId())->name)
                    ->setTimestamp($incidentDescriptionChangelogDto->getCreatedAt());
            }

            $descriptions[] = (new Description())
                ->setDescription(htmlspecialchars_decode($incidentDescriptionDto->getDescription()))
                ->setChangelogs($changelogs);
        }
        return $descriptions;
    }

    /**
     * @param Incident $entry
     * @return \App\Libary\View\Dto\Incident\Edit\Index\Profile[]
     */
    private static function getProfiles(Incident $entry): array
    {
        $profiles = [];
        foreach ($entry->profilesToIncident as $incidentToProfile) {
            /**
             * @var $profileDto \App\Models\Dto\Profile
             */
            $profileDto = Profile::getEntryAsDto($incidentToProfile->profile);

            /**
             * @var $incidentToProfileDto \App\Models\Dto\IncidentToProfile
             */
            $incidentToProfileDto = IncidentToProfile::getEntryAsDto($incidentToProfile);

            $profiles[] = (new \App\Libary\View\Dto\Incident\Edit\Index\Profile())
                ->setId($profileDto->getId())
                ->setName($profileDto->getName())
                ->setImage($profileDto->getImageUrl())
                ->setWarrant($incidentToProfileDto->isWarrant());
        }
        return $profiles;
    }
}
