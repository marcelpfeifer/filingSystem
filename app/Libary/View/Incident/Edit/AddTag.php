<?php
/**
 * AddTag
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.07.2021
 * Time: 13:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Incident\Edit;


use App\Libary\Logging\Logging;
use App\Models\Dto\IncidentToTag;

class AddTag
{
    /**
     * @param \App\Http\Request\Dto\Incident\Edit\AddTag $requestDto
     */
    public static function save(\App\Http\Request\Dto\Incident\Edit\AddTag $requestDto)
    {
        $dto = (new IncidentToTag())
            ->setTagId($requestDto->getTagId())
            ->setIncidentId($requestDto->getId());

        (new \App\Models\IncidentToTag())->insertEntry($dto);

        Logging::insert(\App\Models\IncidentToTag::class, $dto);
    }
}
