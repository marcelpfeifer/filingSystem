<?php
/**
 * Warrant
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 18.05.2021
 * Time: 18:48
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Incident\Edit;

use App\Models\Dto\IncidentToProfile;

class Warrant
{
    /**
     * @param \App\Http\Request\Dto\Incident\Edit\Warrant $requestDto
     */
    public static function update(\App\Http\Request\Dto\Incident\Edit\Warrant $requestDto)
    {
        $dto = (new IncidentToProfile())
            ->setProfileId($requestDto->getProfileId())
            ->setIncidentId($requestDto->getId());
        $entry = (new \App\Models\IncidentToProfile())->getEntryByIncidentIdAndProfileId($dto);
        /**
         * @var $dto IncidentToProfile
         */
        $dto = \App\Models\IncidentToProfile::getEntryAsDto($entry);
        $dto->setWarrant($requestDto->isWarrant());

        (new \App\Models\IncidentToProfile())->updateEntry($dto);
    }
}
