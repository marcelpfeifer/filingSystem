<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.05.2021
 * Time: 14:06
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Incident\Edit;


use App\Http\Response\Dto\Message;
use App\Http\Response\Enum\Message\Status;
use App\Libary\Logging\Logging;
use App\Models\Dto\Incident;
use App\Models\Dto\IncidentDescriptionChangelog;
use App\Models\Enum\IncidentDescriptionChangelog\Action;
use App\Models\IncidentDescription;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Save
{
    /**
     * @param \App\Http\Request\Dto\Incident\Edit\Save $requestDto
     * @return \App\Libary\View\Dto\Incident\Edit\Save
     */
    public static function save(\App\Http\Request\Dto\Incident\Edit\Save $requestDto
    ): \App\Libary\View\Dto\Incident\Edit\Save {
        $oldEntry = \App\Models\Incident::find($requestDto->getId());
        $oldDto = \App\Models\Incident::getEntryAsDto($oldEntry);

        $dto = (new Incident())
            ->setId($requestDto->getId())
            ->setTitle($requestDto->getTitle())
            ->setUpdatedAt(Carbon::now());
        (new \App\Models\Incident())->updateEntry($dto);

        $loggingId = Logging::update(\App\Models\Incident::class, $oldDto, $dto);

        /**
         * @var $oldDescriptions \App\Models\Dto\IncidentDescription[]
         */
        $oldDescriptions = IncidentDescription::getEntriesAsDto($oldEntry->descriptions);

        foreach ($requestDto->getDescriptions() as $index => $description) {
            $htmlDescription = htmlspecialchars($description->getDescription(),ENT_QUOTES);
            if (isset($oldDescriptions[$index])) {
                // Update Entry
                if ($oldDescriptions[$index]->getDescription() !== $htmlDescription) {
                    if (strip_tags($description->getDescription())) {
                        $dto = (new \App\Models\Dto\IncidentDescription())
                            ->setId($oldDescriptions[$index]->getId())
                            ->setIncidentId($requestDto->getId())
                            ->setDescription($htmlDescription)
                            ->setUpdatedAt(Carbon::now());
                        (new IncidentDescription())->updateEntry($dto);
                        Logging::update(\App\Models\Incident::class, $dto, $oldDescriptions[$index], $loggingId);

                        $incidentDescriptionChangelogDto = (new IncidentDescriptionChangelog())
                            ->setIncidentDescriptionId($oldDescriptions[$index]->getId())
                            ->setUserId(Auth::id())
                            ->setAction(Action::UPDATE)
                            ->setCreatedAt(Carbon::now())
                            ->setUpdatedAt(Carbon::now());
                        (new \App\Models\IncidentDescriptionChangelog())->insertEntry($incidentDescriptionChangelogDto);
                    } else {
                        $dto = (new \App\Models\Dto\IncidentDescription())
                            ->setId($oldDescriptions[$index]->getId());
                        (new IncidentDescription())->deleteEntry($dto);
                        Logging::delete(\App\Models\Incident::class, $dto, $loggingId);

                        $incidentDescriptionChangelogDto = (new IncidentDescriptionChangelog())
                            ->setIncidentDescriptionId($oldDescriptions[$index]->getId())
                            ->setUserId(Auth::id())
                            ->setAction(Action::DELETE)
                            ->setCreatedAt(Carbon::now())
                            ->setUpdatedAt(Carbon::now());
                        (new \App\Models\IncidentDescriptionChangelog())->insertEntry($incidentDescriptionChangelogDto);
                    }
                }
            } else {
                // Insert Entry
                $dto = (new \App\Models\Dto\IncidentDescription())
                    ->setIncidentId($requestDto->getId())
                    ->setDescription($htmlDescription)
                    ->setCreatedAt(Carbon::now())
                    ->setUpdatedAt(Carbon::now());
                $incidentDescriptionId = (new IncidentDescription())->insertEntry($dto);

                Logging::insert(\App\Models\Incident::class, $dto, $loggingId);

                $incidentDescriptionChangelogDto = (new IncidentDescriptionChangelog())
                    ->setIncidentDescriptionId($incidentDescriptionId)
                    ->setUserId(Auth::id())
                    ->setAction(Action::INSERT)
                    ->setCreatedAt(Carbon::now())
                    ->setUpdatedAt(Carbon::now());
                (new \App\Models\IncidentDescriptionChangelog())->insertEntry($incidentDescriptionChangelogDto);
            }
        }


        return (new \App\Libary\View\Dto\Incident\Edit\Save())
            ->setMessages(
                [
                    (new Message())
                        ->setStatus(Status::SUCCESS)
                        ->setText('Erfolgreich gespeichert!')
                ]
            );
    }
}
