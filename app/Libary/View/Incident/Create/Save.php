<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.05.2021
 * Time: 13:39
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Incident\Create;


use App\Http\Response\Dto\Message;
use App\Http\Response\Enum\Message\Status;
use App\Libary\Logging\Logging;
use App\Libary\View\Dto\Incident\Create\Save\Incident\Description;
use App\Models\Dto\Incident;
use App\Models\Dto\IncidentDescription;
use App\Models\Dto\IncidentDescriptionChangelog;
use App\Models\Dto\IncidentToGroup;
use App\Models\Enum\IncidentDescriptionChangelog\Action;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Incident\Create\Save $requestDto
     * @return \App\Libary\View\Dto\Incident\Create\Save
     */
    public static function save(\App\Http\Request\Dto\Incident\Create\Save $requestDto
    ): \App\Libary\View\Dto\Incident\Create\Save {
        $dto = (new Incident())
            ->setTitle($requestDto->getTitle())
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        $id = (new \App\Models\Incident())->insertEntry($dto);

        $loggingId = Logging::insert(\App\Models\Incident::class, $dto);
        $incidentDescriptionDto = (new IncidentDescription())
            ->setIncidentId($id)
            ->setDescription(htmlspecialchars($requestDto->getDescription(), ENT_QUOTES))
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        $incidentDescriptionId = (new \App\Models\IncidentDescription())->insertEntry($incidentDescriptionDto);
        Logging::insert(\App\Models\IncidentDescription::class, $incidentDescriptionDto, $loggingId);

        $incidentGroup = (new IncidentToGroup())
            ->setIncidentId($id)
            ->setGroupId(Auth::user()->{User::COLUMN_GROUP_ID})
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        (new \App\Models\IncidentToGroup())->insertEntry($incidentGroup);

        $incidentDescriptionChangelogDto = (new IncidentDescriptionChangelog())
            ->setIncidentDescriptionId($incidentDescriptionId)
            ->setUserId(Auth::id())
            ->setAction(Action::INSERT)
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        (new \App\Models\IncidentDescriptionChangelog())->insertEntry($incidentDescriptionChangelogDto);

        $dto->setId($id);

        return (new \App\Libary\View\Dto\Incident\Create\Save())
            ->setIncident(
                (new \App\Libary\View\Dto\Incident\Create\Save\Incident())
                    ->setId($dto->getId())
                    ->setTitle($dto->getTitle())
                    ->setDescriptions(
                        [
                            (new Description())
                                ->setDescription($requestDto->getDescription())
                                ->setChangelogs(
                                    [
                                        (new Description\Changelog())
                                            ->setAction($incidentDescriptionChangelogDto->getAction())
                                            ->setUser(Auth::user()->{User::COLUMN_NAME})
                                            ->setTimestamp($incidentDescriptionChangelogDto->getCreatedAt())
                                    ]
                                )
                        ]
                    )
            )
            ->setMessages(
                [
                    (new Message())
                        ->setStatus(Status::SUCCESS)
                        ->setText('Erfolgreich gespeichert!')
                ]
            );
    }
}
