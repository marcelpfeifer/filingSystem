<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.07.2021
 * Time: 13:51
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Incident\Index;

use App\Models\Tag;

class Index
{
    /**
     * @return \App\Libary\View\Dto\Incident\Index\Index
     */
    public static function getViewDto(): \App\Libary\View\Dto\Incident\Index\Index
    {
        $tags = (new Tag())->search(null, true);
        return (new \App\Libary\View\Dto\Incident\Index\Index())
            ->setTags($tags);
    }
}
