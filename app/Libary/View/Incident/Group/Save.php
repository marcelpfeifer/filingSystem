<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 09.07.2021
 * Time: 14:47
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Incident\Group;

use App\Models\IncidentToGroup;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Incident\Group\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Incident\Group\Save $requestDto)
    {
        $model = new IncidentToGroup();
        $dto = (new \App\Models\Dto\IncidentToGroup())
            ->setIncidentId($requestDto->getIncidentId());
        $model->deleteEntries($dto);

        foreach ($requestDto->getSelectedGroups() as $groupId) {
            $dto->setGroupId($groupId);
            $model->insertEntry($dto);
        }
    }
}
