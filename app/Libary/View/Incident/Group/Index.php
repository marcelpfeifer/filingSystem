<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 16:28
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Incident\Group;


use App\Models\Group;
use App\Models\Incident;
use App\Models\IncidentToGroup;

class Index
{
    /**
     * @param int $incidentId
     * @param int $profileId
     * @return \App\Libary\View\Dto\Incident\Group\Index
     */
    public static function getViewDto(int $incidentId): \App\Libary\View\Dto\Incident\Group\Index
    {
        return (new \App\Libary\View\Dto\Incident\Group\Index())
            ->setIncidentId($incidentId)
            ->setGroups(Group::getEntriesAsDto(Group::all()))
            ->setSelectedGroups(IncidentToGroup::getEntriesAsDto(Incident::find($incidentId)->groupsToIncident));
    }
}
