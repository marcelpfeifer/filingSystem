<?php
/**
 * Share
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 22.05.2021
 * Time: 21:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Incident\Show;

use App\Libary\Common\RandomString;
use App\Libary\Logging\Logging;
use App\Models\ShareIncident;
use Carbon\Carbon;

class Share
{

    /**
     * @param int $incidentId
     * @return \App\Libary\View\Dto\Incident\Show\Share
     */
    public static function share(int $incidentId): \App\Libary\View\Dto\Incident\Show\Share
    {
        $model = new ShareIncident();
        while (true) {
            $code = RandomString::generate(8);
            if (!$model->getEntryByCode($code)) {
                break;
            }
        }
        $dto = (new \App\Models\Dto\ShareIncident())
            ->setCode($code)
            ->setIncidentId($incidentId)
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        $model->insertEntry($dto);

        Logging::insert(ShareIncident::class, $dto);

        return (new \App\Libary\View\Dto\Incident\Show\Share())
            ->setCode($code);
    }
}
