<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 13:07
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Incident\Show;


use App\Libary\View\Dto\Incident\Edit\Index\Incident\Description;
use App\Models\Charge;
use App\Models\Incident;
use App\Models\IncidentDescription;
use App\Models\IncidentDescriptionChangelog;
use App\Models\IncidentToProfile;
use App\Models\IncidentToProfileCharge;
use App\Models\Profile;
use App\Models\Tag;
use App\Models\User;

class Index
{
    /**
     * @param int $id
     * @return \App\Libary\View\Dto\Incident\Show\Index|null
     */
    public static function getViewDto(int $id): ?\App\Libary\View\Dto\Incident\Show\Index
    {
        $entry = Incident::find($id);
        if (!$entry) {
            return null;
        }

        $profiles = [];
        foreach ($entry->profilesToIncident as $incidentToProfile) {
            /**
             * @var $profileDto \App\Models\Dto\Profile
             */
            $profileDto = Profile::getEntryAsDto($incidentToProfile->profile);

            /**
             * @var $incidentToProfileDto \App\Models\Dto\IncidentToProfile
             */
            $incidentToProfileDto = IncidentToProfile::getEntryAsDto($incidentToProfile);

            $charges = [];
            $time = 0;
            $money = 0;
            foreach ($incidentToProfile->charges as $incidentToProfileCharge) {
                $chargeDto = Charge::getEntryAsDto($incidentToProfileCharge->charge);
                $money += $chargeDto->getMoney();
                $time += $chargeDto->getTime();

                if (!(isset($charges[$incidentToProfileCharge->{IncidentToProfileCharge::COLUMN_CHARGE_ID}]))) {
                    $charges[$chargeDto->getId()] = (new \App\Libary\View\Dto\Incident\Show\Index\Profile\Charge())
                        ->setId($chargeDto->getId())
                        ->setName($chargeDto->getName())
                        ->setCount(1);
                } else {
                    $charges[$incidentToProfileCharge->{IncidentToProfileCharge::COLUMN_CHARGE_ID}]
                        ->setCount(
                            $charges[$incidentToProfileCharge->{IncidentToProfileCharge::COLUMN_CHARGE_ID}]->getCount(
                            ) + 1
                        );
                }
            }

            $profiles[] = (new \App\Libary\View\Dto\Incident\Show\Index\Profile())
                ->setId($profileDto->getId())
                ->setName($profileDto->getName())
                ->setImage($profileDto->getImageUrl())
                ->setMoney($money)
                ->setTime($time)
                ->setWarrant($incidentToProfileDto->isWarrant())
                ->setCharges($charges);
        }

        /**
         * @var $incidentDto \App\Models\Dto\Incident
         */
        $incidentDto = Incident::getEntryAsDto($entry);

        $descriptions = [];
        foreach ($entry->descriptions as $description) {
            /**
             * @var $incidentDescriptionDto \App\Models\Dto\IncidentDescription
             */
            $incidentDescriptionDto = IncidentDescription::getEntryAsDto($description);

            $changelogs = [];
            foreach ($description->changelogs as $changelog) {
                /**
                 * @var $incidentDescriptionChangelogDto \App\Models\Dto\IncidentDescriptionChangelog
                 */
                $incidentDescriptionChangelogDto = IncidentDescriptionChangelog::getEntryAsDto($changelog);

                $changelogs[] = (new Description\Changelog())
                    ->setAction($incidentDescriptionChangelogDto->getAction())
                    ->setUser(User::find($incidentDescriptionChangelogDto->getUserId())->name)
                    ->setTimestamp($incidentDescriptionChangelogDto->getCreatedAt());
            }

            $descriptions[] = (new Description())
                ->setDescription($incidentDescriptionDto->getDescription())
                ->setChangelogs($changelogs);
        }

        return (new \App\Libary\View\Dto\Incident\Show\Index())
            ->setIncident(
                (new \App\Libary\View\Dto\Incident\Show\Index\Incident())
                    ->setId($incidentDto->getId())
                    ->setTitle($incidentDto->getTitle())
                    ->setDescriptions($descriptions)
                    ->setCreatedAt($incidentDto->getCreatedAt())
                    ->setUpdatedAt($incidentDto->getUpdatedAt())
            )
            ->setProfiles($profiles)
            ->setTags(self::getIncidentTags($entry));
    }

    /**
     * @param Incident $entry
     * @return \App\Models\Dto\Tag[]
     */
    private static function getIncidentTags(Incident $entry): array
    {
        $incidentTags = [];

        foreach ($entry->tags as $incidentTag) {
            $incidentTags[] = Tag::getEntryAsDto($incidentTag->tag);
        }

        return $incidentTags;
    }
}
