<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 18.05.2021
 * Time: 18:21
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Incident\Charge;

use App\Models\IncidentToProfile;
use App\Models\IncidentToProfileCharge;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Incident\Charge\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Incident\Charge\Save $requestDto)
    {
        $incidentToProfileDto = (new \App\Models\Dto\IncidentToProfile())
            ->setProfileId($requestDto->getProfileId())
            ->setIncidentId($requestDto->getIncidentId());
        $incidentToProfileEntry = (new IncidentToProfile())->getEntryByIncidentIdAndProfileId($incidentToProfileDto);
        $incidentToProfileDto = IncidentToProfile::getEntryAsDto($incidentToProfileEntry);

        $dto = (new \App\Models\Dto\IncidentToProfileCharge())
            ->setIncidentToProfileId($incidentToProfileDto->getId());

        // Delete Old Entries
        $model = new IncidentToProfileCharge();
        $model->deleteEntries($dto);

        foreach ($requestDto->getCharges() as $charge) {
            $dto->setChargeId($charge->getId());
            $model->insertEntry($dto);
        }
    }
}
