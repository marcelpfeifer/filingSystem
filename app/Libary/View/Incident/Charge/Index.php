<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 17.05.2021
 * Time: 14:44
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Incident\Charge;


use App\Models\Charge;
use App\Models\ChargeGroup;
use App\Models\IncidentToProfile;

class Index
{

    /**
     * @param int $incidentId
     * @param int $profileId
     * @return \App\Libary\View\Dto\Incident\Charge\Index
     */
    public static function getViewDto(int $incidentId, int $profileId): \App\Libary\View\Dto\Incident\Charge\Index
    {
        return (new \App\Libary\View\Dto\Incident\Charge\Index())
            ->setIncidentId($incidentId)
            ->setProfileId($profileId)
            ->setCharges(self::getCharges())
            ->setSelectedCharges(self::getSelectedCharges($incidentId, $profileId));
    }

    /**
     * @return \App\Libary\View\Dto\Incident\Charge\Index\Charge[]
     */
    private static function getCharges(): array
    {
        /**
         * @var $charges \App\Libary\View\Dto\Incident\Charge\Index\Charge[]
         */
        $charges = [];
        foreach (Charge::all() as $charge) {
            /**
             * @var $dto \App\Models\Dto\Charge
             */
            $dto = Charge::getEntryAsDto($charge);
            if (isset($charges[$dto->getChargeGroupId()])) {
                $currentCharges = $charges[$dto->getChargeGroupId()]->getCharges();
                array_push($currentCharges, $dto);
                $charges[$dto->getChargeGroupId()]->setCharges($currentCharges);
            } else {
                /**
                 * @var $groupDto \App\Models\Dto\ChargeGroup
                 */
                $groupDto = ChargeGroup::getEntryAsDto($charge->chargeGroup);
                $charges[$dto->getChargeGroupId()] = (new \App\Libary\View\Dto\Incident\Charge\Index\Charge())
                    ->setCategory($groupDto->getName())
                    ->setCharges([$dto]);
            }
        }

        return $charges;
    }

    /**
     * @param int $incidentId
     * @param int $profileId
     * @return \App\Models\Dto\Charge[]
     */
    private static function getSelectedCharges(int $incidentId, int $profileId): array
    {
        $dto = (new \App\Models\Dto\IncidentToProfile())
            ->setProfileId($profileId)
            ->setIncidentId($incidentId);
        $entry = (new IncidentToProfile())->getEntryByIncidentIdAndProfileId($dto);
        $selectedCharges = [];
        if ($entry) {
            foreach ($entry->charges as $charge) {
                $selectedCharges[] = Charge::getEntryAsDto($charge->charge);
            }
        }
        return $selectedCharges;
    }
}
