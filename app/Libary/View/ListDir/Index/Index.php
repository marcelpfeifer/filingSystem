<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.07.2021
 * Time: 14:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\ListDir\Index;


use App\Models\ListModel;
use Illuminate\Database\Eloquent\Model;

class Index
{

    /**
     * @return \App\Libary\View\Dto\ListDir\Index\Index
     */
    public static function getViewDto(): \App\Libary\View\Dto\ListDir\Index\Index
    {
        $lists = (new ListModel())->search();
        return (new \App\Libary\View\Dto\ListDir\Index\Index())
            ->setLists($lists);
    }
}
