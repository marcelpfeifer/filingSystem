<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.07.2021
 * Time: 13:00
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\ListDir\Entry\Create;


use App\Models\ListHeader;
use Illuminate\Database\Eloquent\Model;

class Index
{
    /**
     * @param int $listId
     * @return \App\Libary\View\Dto\ListDir\Entry\Create\Index
     */
    public static function getViewDto(int $listId): \App\Libary\View\Dto\ListDir\Entry\Create\Index
    {
        $headers = (new ListHeader())->getEntriesByListId($listId);
        return (new \App\Libary\View\Dto\ListDir\Entry\Create\Index())
            ->setListId($listId)
            ->setHeaders($headers);
    }
}
