<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.07.2021
 * Time: 13:21
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\ListDir\Entry\Create;


use App\Libary\Logging\Logging;
use App\Models\Dto\ListEntry;
use App\Models\Dto\ListEntryValue;
use Carbon\Carbon;

class Save
{
    /**
     * @param \App\Http\Request\Dto\ListDir\Entry\Create\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\ListDir\Entry\Create\Save $requestDto)
    {
        $dto = (new ListEntry())
            ->setListId($requestDto->getListId())
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());

        $listEntryId = (new \App\Models\ListEntry())->insertEntry($dto);
        $loggingId = Logging::insert(\App\Models\ListEntry::class, $dto);

        foreach ($requestDto->getValues() as $headerId => $value) {
            $dto = (new ListEntryValue())
                ->setListEntryId($listEntryId)
                ->setListHeaderId($headerId)
                ->setValue($value)
                ->setCreatedAt(Carbon::now())
                ->setUpdatedAt(Carbon::now());
            (new \App\Models\ListEntryValue())->insertEntry($dto);
            Logging::insert(\App\Models\ListEntryValue::class, $dto, $loggingId);
        }
    }
}
