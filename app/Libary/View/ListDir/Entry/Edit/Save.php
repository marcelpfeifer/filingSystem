<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.07.2021
 * Time: 13:21
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\ListDir\Entry\Edit;


use App\Http\Response\AResponse;
use App\Libary\ListDir\Exception\NoPermission;
use App\Libary\ListDir\Permission;
use App\Libary\Logging\Logging;
use App\Models\Dto\ListEntryValue;
use Carbon\Carbon;

class Save
{
    /**
     * @param \App\Http\Request\Dto\ListDir\Entry\Edit\Save $requestDto
     * @throws NoPermission
     */
    public static function save(\App\Http\Request\Dto\ListDir\Entry\Edit\Save $requestDto)
    {
        $oldEntry = \App\Models\ListEntry::find($requestDto->getEntryId());
        if (!$oldEntry) {
            return;
        }
        $oldDto = \App\Models\ListEntry::getEntryAsDto($oldEntry);
        if (!(Permission::hasPermission($oldDto->getListId()))) {
            throw new NoPermission();
        }

        $model = new \App\Models\ListEntryValue();
        $values = [];
        foreach ($oldEntry->values as $value) {
            $valueDto = $model::getEntryAsDto($value);
            $values[$valueDto->getListHeaderId()] = $valueDto;
        }

        $dto = (new ListEntryValue())
            ->setListEntryId($requestDto->getEntryId());
        foreach ($requestDto->getValues() as $headerId => $value) {
            $dto->setListHeaderId($headerId)
                ->setValue($value ?? '');

            if (isset($values[$headerId])) {
                $oldDto = $values[$headerId];
                $dto->setId($oldDto->getId())
                    ->setUpdatedAt(Carbon::now());
                if ($value === null || $value === '') {
                    $model->deleteEntry($dto);
                    Logging::delete(\App\Models\ListEntryValue::class, $oldDto);
                } else {
                    if ($oldDto->getValue() === $value) {
                        continue;
                    }
                    $model->updateEntry($dto);
                    Logging::update(\App\Models\ListEntryValue::class, $oldDto, $dto);
                }
            } else {
                if ($value === null || $value === '') {
                    continue;
                }
                $model->insertEntry($dto);
                Logging::insert(\App\Models\ListEntryValue::class, $dto);
            }
        }
    }
}
