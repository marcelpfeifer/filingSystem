<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.07.2021
 * Time: 13:00
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\ListDir\Entry\Edit;


use App\Models\ListEntry;
use App\Models\ListEntryValue;
use App\Models\ListHeader;

class Index
{
    /**
     * @param int $listId
     * @return \App\Libary\View\Dto\ListDir\Entry\Edit\Index
     */
    public static function getViewDto(int $entryId): \App\Libary\View\Dto\ListDir\Entry\Edit\Index
    {
        $entry = ListEntry::find($entryId);
        $entryDto = ListEntry::getEntryAsDto($entry);
        $headers = (new ListHeader())->getEntriesByListId($entryDto->getListId());
        $values = [];
        foreach ($entry->values as $value) {
            $dto = ListEntryValue::getEntryAsDto($value);
            $values[$dto->getListHeaderId()] = $dto->getValue();
        }
        return (new \App\Libary\View\Dto\ListDir\Entry\Edit\Index())
            ->setEntryId($entryId)
            ->setListId($entryDto->getListId())
            ->setHeaders($headers)
            ->setValues($values);
    }
}
