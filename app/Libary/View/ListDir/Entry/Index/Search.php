<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.07.2021
 * Time: 15:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\ListDir\Entry\Index;

use App\Models\ListEntryValue;
use App\Models\ListHeader;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Search
{
    /**
     * @param \App\Http\Request\Dto\ListDir\Entry\Index\Search $requestDto
     * @return Search
     */
    public static function getViewDto(\App\Http\Request\Dto\ListDir\Entry\Index\Search $requestDto
    ): \App\Libary\View\Dto\ListDir\Entry\Index\Search {
        $headers = [];
        $entries = [];
        $listEntries = (new ListEntryValue())->search($requestDto->getListId(), $requestDto->getSearch());
        $listHeaders = (new ListHeader())->getEntriesByListId($requestDto->getListId(), true);

        foreach ($listHeaders as $listHeader) {
            $headers[$listHeader->getName()] = '';
        }

        foreach ($listEntries as $listEntry) {
            if (!isset($entries[$listEntry->entry->id])) {
                $entries[$listEntry->entry->id] = $headers;
            }
            $entries[$listEntry->entry->id][$listEntry->header->name] = $listEntry->value;
        }

        return (new \App\Libary\View\Dto\ListDir\Entry\Index\Search())
            ->setEntries($entries);
    }
}
