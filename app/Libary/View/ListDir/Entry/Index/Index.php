<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.07.2021
 * Time: 15:14
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\ListDir\Entry\Index;

use App\Models\ListHeader;
use App\Models\ListModel;

class Index
{

    /**
     * @param int $listId
     * @return \App\Libary\View\Dto\ListDir\Entry\Index\Index
     */
    public static function getViewDto(int $listId): \App\Libary\View\Dto\ListDir\Entry\Index\Index
    {
        $headers = [];
        $listHeaders = (new ListHeader())->getEntriesByListId($listId, true);
        foreach ($listHeaders as $listHeader) {
            $headers[] = $listHeader->getName();
        }
        return (new \App\Libary\View\Dto\ListDir\Entry\Index\Index())
            ->setListId($listId)
            ->setHeaders($headers);
    }
}

