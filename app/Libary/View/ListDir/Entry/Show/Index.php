<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.07.2021
 * Time: 13:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\ListDir\Entry\Show;


use App\Libary\View\Dto\ListDir\Entry\Show\Index\Value;
use App\Models\ListEntry;
use App\Models\ListEntryValue;
use App\Models\ListHeader;
use Illuminate\Database\Eloquent\Model;

class Index
{
    /**
     * @param int $entryId
     * @return \App\Libary\View\Dto\ListDir\Entry\Show\Index
     */
    public static function getViewDto(int $entryId): \App\Libary\View\Dto\ListDir\Entry\Show\Index
    {
        $values = [];
        $listEntry = ListEntry::find($entryId);
        foreach ($listEntry->values as $value) {
            $dto = ListEntryValue::getEntryAsDto($value);
            $values[] = (new Value())
                ->setId($dto->getListHeaderId())
                ->setName(ListHeader::getEntryAsDto($value->header)->getName())
                ->setValue($dto->getValue());
        }

        return (new \App\Libary\View\Dto\ListDir\Entry\Show\Index())
            ->setEntryId($entryId)
            ->setListId(ListEntry::getEntryAsDto($listEntry)->getListId())
            ->setValues($values);
    }
}
