<?php
/**
 * AddTag
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.07.2021
 * Time: 13:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Profile\Edit;

use App\Libary\Logging\Logging;
use App\Models\Dto\ProfileToTag;

class AddTag
{
    /**
     * @param \App\Http\Request\Dto\Profile\Edit\AddTag $requestDto
     */
    public static function save(\App\Http\Request\Dto\Profile\Edit\AddTag $requestDto)
    {
        $dto = (new ProfileToTag())
            ->setTagId($requestDto->getTagId())
            ->setProfileId($requestDto->getId());

        (new \App\Models\ProfileToTag())->insertEntry($dto);

        Logging::insert(\App\Models\ProfileToTag::class, $dto);
    }
}
