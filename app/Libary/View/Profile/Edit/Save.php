<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.04.2021
 * Time: 16:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Profile\Edit;

use App\Libary\Logging\Logging;
use App\Models\Dto\Profile;
use App\Models\Dto\ProfilePropertyValue;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Profile\Edit\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Profile\Edit\Save $requestDto)
    {
        $oldEntry = \App\Models\Profile::find($requestDto->getId());
        if (!$oldEntry) {
            return;
        }
        $oldDto = \App\Models\Profile::getEntryAsDto($oldEntry);

        $dto = (new Profile())
            ->setId($requestDto->getId())
            ->setName($requestDto->getName())
            ->setImageUrl($requestDto->getImageUrl())
            ->setDescription($requestDto->getDescription())
            ->setUpdatedAt(Carbon::now());

        (new \App\Models\Profile())->updateEntry($dto);

        Logging::update(\App\Models\Profile::class, $oldDto, $dto);

        // Update ProfilePropertyValue Table
        self::updateProperties($requestDto, $oldEntry);
    }

    /**
     * @param \App\Http\Request\Dto\Profile\Edit\Save $requestDto
     * @param \App\Models\Profile $profile
     */
    private static function updateProperties(
        \App\Http\Request\Dto\Profile\Edit\Save $requestDto,
        \App\Models\Profile $profile
    ) {
        $model = new \App\Models\ProfilePropertyValue();
        $values = [];
        foreach ($profile->propertyValues as $propertyValue) {
            $propertyValueDto = $model::getEntryAsDto($propertyValue);
            $values[$propertyValueDto->getProfilePropertyId()] = $propertyValueDto;
        }

        $dto = (new ProfilePropertyValue())
            ->setProfileId($requestDto->getId());
        foreach ($requestDto->getProperties() as $propertyId => $value) {
            $dto->setProfilePropertyId($propertyId)
                ->setValue($value ?? '');

            if (isset($values[$propertyId])) {
                $oldDto = $values[$propertyId];
                $dto->setId($oldDto->getId());
                if ($value === null || $value === '') {
                    $model->deleteEntry($dto);
                    Logging::delete(\App\Models\ProfilePropertyValue::class, $oldDto);
                } else {
                    if ($oldDto->getValue() === $value) {
                        continue;
                    }
                    $model->updateEntry($dto);
                    Logging::update(\App\Models\ProfilePropertyValue::class, $oldDto, $dto);
                }
            } else {
                if ($value === null || $value === '') {
                    continue;
                }
                $model->insertEntry($dto);
                Logging::insert(\App\Models\ProfilePropertyValue::class, $dto);
            }
        }
    }
}
