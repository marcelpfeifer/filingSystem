<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.05.2021
 * Time: 10:58
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Profile\Edit;


use App\Libary\View\Dto\Profile\Edit\Index\PropertyGroup;
use App\Libary\View\Dto\Profile\Edit\Index\PropertyGroup\Property;
use App\Models\Charge;
use App\Models\IncidentToProfileCharge;
use App\Models\Profile;
use App\Models\ProfileProperty;
use App\Models\ProfilePropertyGroup;
use App\Models\ProfilePropertyValue;
use App\Models\Tag;

class Index
{

    /**
     * @param int $id
     * @return \App\Libary\View\Dto\Profile\Edit\Index|null
     */
    public static function getViewDto(int $id): ?\App\Libary\View\Dto\Profile\Edit\Index
    {
        $entry = Profile::find($id);
        if (!($entry)) {
            return null;
        }

        $tags = (new Tag())->search(null, null, true);

        $dto = Profile::getEntryAsDto($entry);

        $profileTags = [];
        $propertyGroups = [];
        $propertiesArray = [];

        foreach ($entry->tags as $profileToTag) {
            $profileTags[] = Tag::getEntryAsDto($profileToTag->tag);
        }

        foreach (ProfilePropertyGroup::getEntriesAsDto(ProfilePropertyGroup::all()) as $propertyGroup) {
            $propertyGroups[$propertyGroup->getId()] = (new PropertyGroup())
                ->setId($propertyGroup->getId())
                ->setName($propertyGroup->getName())
                ->setProperties([]);
        }
        $properties = ProfileProperty::getEntriesAsDto(ProfileProperty::all());
        foreach ($properties as $property) {
            if (isset($propertyGroups[$property->getProfilePropertyGroupId()])) {
                $propertyDto = (new Property())
                    ->setId($property->getId())
                    ->setName($property->getName());
                $entries = $propertyGroups[$property->getProfilePropertyGroupId()]->getProperties();
                array_push($entries, $propertyDto);
                $propertyGroups[$property->getProfilePropertyGroupId()]->setProperties(
                    $entries
                );
                $propertiesArray[$propertyDto->getId()] = '';
            }
        }

        foreach ($entry->propertyValues as $propertyValue) {
            $propertyValueDto = ProfilePropertyValue::getEntryAsDto($propertyValue);
            $propertiesArray[$propertyValueDto->getProfilePropertyId()] = $propertyValueDto->getValue();
        }

        return (new \App\Libary\View\Dto\Profile\Edit\Index())
            ->setProfile($dto)
            ->setTags($tags)
            ->setProfileTags($profileTags)
            ->setPropertyGroups($propertyGroups)
            ->setProperties($propertiesArray);
    }
}
