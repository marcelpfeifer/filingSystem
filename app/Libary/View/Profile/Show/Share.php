<?php
/**
 * Share
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 22.05.2021
 * Time: 21:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Profile\Show;

use App\Libary\Common\RandomString;
use App\Libary\Logging\Logging;
use App\Models\ShareProfile;
use Carbon\Carbon;

class Share
{

    /**
     * @param int $profileId
     * @return \App\Libary\View\Dto\Profile\Show\Share
     */
    public static function share(int $profileId): \App\Libary\View\Dto\Profile\Show\Share
    {
        $model = new ShareProfile();
        while (true) {
            $code = RandomString::generate(8);
            if (!$model->getEntryByCode($code)) {
                break;
            }
        }
        $dto = (new \App\Models\Dto\ShareProfile())
            ->setCode($code)
            ->setProfileId($profileId)
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        $model->insertEntry($dto);

        Logging::insert(ShareProfile::class, $dto);

        return (new \App\Libary\View\Dto\Profile\Show\Share())
            ->setCode($code);
    }
}
