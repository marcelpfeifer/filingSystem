<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 12:48
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Profile\Show;


use App\Libary\View\Dto\Profile\Show\Index\PropertyGroup;
use App\Libary\View\Dto\Profile\Show\Index\PropertyGroup\Property;
use App\Models\Charge;
use App\Models\Incident;
use App\Models\IncidentToProfileCharge;
use App\Models\Profile;
use App\Models\ProfileProperty;
use App\Models\ProfilePropertyGroup;
use App\Models\ProfilePropertyValue;
use App\Models\Tag;

class Index
{
    /**
     * @param int $id
     * @return \App\Libary\View\Dto\Profile\Show\Index|null
     */
    public static function getViewDto(int $id): ?\App\Libary\View\Dto\Profile\Show\Index
    {
        $entry = Profile::find($id);
        if (!($entry)) {
            return null;
        }

        $dto = Profile::getEntryAsDto($entry);
        $latestIncidents = (new Incident())->getLatestEntriesByProfileId($id);
        return (new \App\Libary\View\Dto\Profile\Show\Index())
            ->setCharges(self::getCharges($entry))
            ->setTags(self::getTags($entry))
            ->setProfile($dto)
            ->setPropertyGroups(self::getPropertyGroups($entry))
            ->setLatestIncidents($latestIncidents);
    }

    /**
     * @return \App\Libary\View\Dto\Profile\Show\Index\Charge[]
     */
    private static function getCharges(Profile $entry): array
    {
        /**
         * @var $charges \App\Libary\View\Dto\Profile\Show\Index\Charge[]
         */
        $charges = [];
        foreach ($entry->incidentToProfileCharges as $incidentToProfileCharge) {
            if (!(isset($charges[$incidentToProfileCharge->{IncidentToProfileCharge::COLUMN_CHARGE_ID}]))) {
                $chargeDto = Charge::getEntryAsDto($incidentToProfileCharge->charge);
                $charges[$chargeDto->getId()] = (new \App\Libary\View\Dto\Profile\Show\Index\Charge())
                    ->setId($chargeDto->getId())
                    ->setName($chargeDto->getName())
                    ->setCount(1);
            } else {
                $charges[$incidentToProfileCharge->{IncidentToProfileCharge::COLUMN_CHARGE_ID}]
                    ->setCount(
                        $charges[$incidentToProfileCharge->{IncidentToProfileCharge::COLUMN_CHARGE_ID}]->getCount() + 1
                    );
            }
        }
        return $charges;
    }

    /**
     * @param Profile $entry
     * @return \App\Models\Dto\Tag[]
     */
    private static function getTags(Profile $entry): array
    {
        $tags = [];
        foreach ($entry->tags as $profileToTag) {
            $tags[] = Tag::getEntryAsDto($profileToTag->tag);
        }
        return $tags;
    }

    /**
     * @param Profile $entry
     * @return PropertyGroup[]
     */
    private static function getPropertyGroups(Profile $entry): array
    {
        $propertyValues = self::getPropertyValues($entry);
        $propertyGroups = [];

        foreach (ProfilePropertyGroup::getEntriesAsDto(ProfilePropertyGroup::all()) as $propertyGroup) {
            $propertyGroups[$propertyGroup->getId()] = (new PropertyGroup())
                ->setId($propertyGroup->getId())
                ->setName($propertyGroup->getName())
                ->setProperties([]);
        }

        $properties = ProfileProperty::getEntriesAsDto(ProfileProperty::all());
        foreach ($properties as $property) {
            if (isset($propertyGroups[$property->getProfilePropertyGroupId()])) {
                $propertyDto = (new Property())
                    ->setId($property->getId())
                    ->setName($property->getName())
                    ->setValue(isset($propertyValues[$property->getId()]) ? $propertyValues[$property->getId()] : '');

                $entries = $propertyGroups[$property->getProfilePropertyGroupId()]->getProperties();
                array_push($entries, $propertyDto);
                $propertyGroups[$property->getProfilePropertyGroupId()]->setProperties(
                    $entries
                );
            }
        }
        return $propertyGroups;
    }

    /**
     * @param Profile $entry
     * @return \App\Models\Dto\ProfilePropertyValue[]
     */
    private static function getPropertyValues(Profile $entry): array
    {
        $propertyValues = [];
        foreach ($entry->propertyValues as $propertyValue) {
            $propertyValueDto = ProfilePropertyValue::getEntryAsDto($propertyValue);
            $propertyValues[$propertyValueDto->getProfilePropertyId()] = $propertyValueDto->getValue();
        }
        return $propertyValues;
    }
}
