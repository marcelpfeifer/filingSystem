<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.04.2021
 * Time: 16:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Profile\Create;

use App\Libary\Logging\Logging;
use App\Models\Dto\Profile;
use Carbon\Carbon;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Profile\Create\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Profile\Create\Save $requestDto)
    {
        $dto = (new Profile())
            ->setName($requestDto->getName())
            ->setImageUrl($requestDto->getImageUrl())
            ->setDescription($requestDto->getDescription())
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());

        (new \App\Models\Profile())->insertEntry($dto);

        Logging::insert(\App\Models\Profile::class, $dto);
    }
}
