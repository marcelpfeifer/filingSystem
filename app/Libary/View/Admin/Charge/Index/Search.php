<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 10.05.2021
 * Time: 09:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\Charge\Index;


use App\Models\Charge;
use App\Models\ChargeGroup;

class Search
{
    /**
     * @param \App\Http\Request\Dto\Admin\Charge\Index\Search $requestDto
     * @return \App\Libary\View\Dto\Admin\Charge\Index\Search|null
     */
    public static function getViewDto(\App\Http\Request\Dto\Admin\Charge\Index\Search $requestDto
    ): ?\App\Libary\View\Dto\Admin\Charge\Index\Search {
        $groups = ChargeGroup::getEntriesAsDto(ChargeGroup::all());
        $charges = (new Charge())->search($requestDto->getName());
        return (new \App\Libary\View\Dto\Admin\Charge\Index\Search())
            ->setChargeGroups($groups)
            ->setCharges($charges);
    }
}
