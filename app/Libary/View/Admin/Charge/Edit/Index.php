<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 00:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\Charge\Edit;


use App\Models\Charge;
use App\Models\ChargeGroup;
use Illuminate\Database\Eloquent\Model;

class Index
{

    /**
     * @param \App\Http\Request\Dto\Admin\Charge\Index\Search $requestDto
     * @return \App\Libary\View\Dto\Admin\Charge\Edit\Index
     */
    public static function getViewDto(int $id): ?\App\Libary\View\Dto\Admin\Charge\Edit\Index
    {
        $groups = ChargeGroup::getEntriesAsDto(ChargeGroup::all());
        $charge = Charge::getEntryAsDto(Charge::find($id));
        return (new \App\Libary\View\Dto\Admin\Charge\Edit\Index())
            ->setChargeGroups($groups)
            ->setCharge($charge);
    }
}
