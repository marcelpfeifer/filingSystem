<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 00:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\Charge\Edit;

use App\Libary\Logging\Logging;
use App\Models\Dto\Charge;
use Carbon\Carbon;

class Save
{
    /**
     * @param \App\Http\Request\Dto\Admin\Charge\Edit\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Admin\Charge\Edit\Save $requestDto)
    {
        $oldEntry = \App\Models\Charge::find($requestDto->getId());
        if (!$oldEntry) {
            return;
        }
        $oldDto = \App\Models\Charge::getEntryAsDto($oldEntry);

        $dto = (new Charge())
            ->setId($requestDto->getId())
            ->setName($requestDto->getName())
            ->setDescription($requestDto->getDescription())
            ->setChargeGroupId($requestDto->getChargeGroupId())
            ->setTime($requestDto->getTime())
            ->setMoney($requestDto->getMoney())
            ->setUpdatedAt(Carbon::now());
        (new \App\Models\Charge())->updateEntry($dto);

        Logging::update(\App\Models\Charge::class, $oldDto, $dto);
    }
}
