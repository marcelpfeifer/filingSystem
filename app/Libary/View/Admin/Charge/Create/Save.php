<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 00:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\Charge\Create;


use App\Libary\Logging\Logging;
use App\Models\Dto\Charge;
use Carbon\Carbon;

class Save
{
    /**
     * @param \App\Http\Request\Dto\Admin\Charge\Create\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Admin\Charge\Create\Save $requestDto)
    {
        $dto = (new Charge())
            ->setName($requestDto->getName())
            ->setDescription($requestDto->getDescription())
            ->setChargeGroupId($requestDto->getChargeGroupId())
            ->setTime($requestDto->getTime())
            ->setMoney($requestDto->getMoney())
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        (new \App\Models\Charge())->insertEntry($dto);

        Logging::insert(\App\Models\Charge::class, $dto);
    }
}
