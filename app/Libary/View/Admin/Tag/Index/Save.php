<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.07.2021
 * Time: 11:26
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\Tag\Index;

use App\Libary\Logging\Logging;
use App\Models\Tag;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Admin\Tag\Index\Save $saveDto
     */
    public static function save(\App\Http\Request\Dto\Admin\Tag\Index\Save $saveDto)
    {
        $dto = (new \App\Models\Dto\Tag())
            ->setName($saveDto->getName())
            ->setIncident($saveDto->isIncident())
            ->setProfile($saveDto->isProfile());
        (new Tag())->insertEntry($dto);

        Logging::insert(Tag::class, $dto);
    }
}
