<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 06.05.2021
 * Time: 14:39
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\User\Edit;


use App\Libary\Logging\Logging;
use App\Models\Dto\User;
use Illuminate\Support\Facades\Hash;

class Save
{
    /**
     * @param \App\Http\Request\Dto\Admin\User\Edit\Save $saveDto
     */
    public static function save(\App\Http\Request\Dto\Admin\User\Edit\Save $saveDto)
    {
        $user = \App\Models\User::find($saveDto->getId());
        if (!($user)) {
            return;
        }
        /**
         * @var $oldDto User
         * @var $dto User
         */
        $oldDto = \App\Models\User::getEntryAsDto($user);
        $dto = \App\Models\User::getEntryAsDto($user);
        if ($saveDto->getPassword()) {
            $dto->setPassword(Hash::make($saveDto->getPassword()));
        }

        $dto->setName($saveDto->getName())
            ->setPermissionGroupId($saveDto->getPermissionGroupId())
            ->setGroupId($saveDto->getGroupId());
        (new \App\Models\User())->updateEntry($dto);

        Logging::update(\App\Models\User::class, $oldDto, $dto);
    }
}
