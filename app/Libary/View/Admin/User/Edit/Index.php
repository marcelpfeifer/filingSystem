<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 06.05.2021
 * Time: 13:59
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\User\Edit;


use App\Models\Group;
use App\Models\PermissionGroup;
use App\Models\User;

class Index
{
    /**
     * @param int $id
     * @return \App\Libary\View\Dto\Admin\User\Edit\Index|null
     */
    public static function getViewDto(int $id): ?\App\Libary\View\Dto\Admin\User\Edit\Index
    {
        $permissionGroups = PermissionGroup::getEntriesAsDto(PermissionGroup::all());
        $groups = Group::getEntriesAsDto(Group::all());
        $user = User::find($id);
        $userDto = User::getEntryAsDto($user);

        return (new \App\Libary\View\Dto\Admin\User\Edit\Index())
            ->setUser($userDto)
            ->setPermissionGroups($permissionGroups)
            ->setGroups($groups);
    }
}
