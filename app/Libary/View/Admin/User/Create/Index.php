<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 14:30
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\User\Create;


use App\Models\Group;
use App\Models\PermissionGroup;

class Index
{
    /**
     * @return \App\Libary\View\Dto\Admin\User\Create\Index|null
     */
    public static function getViewDto(): ?\App\Libary\View\Dto\Admin\User\Create\Index
    {
        $permissionGroups = PermissionGroup::getEntriesAsDto(PermissionGroup::all());
        $groups = Group::getEntriesAsDto(Group::all());

        return (new \App\Libary\View\Dto\Admin\User\Create\Index())
            ->setPermissionGroups($permissionGroups)
            ->setGroups($groups);
    }
}
