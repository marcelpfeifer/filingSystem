<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.04.2021
 * Time: 15:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\User\Create;

use App\Libary\Logging\Logging;
use App\Models\Dto\User;
use Illuminate\Support\Facades\Hash;

class Save
{
    /**
     * @param \App\Http\Request\Dto\Admin\User\Create\Save $saveDto
     */
    public static function save(\App\Http\Request\Dto\Admin\User\Create\Save $saveDto)
    {
        $dto = (new User())
            ->setName($saveDto->getName())
            ->setPassword(Hash::make($saveDto->getPassword()))
            ->setPermissionGroupId($saveDto->getPermissionGroupId())
            ->setGroupId($saveDto->getGroupId());
        (new \App\Models\User())->insertEntry($dto);
        Logging::insert(\App\Models\User::class, $dto);
    }
}
