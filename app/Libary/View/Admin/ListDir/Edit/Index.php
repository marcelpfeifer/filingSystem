<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 13:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\ListDir\Edit;

use App\Models\ListHeader;

class Index
{

    /**
     * @return \App\Libary\View\Dto\Admin\ListDir\Edit\Index
     */
    public static function getViewDto(int $id): \App\Libary\View\Dto\Admin\ListDir\Edit\Index
    {
        $headers = (new ListHeader())->getEntriesByListId($id);

        return (new \App\Libary\View\Dto\Admin\ListDir\Edit\Index())
            ->setListId($id)
            ->setHeaders($headers);
    }
}
