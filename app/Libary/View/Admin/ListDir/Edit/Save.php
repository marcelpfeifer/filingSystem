<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 13:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\ListDir\Edit;

use App\Libary\Logging\Logging;
use App\Models\Dto\ListHeader;
use Carbon\Carbon;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Admin\ListDir\Edit\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Admin\ListDir\Edit\Save $requestDto)
    {
        $entry = \App\Models\ListHeader::find($requestDto->getHeaderId());
        if($entry) {
            $oldDto =  \App\Models\ListHeader::getEntryAsDto($entry);
            $dto = (clone $oldDto)
                ->setName($requestDto->getName())
                ->setList($requestDto->isList())
                ->setOrder($requestDto->getOrder())
                ->setUpdatedAt(Carbon::now());
            (new \App\Models\ListHeader())->updateEntry($dto);

            Logging::update(\App\Models\ListHeader::class, $oldDto, $dto);
        }
    }
}
