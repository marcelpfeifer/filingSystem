<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 13:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\ListDir\Edit;

use App\Libary\Logging\Logging;
use App\Models\Dto\ListHeader;
use Carbon\Carbon;

class Create
{

    /**
     * @param \App\Http\Request\Dto\Admin\ListDir\Edit\Create $requestDto
     * @return ListHeader
     */
    public static function save(\App\Http\Request\Dto\Admin\ListDir\Edit\Create $requestDto): ListHeader
    {
        $dto = (new ListHeader())
            ->setListId($requestDto->getListId())
            ->setName($requestDto->getName())
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        $id = (new \App\Models\ListHeader())->insertEntry($dto);
        $dto->setId($id);
        Logging::insert(\App\Models\ListHeader::class, $dto);

        return $dto;
    }
}
