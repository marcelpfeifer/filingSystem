<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 13:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\ListDir\Index;

use App\Libary\Logging\Logging;
use App\Models\Dto\ListModel;
use App\Models\Dto\ListToGroup;
use Carbon\Carbon;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Admin\ListDir\Index\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Admin\ListDir\Index\Save $requestDto)
    {
        $dto = (new ListModel())
            ->setName($requestDto->getName())
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        $listId = (new \App\Models\ListModel())->insertEntry($dto);

        foreach ($requestDto->getGroupIds() as $groupId) {
            $listToGroupDto = (new ListToGroup())
                ->setListId($listId)
                ->setGroupId($groupId);
            (new \App\Models\ListToGroup())->insertEntry($listToGroupDto);
        }

        Logging::insert(\App\Models\ListModel::class, $dto);
    }
}
