<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 10.05.2021
 * Time: 09:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\ProfileProperty\Index;

use App\Models\ProfileProperty;

class Search
{
    /**
     * @param \App\Http\Request\Dto\Admin\ProfileProperty\Index\Search $requestDto
     * @return \App\Libary\View\Dto\Admin\ProfileProperty\Index\Search|null
     */
    public static function getViewDto(\App\Http\Request\Dto\Admin\ProfileProperty\Index\Search $requestDto
    ): ?\App\Libary\View\Dto\Admin\ProfileProperty\Index\Search {
        $result = (new ProfileProperty())->search($requestDto->getName());
        return (new \App\Libary\View\Dto\Admin\ProfileProperty\Index\Search())
            ->setProfileProperties($result);
    }
}
