<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 00:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\ProfileProperty\Edit;


use App\Models\ProfileProperty;
use App\Models\ProfilePropertyGroup;
use Illuminate\Database\Eloquent\Model;

class Index
{

    /**
     * @param int $id
     * @return \App\Libary\View\Dto\Admin\ProfileProperty\Edit\Index
     */
    public static function getViewDto(int $id): ?\App\Libary\View\Dto\Admin\ProfileProperty\Edit\Index
    {
        $groups = ProfilePropertyGroup::getEntriesAsDto(ProfilePropertyGroup::all());
        $entry = ProfileProperty::getEntryAsDto(ProfileProperty::find($id));
        return (new \App\Libary\View\Dto\Admin\ProfileProperty\Edit\Index())
            ->setGroups($groups)
            ->setProfileProperty($entry);
    }
}
