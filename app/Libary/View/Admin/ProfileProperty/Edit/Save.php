<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 00:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\ProfileProperty\Edit;

use App\Libary\Logging\Logging;
use App\Models\Dto\ProfileProperty;
use Carbon\Carbon;

class Save
{
    /**
     * @param \App\Http\Request\Dto\Admin\ProfileProperty\Edit\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Admin\ProfileProperty\Edit\Save $requestDto)
    {
        $oldEntry = \App\Models\ProfileProperty::find($requestDto->getId());
        if (!$oldEntry) {
            return;
        }
        $oldDto = \App\Models\ProfileProperty::getEntryAsDto($oldEntry);

        $dto = (new ProfileProperty())
            ->setId($requestDto->getId())
            ->setName($requestDto->getName())
            ->setProfilePropertyGroupId($requestDto->getProfilePropertyGroupId())
            ->setUpdatedAt(Carbon::now());
        (new \App\Models\ProfileProperty())->updateEntry($dto);

        Logging::update(\App\Models\ProfileProperty::class, $oldDto, $dto);
    }
}
