<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 00:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\ProfileProperty\Create;


use App\Libary\Logging\Logging;
use App\Models\Dto\ProfileProperty;
use Carbon\Carbon;

class Save
{
    /**
     * @param \App\Http\Request\Dto\Admin\ProfileProperty\Create\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Admin\ProfileProperty\Create\Save $requestDto)
    {
        $dto = (new ProfileProperty())
            ->setName($requestDto->getName())
            ->setProfilePropertyGroupId($requestDto->getProfilePropertyGroupId())
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        (new \App\Models\ProfileProperty())->insertEntry($dto);

        Logging::insert(\App\Models\ProfileProperty::class, $dto);
    }
}
