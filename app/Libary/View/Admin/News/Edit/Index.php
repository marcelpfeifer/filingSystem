<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.06.2021
 * Time: 13:18
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\News\Edit;


use App\Models\News;

class Index
{
    /**
     * @return \App\Libary\View\Dto\Admin\News\Edit\Index
     */
    public static function getViewDto(int $id): \App\Libary\View\Dto\Admin\News\Edit\Index
    {
        return (new \App\Libary\View\Dto\Admin\News\Edit\Index())
            ->setNews(News::getEntryAsDto(News::find($id)));
    }
}
