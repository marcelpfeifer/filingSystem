<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.06.2021
 * Time: 13:18
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\News\Edit;


use App\Libary\Logging\Logging;
use App\Models\Dto\News;
use Carbon\Carbon;

class Save
{
    /**
     * @param \App\Http\Request\Dto\Admin\News\Edit\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Admin\News\Edit\Save $requestDto)
    {
        $oldDto = \App\Models\News::getEntryAsDto(\App\Models\News::find($requestDto->getId()));
        $dto = (new News())
            ->setId($requestDto->getId())
            ->setTitle($requestDto->getTitle())
            ->setContent(htmlspecialchars($requestDto->getContent(),ENT_QUOTES))
            ->setUpdatedAt(Carbon::now());
        (new \App\Models\News())->updateEntry($dto);
        Logging::update(\App\Models\News::class, $oldDto, $dto);
    }
}
