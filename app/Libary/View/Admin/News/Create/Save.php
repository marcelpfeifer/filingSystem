<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.06.2021
 * Time: 12:54
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\News\Create;


use App\Libary\Logging\Logging;
use App\Models\Dto\News;
use Carbon\Carbon;

class Save
{
    /**
     * @param \App\Http\Request\Dto\Admin\News\Create\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Admin\News\Create\Save $requestDto)
    {
        $dto = (new News())
            ->setTitle($requestDto->getTitle())
            ->setContent(htmlspecialchars($requestDto->getContent(),ENT_QUOTES))
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        (new \App\Models\News())->insertEntry($dto);
        Logging::insert(\App\Models\News::class, $dto);
    }
}
