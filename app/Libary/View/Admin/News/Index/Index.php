<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 05.06.2021
 * Time: 19:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\News\Index;


use App\Models\News;

class Index
{
    /**
     * @return \App\Libary\View\Dto\Admin\News\Index\Index
     */
    public static function getViewDto(): \App\Libary\View\Dto\Admin\News\Index\Index
    {
        return (new \App\Libary\View\Dto\Admin\News\Index\Index())
            ->setNews(News::getEntriesAsDto(News::all()));
    }
}
