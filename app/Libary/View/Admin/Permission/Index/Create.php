<?php
/**
 * Create
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 25.04.2021
 * Time: 12:57
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\Permission\Index;

use App\Models\Dto\PermissionGroup;
use Carbon\Carbon;

class Create
{

    /**
     * @param \App\Http\Request\Dto\Admin\Permission\Index\Create $createDto
     * @return PermissionGroup|null
     */
    public static function create(\App\Http\Request\Dto\Admin\Permission\Index\Create $createDto): ?PermissionGroup
    {
        $dto = (new PermissionGroup())
            ->setName($createDto->getName())
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        $id = (new \App\Models\PermissionGroup())->insertEntry($dto);
        if ($id === null) {
            return null;
        }
        $dto->setId($id);
        return $dto;
    }
}
