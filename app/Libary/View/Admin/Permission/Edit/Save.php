<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.04.2021
 * Time: 22:31
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\Permission\Edit;

use App\Libary\Logging\Logging;
use App\Models\PermissionToPermissionGroup;
use Illuminate\Support\Facades\Log;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Admin\Permission\Edit\Save $saveDto
     */
    public static function save(\App\Http\Request\Dto\Admin\Permission\Edit\Save $saveDto)
    {
        $dto = (new \App\Models\Dto\PermissionToPermissionGroup())
            ->setPermissionGroupId($saveDto->getGroupId());
        (new PermissionToPermissionGroup())->deleteEntry($dto);

        foreach ($saveDto->getPermissions() as $permissionId) {
            $dto->setPermissionId($permissionId);
            (new PermissionToPermissionGroup())->insertEntry($dto);
        }
    }
}
