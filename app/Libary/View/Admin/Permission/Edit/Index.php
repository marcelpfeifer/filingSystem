<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 25.04.2021
 * Time: 13:15
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\Permission\Edit;


use App\Libary\View\Dto\Admin\Permission\Edit\Index\Group;
use App\Models\Permission;
use App\Models\PermissionGroup;
use App\Models\PermissionToPermissionGroup;

class Index
{

    /**
     * @var \App\Models\Dto\Permission[]
     */
    private static $allPermissions = [];

    /**
     * @var \App\Libary\View\Dto\Admin\Permission\Edit\Index\Permission[]
     */
    private static $permissions = [];

    /**
     * @param int $id
     * @return \App\Libary\View\Dto\Admin\Permission\Edit\Index|null
     */
    public static function getViewDto(int $id): ?\App\Libary\View\Dto\Admin\Permission\Edit\Index
    {
        $group = PermissionGroup::find($id);
        $groupDto = PermissionGroup::getEntryAsDto($group);

        $selectedEntries = (new PermissionToPermissionGroup())->getPermissionsIdsByPermissionGroupId($id);

        self::$allPermissions = Permission::getEntriesAsDto(Permission::all());
        self::sortPermissions(null);

        return (new \App\Libary\View\Dto\Admin\Permission\Edit\Index())
            ->setGroup(
                (new Group())
                    ->setId($groupDto->getId())
                    ->setName($groupDto->getName())
            )
            ->setPermissions(self::$permissions)
            ->setSelectedPermissions($selectedEntries);
    }

    /**
     * @param \App\Models\Dto\Permission|null $permission
     */
    private static function sortPermissions(?\App\Models\Dto\Permission $permission)
    {
        if ($permission === null) {
            $key = array_key_first(self::$allPermissions);
            if ($key == null) {
                return;
            }
            $permission = self::$allPermissions[$key];
            unset(self::$allPermissions[$key]);
        }
        if ($permission->getParentPermission() === null) {
            self::$permissions[$permission->getId()] = self::getViewPermissionDto($permission);
            self::sortPermissions(null);
            return;
        }

        // Adds Parent Permission to Permission Array
        if (isset(self::$allPermissions[$permission->getParentPermission()])) {
            self::sortPermissions(self::$allPermissions[$permission->getParentPermission()]);
            unset(self::$allPermissions[$permission->getParentPermission()]);
        }

        self::$permissions = self::addPermissionAsChild($permission, self::$permissions);

        self::sortPermissions(null);
    }

    /**
     * @param \App\Models\Dto\Permission $permission
     * @param \App\Libary\View\Dto\Admin\Permission\Edit\Index\Permission[] $permissions
     * @return \App\Libary\View\Dto\Admin\Permission\Edit\Index\Permission[]
     */
    private static function addPermissionAsChild(\App\Models\Dto\Permission $permission, array $permissions): array
    {
        foreach ($permissions as $key => $entry) {
            if ($entry->getId() === $permission->getParentPermission()) {
                $childrenPermissions = $entry->getChildren();
                array_push($childrenPermissions, self::getViewPermissionDto($permission));
                $entry->setChildren($childrenPermissions);
                break;
            }
            $entry->setChildren(self::addPermissionAsChild($permission, $entry->getChildren()));
        }
        return $permissions;
    }

    /**
     * @param \App\Models\Dto\Permission $permission
     * @return \App\Libary\View\Dto\Admin\Permission\Edit\Index\Permission
     */
    private static function getViewPermissionDto(\App\Models\Dto\Permission $permission
    ): \App\Libary\View\Dto\Admin\Permission\Edit\Index\Permission {
        return (new \App\Libary\View\Dto\Admin\Permission\Edit\Index\Permission())
            ->setId($permission->getId())
            ->setName($permission->getName())
            ->setDescription($permission->getDescription())
            ->setChildren([]);
    }
}

