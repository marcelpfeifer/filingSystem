<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 13:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\Group\Index;

use App\Libary\Logging\Logging;
use App\Models\Dto\Group;
use Carbon\Carbon;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Admin\Group\Index\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Admin\Group\Index\Save $requestDto)
    {
        $dto = (new Group())
            ->setName($requestDto->getName())
            ->setShowAll($requestDto->isShowAll())
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        (new \App\Models\Group())->insertEntry($dto);

        Logging::insert(\App\Models\Group::class, $dto);
    }
}
