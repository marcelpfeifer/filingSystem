<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 10.05.2021
 * Time: 14:29
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Libary\View\Admin\ProfilePropertyGroup\Index;


use App\Libary\Logging\Logging;
use App\Models\Dto\ProfilePropertyGroup;
use Carbon\Carbon;

class Save
{

    /**
     * @param \App\Http\Request\Dto\Admin\ProfilePropertyGroup\Index\Save $requestDto
     */
    public static function save(\App\Http\Request\Dto\Admin\ProfilePropertyGroup\Index\Save $requestDto)
    {
        $dto = (new ProfilePropertyGroup())
            ->setName($requestDto->getName())
            ->setCreatedAt(Carbon::now())
            ->setUpdatedAt(Carbon::now());
        (new \App\Models\ProfilePropertyGroup())->insertEntry($dto);

        Logging::insert(\App\Models\ProfilePropertyGroup::class, $dto);
    }
}
