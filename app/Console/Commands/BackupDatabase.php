<?php
/**
 * BackupDatabase
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 07.04.2020
 * Time: 20:59
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class BackupDatabase extends Command
{
    /**
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * @var string
     */
    protected $description = 'Backup the database';

    /**
     * @var Process
     */
    protected $process;

    /**
     * @var string
     */
    protected $storagePath = '';

    /**
     * BackupDatabase constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->storagePath = storage_path('backups/database/');
        if(!file_exists($this->storagePath)) {
            mkdir($this->storagePath, 0755, true);
        }
        $filename = "database." . Carbon::now()->timestamp . ".sql";
        $this->process = new Process([sprintf(
            "mysqldump -h %s -u %s -p'%s' %s > %s",
            config('database.connections.mysql.host'),
            config('database.connections.mysql.username'),
            config('database.connections.mysql.password'),
            config('database.connections.mysql.database'),
            $this->storagePath . $filename
        )]);
    }

    public function handle()
    {
        try {
            $this->process->mustRun();
            $this->deleteOldFiles();
            $this->info('The backup has been proceed successfully.');
        } catch (ProcessFailedException $exception) {
            $this->error($exception->getMessage());
        }
    }

    private function deleteOldFiles(): void
    {
        $files = glob($this->storagePath . "*");
        $now = time();

        foreach ($files as $file) {
            if (is_file($file)) {
                if ($now - filemtime($file) >= 60 * 60 * 24 * 7) { // 7 days
                    unlink($file);
                }
            }
        }
    }
}
