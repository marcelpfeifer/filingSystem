<?php
/**
 * ListModel
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 23.07.2021
 * Time: 15:32
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;

class ListModel extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    protected $table = 'list';

    /**
     * @var array
     */
    protected $dates = [
        ListModel::CREATED_AT,
        ListModel::UPDATED_AT,
    ];

    /**
     * @return HasMany
     */
    public function headers(): HasMany
    {
        return $this->hasMany(ListHeader::class, ListHeader::COLUMN_LIST_ID, ListModel::COLUMN_ID);
    }

    /**
     * @return HasMany
     */
    public function entries(): HasMany
    {
        return $this->hasMany(ListEntry::class, ListEntry::COLUMN_LIST_ID, ListEntry::COLUMN_ID);
    }

    /**
     * @return HasMany
     */
    public function listToGroup(): HasMany
    {
        return $this->hasMany(ListToGroup::class, ListToGroup::COLUMN_LIST_ID, ListModel::COLUMN_ID);
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\ListModel
         */
        return $this->insertGetId(
            [
                ListModel::COLUMN_NAME => $dto->getName(),
                ListModel::CREATED_AT  => $dto->getCreatedAt() ?? Carbon::now(),
                ListModel::UPDATED_AT  => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\ListModel
         */
        return $this->where(ListModel::COLUMN_ID, $dto->getId())->update(
            [
                ListModel::COLUMN_NAME => $dto->getName(),
                ListModel::UPDATED_AT  => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\ListModel
         */
        $this->where(ListModel::COLUMN_ID, $dto->getId())->delete();
    }


    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = ListModel::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @return ADto[]
     */
    public function search(): array
    {
        $query = $this;

        $groupId = Auth::user()->{User::COLUMN_GROUP_ID};
        if ($groupId && !Auth::user()->group->{Group::COLUMN_SHOW_ALL}) {
            $query = $query->with('listToGroup')
                ->whereHas(
                    'listToGroup',
                    function ($query) use ($groupId) {
                        return $query->where(User::COLUMN_GROUP_ID, '=', $groupId);
                    }
                );
        }

        $result = $query->get();

        return self::getEntriesAsDto($result);
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(ListModel $entry): ADto
    {
        return (new Dto\ListModel())
            ->setId($entry->{ListModel::COLUMN_ID})
            ->setName($entry->{ListModel::COLUMN_NAME})
            ->setCreatedAt($entry->{ListModel::CREATED_AT})
            ->setUpdatedAt($entry->{ListModel::UPDATED_AT});
    }
}
