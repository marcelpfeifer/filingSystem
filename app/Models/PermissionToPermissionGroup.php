<?php
/**
 * PermissionToPermissionGroup
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.04.2021
 * Time: 12:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;

use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Model;

class PermissionToPermissionGroup extends Model
{
    /**
     * @var string
     */
    public const COLUMN_PERMISSION_ID = 'permissionId';

    /**
     * @var string
     */
    public const COLUMN_PERMISSION_GROUP_ID = 'permissionGroupId';

    /**
     * @var string
     */
    protected $table = 'permissionToPermissionGroup';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param int $permissionGroupId
     * @return int[]
     */
    public function getPermissionsIdsByPermissionGroupId(int $permissionGroupId)
    {
        $result = [];

        $entries = $this
            ->where(self::COLUMN_PERMISSION_GROUP_ID, $permissionGroupId)
            ->get();
        foreach ($entries as $entry) {
            /**
             * @var $dto \App\Models\Dto\PermissionToPermissionGroup
             */
            $dto = self::getEntryAsDto($entry);
            $result[] = $dto->getPermissionId();
        }
        return $result;
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\PermissionToPermissionGroup
         */
        return $this->insertGetId(
            [
                PermissionToPermissionGroup::COLUMN_PERMISSION_ID       => $dto->getPermissionId(),
                PermissionToPermissionGroup::COLUMN_PERMISSION_GROUP_ID => $dto->getPermissionGroupId(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\PermissionToPermissionGroup
         */
        $this->where(PermissionToPermissionGroup::COLUMN_PERMISSION_GROUP_ID, $dto->getPermissionGroupId())->delete();
    }


    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = PermissionToPermissionGroup::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(PermissionToPermissionGroup $entry): ADto
    {
        return (new Dto\PermissionToPermissionGroup())
            ->setPermissionId($entry->{PermissionToPermissionGroup::COLUMN_PERMISSION_ID})
            ->setPermissionGroupId($entry->{PermissionToPermissionGroup::COLUMN_PERMISSION_GROUP_ID});
    }
}
