<?php
/**
 * IncidentToProfile
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 12:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;

use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class IncidentToProfile extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_INCIDENT_ID = 'incidentId';

    /**
     * @var string
     */
    const COLUMN_PROFILE_ID = 'profileId';

    /**
     * @var string
     */
    const COLUMN_WARRANT = 'warrant';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'incidentToProfile';

    /**
     * @return HasOne
     */
    public function profile(): HasOne
    {
        return $this->hasOne(Profile::class, Incident::COLUMN_ID, IncidentToProfile::COLUMN_PROFILE_ID);
    }

    /**
     * @return HasOne
     */
    public function incident(): HasOne
    {
        return $this->hasOne(Incident::class, Incident::COLUMN_ID, IncidentToProfile::COLUMN_INCIDENT_ID);
    }

    /**
     * @return HasMany
     */
    public function charges(): HasMany
    {
        return $this->hasMany(
            IncidentToProfileCharge::class,
            IncidentToProfileCharge::COLUMN_INCIDENT_TO_PROFILE_ID,
            IncidentToProfile::COLUMN_ID
        );
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\IncidentToProfile
         */
        return $this->insertGetId(
            [
                IncidentToProfile::COLUMN_INCIDENT_ID => $dto->getIncidentId(),
                IncidentToProfile::COLUMN_PROFILE_ID  => $dto->getProfileId(),
                IncidentToProfile::COLUMN_WARRANT     => $dto->isWarrant(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\IncidentToProfile
         */
        return $this->where(IncidentToProfile::COLUMN_ID, $dto->getId())->update(
            [
                IncidentToProfile::COLUMN_INCIDENT_ID => $dto->getIncidentId(),
                IncidentToProfile::COLUMN_PROFILE_ID  => $dto->getProfileId(),
                IncidentToProfile::COLUMN_WARRANT     => $dto->isWarrant(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\IncidentToProfile
         */
        $this->where(IncidentToProfile::COLUMN_ID, $dto->getId())->delete();
    }

    public function deleteEntryByIncidentIdAndProfileId(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\IncidentToProfile
         */
        $this->where(IncidentToProfile::COLUMN_INCIDENT_ID, $dto->getIncidentId())
            ->where(IncidentToProfile::COLUMN_PROFILE_ID, $dto->getProfileId())
            ->delete();
    }

    /**
     * @param ADto $dto
     * @return mixed
     */
    public function getEntryByIncidentIdAndProfileId(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\IncidentToProfile
         */
        return $this->where(IncidentToProfile::COLUMN_INCIDENT_ID, $dto->getIncidentId())
            ->where(IncidentToProfile::COLUMN_PROFILE_ID, $dto->getProfileId())
            ->first();
    }

    /**
     * @return mixed
     */
    public function getAllWarrants()
    {
        return $this->where(IncidentToProfile::COLUMN_WARRANT, true)->get();
    }

    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = IncidentToProfile::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(IncidentToProfile $entry): ADto
    {
        return (new Dto\IncidentToProfile())
            ->setId($entry->{IncidentToProfile::COLUMN_ID})
            ->setIncidentId($entry->{IncidentToProfile::COLUMN_INCIDENT_ID})
            ->setProfileId($entry->{IncidentToProfile::COLUMN_PROFILE_ID})
            ->setWarrant($entry->{IncidentToProfile::COLUMN_WARRANT});
    }
}
