<?php
/**
 * ProfilePropertyGroup
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 22.07.2021
 * Time: 09:46
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfilePropertyGroup extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    protected $table = 'profilePropertyGroup';

    /**
     * @var array
     */
    protected $dates = [
        ProfilePropertyGroup::CREATED_AT,
        ProfilePropertyGroup::UPDATED_AT,
    ];

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\ProfilePropertyGroup
         */
        return $this->insertGetId(
            [
                ProfilePropertyGroup::COLUMN_NAME => $dto->getName(),
                ProfilePropertyGroup::CREATED_AT  => $dto->getCreatedAt() ?? Carbon::now(),
                ProfilePropertyGroup::UPDATED_AT  => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\ProfilePropertyGroup
         */
        return $this->where(ProfilePropertyGroup::COLUMN_ID, $dto->getId())->update(
            [
                ProfilePropertyGroup::COLUMN_NAME => $dto->getName(),
                ProfilePropertyGroup::UPDATED_AT  => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\ProfilePropertyGroup
         */
        $this->where(ProfilePropertyGroup::COLUMN_ID, $dto->getId())->delete();
    }


    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $dto = ProfilePropertyGroup::getEntryAsDto($entry);
            $result[$dto->getId()] = $dto;
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(ProfilePropertyGroup $entry): ADto
    {
        return (new Dto\ProfilePropertyGroup())
            ->setId($entry->{ProfilePropertyGroup::COLUMN_ID})
            ->setName($entry->{ProfilePropertyGroup::COLUMN_NAME})
            ->setCreatedAt($entry->{ProfilePropertyGroup::CREATED_AT})
            ->setUpdatedAt($entry->{ProfilePropertyGroup::UPDATED_AT});
    }
}
