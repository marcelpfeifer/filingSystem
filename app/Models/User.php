<?php

namespace App\Models;

use App\Models\Dto\ADto;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_EMAIL = 'email';

    /**
     * @var string
     */
    const COLUMN_PASSWORD = 'password';

    /**
     * @var string
     */
    const COLUMN_PERMISSION_GROUP_ID = 'permissionGroupId';

    /**
     * @var string
     */
    const COLUMN_GROUP_ID = 'groupId';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
        'can',
    ];

    /**
     * @return HasOne
     */
    public function group(): HasOne
    {
        return $this->hasOne(Group::class, Group::COLUMN_ID, User::COLUMN_GROUP_ID);
    }

    /**
     * @return array
     */
    public function getCanAttribute()
    {
        return \App\Libary\Permission\Permission::getPermissionNames();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function deleteById(int $id)
    {
        return $this->where(self::COLUMN_ID, $id)->delete();
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\User
         */
        return $this->insertGetId(
            [
                User::COLUMN_NAME                => $dto->getName(),
                User::COLUMN_EMAIL               => $dto->getName() . '@test.de',
                User::COLUMN_PASSWORD            => $dto->getPassword(),
                User::COLUMN_PERMISSION_GROUP_ID => $dto->getPermissionGroupId(),
                User::COLUMN_GROUP_ID            => $dto->getGroupId(),
                User::CREATED_AT                 => $dto->getCreatedAt(),
                User::UPDATED_AT                 => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\User
         */
        return $this->where(User::COLUMN_ID, $dto->getId())->update(
            [
                User::COLUMN_NAME                => $dto->getName(),
                User::COLUMN_PASSWORD            => $dto->getPassword(),
                User::COLUMN_PERMISSION_GROUP_ID => $dto->getPermissionGroupId(),
                User::COLUMN_GROUP_ID            => $dto->getGroupId(),
                User::UPDATED_AT                 => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\User
         */
        $this->where(Profile::COLUMN_ID, $dto->getId())->delete();
    }


    /**
     * @param string $name
     * @param int $offset
     * @param int $limit
     * @return ADto[]
     */
    public function search(string $name = '', int $offset = 0, int $limit = 15): array
    {
        $entries = $this
            ->when(
                $name,
                function ($query, $name) {
                    return $query->where(User::COLUMN_NAME, 'LIKE', "%$name%");
                }
            )
            ->offset($offset)
            ->limit($limit)
            ->get();
        return User::getEntriesAsDto($entries);
    }

    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = User::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(User $entry): ADto
    {
        return (new Dto\User())
            ->setId($entry->{User::COLUMN_ID})
            ->setName($entry->{User::COLUMN_NAME})
            ->setPassword($entry->{User::COLUMN_PASSWORD})
            ->setPermissionGroupId($entry->{User::COLUMN_PERMISSION_GROUP_ID})
            ->setGroupId($entry->{User::COLUMN_GROUP_ID})
            ->setCreatedAt($entry->{User::CREATED_AT})
            ->setUpdatedAt($entry->{User::UPDATED_AT});
    }
}
