<?php
/**
 * Charge
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 06.05.2021
 * Time: 15:04
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;

use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Charge extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_DESCRIPTION = 'description';

    /**
     * @var string
     */
    const COLUMN_CHARGE_GROUP_ID = 'chargeGroupId';

    /**
     * @var string
     */
    const COLUMN_PRIORITY = 'priority';

    /**
     * @var string
     */
    const COLUMN_MONEY = 'money';

    /**
     * @var string
     */
    const COLUMN_TIME = 'time';

    /**
     * @var string
     */
    protected $table = 'charge';

    /**
     * @var array
     */
    protected $dates = [
        Charge::CREATED_AT,
        Charge::UPDATED_AT,
    ];

    /**
     * @return HasOne
     */
    public function chargeGroup(): HasOne
    {
        return $this->hasOne(ChargeGroup::class, ChargeGroup::COLUMN_ID, Charge::COLUMN_CHARGE_GROUP_ID);
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\Charge
         */
        return $this->insertGetId(
            [
                Charge::COLUMN_NAME            => $dto->getName(),
                Charge::COLUMN_DESCRIPTION     => $dto->getDescription(),
                Charge::COLUMN_CHARGE_GROUP_ID => $dto->getChargeGroupId(),
                Charge::COLUMN_PRIORITY        => $dto->getPriority(),
                Charge::COLUMN_MONEY           => $dto->getMoney(),
                Charge::COLUMN_TIME            => $dto->getTime(),
                Charge::CREATED_AT             => $dto->getCreatedAt(),
                Charge::UPDATED_AT             => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\Charge
         */
        return $this->where(Charge::COLUMN_ID, $dto->getId())->update(
            [
                Charge::COLUMN_NAME            => $dto->getName(),
                Charge::COLUMN_DESCRIPTION     => $dto->getDescription(),
                Charge::COLUMN_CHARGE_GROUP_ID => $dto->getChargeGroupId(),
                Charge::COLUMN_PRIORITY        => $dto->getPriority(),
                Charge::COLUMN_MONEY           => $dto->getMoney(),
                Charge::COLUMN_TIME            => $dto->getTime(),
                Charge::UPDATED_AT             => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\Charge
         */
        $this->where(Charge::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param string $name
     * @return ADto[]
     */
    public function search(string $name = ''): array
    {
        $entries = $this
            ->when(
                $name,
                function ($query, $name) {
                    return $query->where(self::COLUMN_NAME, 'LIKE', "%$name%");
                }
            )
            ->get();
        return Charge::getEntriesAsDto($entries);
    }

    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = Charge::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(Charge $entry): ADto
    {
        return (new Dto\Charge())
            ->setId($entry->{Charge::COLUMN_ID})
            ->setName($entry->{Charge::COLUMN_NAME})
            ->setDescription($entry->{Charge::COLUMN_DESCRIPTION})
            ->setChargeGroupId($entry->{Charge::COLUMN_CHARGE_GROUP_ID})
            ->setPriority($entry->{Charge::COLUMN_PRIORITY})
            ->setMoney($entry->{Charge::COLUMN_MONEY})
            ->setTime($entry->{Charge::COLUMN_TIME})
            ->setCreatedAt($entry->{Charge::CREATED_AT})
            ->setUpdatedAt($entry->{Charge::UPDATED_AT});
    }
}
