<?php
/**
 * Tag
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 16.07.2021
 * Time: 15:17
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_INCIDENT = 'incident';

    /**
     * @var string
     */
    const COLUMN_PROFILE = 'profile';

    /**
     * @var array
     */
    protected $dates = [
        Tag::CREATED_AT,
        Tag::UPDATED_AT,
    ];

    /**
     * @var string
     */
    protected $table = 'tag';

    /**
     * @param string|null $name
     * @param bool|null $incident
     * @param bool|null $profile
     * @return ADto[]
     */
    public function search(?string $name, ?bool $incident = null, ?bool $profile = null): array
    {
        $query = $this
            ->when(
                $name,
                function ($query, $name) {
                    return $query->where(self::COLUMN_NAME, $name);
                }
            );
        if ($incident !== null) {
            $query = $query->where(self::COLUMN_INCIDENT, $incident);
        }

        if ($profile !== null) {
            $query = $query->where(self::COLUMN_PROFILE, $profile);
        }

        $results = $query->get();
        return self::getEntriesAsDto($results);
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\Tag
         */
        return $this->insertGetId(
            [
                Tag::COLUMN_NAME     => $dto->getName(),
                Tag::COLUMN_INCIDENT => $dto->isIncident(),
                Tag::COLUMN_PROFILE  => $dto->isProfile(),
                Tag::CREATED_AT      => $dto->getCreatedAt() ?? Carbon::now(),
                Tag::UPDATED_AT      => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\Tag
         */
        return $this->where(Tag::COLUMN_ID, $dto->getId())->update(
            [
                Tag::COLUMN_NAME     => $dto->getName(),
                Tag::COLUMN_INCIDENT => $dto->isIncident(),
                Tag::COLUMN_PROFILE  => $dto->isProfile(),
                Tag::UPDATED_AT      => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\Tag
         */
        $this->where(Tag::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = Tag::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(Tag $entry): ADto
    {
        return (new Dto\Tag())
            ->setId($entry->{Tag::COLUMN_ID})
            ->setName($entry->{Tag::COLUMN_NAME})
            ->setIncident($entry->{Tag::COLUMN_INCIDENT})
            ->setProfile($entry->{Tag::COLUMN_PROFILE})
            ->setCreatedAt($entry->{Tag::CREATED_AT})
            ->setUpdatedAt($entry->{Tag::UPDATED_AT});
    }
}
