<?php
/**
 * Profile
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.04.2021
 * Time: 19:55
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;

use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Facades\Log;

class Profile extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_DESCRIPTION = 'description';

    /**
     * @var string
     */
    const COLUMN_IMAGE_URL = 'imageUrl';

    /**
     * @var string
     */
    protected $table = 'profile';

    /**
     * @var array
     */
    protected $dates = [
        Profile::CREATED_AT,
        Profile::UPDATED_AT,
    ];

    /**
     * @return HasMany
     */
    public function profilesToIncident(): HasMany
    {
        return $this->hasMany(IncidentToProfile::class, IncidentToProfile::COLUMN_PROFILE_ID, Profile::COLUMN_ID);
    }

    /**
     * @return HasMany
     */
    public function tags(): HasMany
    {
        return $this->hasMany(ProfileToTag::class, ProfileToTag::COLUMN_PROFILE_ID, Profile::COLUMN_ID);
    }

    /**
     * @return HasMany
     */
    public function propertyValues(): HasMany
    {
        return $this->hasMany(ProfilePropertyValue::class, ProfilePropertyValue::COLUMN_PROFILE_ID, Profile::COLUMN_ID);
    }

    /**
     * @return HasManyThrough
     */
    public function incidentToProfileCharges(): HasManyThrough
    {
        return $this->hasManyThrough(
            IncidentToProfileCharge::class,
            IncidentToProfile::class,
            IncidentToProfile::COLUMN_PROFILE_ID,
            IncidentToProfileCharge::COLUMN_INCIDENT_TO_PROFILE_ID,
            Profile::COLUMN_ID,
            IncidentToProfile::COLUMN_ID,
        );
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\Profile
         */
        return $this->insertGetId(
            [
                Profile::COLUMN_NAME        => $dto->getName(),
                Profile::COLUMN_DESCRIPTION => $dto->getDescription(),
                Profile::COLUMN_IMAGE_URL   => $dto->getImageUrl(),
                Profile::CREATED_AT         => $dto->getCreatedAt(),
                Profile::UPDATED_AT         => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\Profile
         */
        return $this->where(Profile::COLUMN_ID, $dto->getId())->update(
            [
                Profile::COLUMN_NAME        => $dto->getName(),
                Profile::COLUMN_DESCRIPTION => $dto->getDescription(),
                Profile::COLUMN_IMAGE_URL   => $dto->getImageUrl(),
                Profile::UPDATED_AT         => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\Profile
         */
        $this->where(Profile::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param string $name
     * @return Collection
     */
    public function search(string $name = '', array $tags = []): Collection
    {
        $query = $this
            ->with('tags')
            ->when(
                $name,
                function ($query, $name) {
                    return $query->where(self::COLUMN_NAME, 'LIKE', "%$name%");
                }
            );
        if (count($tags) !== 0) {
            $query = $query->whereHas(
                'tags',
                function ($query) use ($tags) {
                    return $query->whereIn(ProfileToTag::COLUMN_TAG_ID, $tags);
                }
            );
        }
        return $query
            ->limit(20)
            ->get();
    }

    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = Profile::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(Profile $entry): ADto
    {
        return (new Dto\Profile())
            ->setId($entry->{Profile::COLUMN_ID})
            ->setName($entry->{Profile::COLUMN_NAME})
            ->setDescription($entry->{Profile::COLUMN_DESCRIPTION})
            ->setImageUrl($entry->{Profile::COLUMN_IMAGE_URL})
            ->setCreatedAt($entry->{Profile::CREATED_AT})
            ->setUpdatedAt($entry->{Profile::UPDATED_AT});
    }

}
