<?php
/**
 * ProfilePropertyValue
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 22.07.2021
 * Time: 10:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ProfilePropertyValue extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_PROFILE_ID = 'profileId';

    /**
     * @var string
     */
    const COLUMN_PROFILE_PROPERTY_ID = 'profilePropertyId';

    /**
     * @var string
     */
    const COLUMN_VALUE = 'value';

    /**
     * @var string
     */
    protected $table = 'profilePropertyValue';

    /**
     * @var array
     */
    protected $dates = [
        ProfilePropertyValue::CREATED_AT,
        ProfilePropertyValue::UPDATED_AT,
    ];

    /**
     * @return HasOne
     */
    public function profile(): HasOne
    {
        return $this->hasOne(
            Profile::class,
            Profile::COLUMN_ID,
            ProfilePropertyValue::COLUMN_PROFILE_ID
        );
    }

    /**
     * @return HasOne
     */
    public function property(): HasOne
    {
        return $this->hasOne(
            ProfileProperty::class,
            ProfileProperty::COLUMN_ID,
            ProfilePropertyValue::COLUMN_PROFILE_PROPERTY_ID
        );
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\ProfilePropertyValue
         */
        return $this->insertGetId(
            [
                ProfilePropertyValue::COLUMN_PROFILE_ID          => $dto->getProfileId(),
                ProfilePropertyValue::COLUMN_PROFILE_PROPERTY_ID => $dto->getProfilePropertyId(),
                ProfilePropertyValue::COLUMN_VALUE               => $dto->getValue(),
                ProfilePropertyValue::CREATED_AT                 => $dto->getCreatedAt() ?? Carbon::now(),
                ProfilePropertyValue::UPDATED_AT                 => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\ProfilePropertyValue
         */
        return $this->where(ProfilePropertyValue::COLUMN_ID, $dto->getId())->update(
            [
                ProfilePropertyValue::COLUMN_PROFILE_ID          => $dto->getProfileId(),
                ProfilePropertyValue::COLUMN_PROFILE_PROPERTY_ID => $dto->getProfilePropertyId(),
                ProfilePropertyValue::COLUMN_VALUE               => $dto->getValue(),
                ProfilePropertyValue::UPDATED_AT                 => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\ProfilePropertyValue
         */
        $this->where(ProfilePropertyValue::COLUMN_ID, $dto->getId())->delete();
    }


    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = ProfilePropertyValue::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(ProfilePropertyValue $entry): ADto
    {
        return (new Dto\ProfilePropertyValue())
            ->setId($entry->{ProfilePropertyValue::COLUMN_ID})
            ->setProfileId($entry->{ProfilePropertyValue::COLUMN_PROFILE_ID})
            ->setProfilePropertyId($entry->{ProfilePropertyValue::COLUMN_PROFILE_PROPERTY_ID})
            ->setValue($entry->{ProfilePropertyValue::COLUMN_VALUE})
            ->setCreatedAt($entry->{ProfilePropertyValue::CREATED_AT})
            ->setUpdatedAt($entry->{ProfilePropertyValue::UPDATED_AT});
    }
}
