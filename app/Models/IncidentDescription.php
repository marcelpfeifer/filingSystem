<?php
/**
 * IncidentDescription
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.05.2021
 * Time: 14:55
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;

use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class IncidentDescription extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_INCIDENT_ID = 'incidentId';

    /**
     * @var string
     */
    const COLUMN_DESCRIPTION = 'description';


    /**
     * @var array
     */
    protected $dates = [
        IncidentDescription::CREATED_AT,
        IncidentDescription::UPDATED_AT,
    ];

    /**
     * @var string
     */
    protected $table = 'incidentDescription';

    /**
     * @return HasOne
     */
    public function incident(): HasOne
    {
        return $this->hasOne(Incident::class, Incident::COLUMN_ID, IncidentDescription::COLUMN_INCIDENT_ID);
    }

    /**
     * @return HasMany
     */
    public function changelogs(): HasMany
    {
        return $this->hasMany(
            IncidentDescriptionChangelog::class,
            IncidentDescriptionChangelog::COLUMN_INCIDENT_DESCRIPTION_ID,
            IncidentDescription::COLUMN_ID
        );
    }


    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\IncidentDescription
         */
        return $this->insertGetId(
            [
                IncidentDescription::COLUMN_INCIDENT_ID => $dto->getIncidentId(),
                IncidentDescription::COLUMN_DESCRIPTION => $dto->getDescription(),
                IncidentDescription::CREATED_AT         => $dto->getCreatedAt(),
                IncidentDescription::UPDATED_AT         => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\IncidentDescription
         */
        return $this->where(IncidentDescription::COLUMN_ID, $dto->getId())->update(
            [
                IncidentDescription::COLUMN_INCIDENT_ID => $dto->getIncidentId(),
                IncidentDescription::COLUMN_DESCRIPTION => $dto->getDescription(),
                IncidentDescription::UPDATED_AT         => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\Incident
         */
        $this->where(IncidentDescription::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = IncidentDescription::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(IncidentDescription $entry): ADto
    {
        return (new Dto\IncidentDescription())
            ->setId($entry->{IncidentDescription::COLUMN_ID})
            ->setIncidentId($entry->{IncidentDescription::COLUMN_INCIDENT_ID})
            ->setDescription($entry->{IncidentDescription::COLUMN_DESCRIPTION})
            ->setCreatedAt($entry->{IncidentDescription::CREATED_AT})
            ->setUpdatedAt($entry->{IncidentDescription::UPDATED_AT});
    }
}
