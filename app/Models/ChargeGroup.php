<?php
/**
 * ChargeGroup
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 06.05.2021
 * Time: 15:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;

use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChargeGroup extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    protected $table = 'chargeGroup';

    /**
     * @var array
     */
    protected $dates = [
        ChargeGroup::CREATED_AT,
        ChargeGroup::UPDATED_AT,
    ];

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\ChargeGroup
         */
        return $this->insertGetId(
            [
                ChargeGroup::COLUMN_NAME => $dto->getName(),
                ChargeGroup::CREATED_AT  => $dto->getCreatedAt(),
                ChargeGroup::UPDATED_AT  => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\ChargeGroup
         */
        return $this->where(ChargeGroup::COLUMN_ID, $dto->getId())->update(
            [
                ChargeGroup::COLUMN_NAME => $dto->getName(),
                ChargeGroup::UPDATED_AT  => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\ChargeGroup
         */
        $this->where(ChargeGroup::COLUMN_ID, $dto->getId())->delete();
    }


    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = ChargeGroup::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(ChargeGroup $entry): ADto
    {
        return (new Dto\ChargeGroup())
            ->setId($entry->{ChargeGroup::COLUMN_ID})
            ->setName($entry->{ChargeGroup::COLUMN_NAME})
            ->setCreatedAt($entry->{ChargeGroup::CREATED_AT})
            ->setUpdatedAt($entry->{ChargeGroup::UPDATED_AT});
    }
}
