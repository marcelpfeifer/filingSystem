<?php
/**
 * News
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 05.06.2021
 * Time: 19:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_TITLE = 'title';

    /**
     * @var string
     */
    const COLUMN_CONTENT = 'content';

    /**
     * @var string
     */
    protected $table = 'news';

    /**
     * @var array
     */
    protected $dates = [
        News::CREATED_AT,
        News::UPDATED_AT,
    ];

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\News
         */
        return $this->insertGetId(
            [
                News::COLUMN_TITLE   => $dto->getTitle(),
                News::COLUMN_CONTENT => $dto->getContent(),
                News::CREATED_AT     => $dto->getCreatedAt(),
                News::UPDATED_AT     => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\News
         */
        return $this->where(News::COLUMN_ID, $dto->getId())->update(
            [
                News::COLUMN_TITLE   => $dto->getTitle(),
                News::COLUMN_CONTENT => $dto->getContent(),
                News::UPDATED_AT     => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\News
         */
        $this->where(News::COLUMN_ID, $dto->getId())->delete();
    }


    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = News::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @return ADto[]
     */
    public function getNews(): array
    {
        $news = $this
            ->orderBy(self::CREATED_AT, 'DESC')
            ->limit(20)
            ->get();
        return self::getEntriesAsDto($news);
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(News $entry): ADto
    {
        return (new Dto\News())
            ->setId($entry->{News::COLUMN_ID})
            ->setTitle($entry->{News::COLUMN_TITLE})
            ->setContent($entry->{News::COLUMN_CONTENT})
            ->setCreatedAt($entry->{News::CREATED_AT})
            ->setUpdatedAt($entry->{News::UPDATED_AT});
    }
}
