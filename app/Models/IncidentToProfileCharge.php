<?php
/**
 * IncidentToProfileCharge
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 12:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class IncidentToProfileCharge extends Model
{

    /**
     * @var string
     */
    const COLUMN_INCIDENT_TO_PROFILE_ID = 'incidentToProfileId';

    /**
     * @var string
     */
    const COLUMN_CHARGE_ID = 'chargeId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'incidentToProfileCharge';


    /**
     * @return HasOne
     */
    public function charge(): HasOne
    {
        return $this->hasOne(Charge::class, Charge::COLUMN_ID, IncidentToProfileCharge::COLUMN_CHARGE_ID);
    }

    /**
     * @return HasOne
     */
    public function incidentToProfile(): HasOne
    {
        return $this->hasOne(
            IncidentToProfile::class,
            IncidentToProfile::COLUMN_ID,
            IncidentToProfileCharge::COLUMN_INCIDENT_TO_PROFILE_ID
        );
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\IncidentToProfileCharge
         */
        return $this->insert(
            [
                IncidentToProfileCharge::COLUMN_INCIDENT_TO_PROFILE_ID => $dto->getIncidentToProfileId(),
                IncidentToProfileCharge::COLUMN_CHARGE_ID              => $dto->getChargeId(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntries(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\IncidentToProfileCharge
         */
        $this->where(IncidentToProfileCharge::COLUMN_INCIDENT_TO_PROFILE_ID, $dto->getIncidentToProfileId())->delete();
    }

    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = IncidentToProfileCharge::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(IncidentToProfileCharge $entry): ADto
    {
        return (new Dto\IncidentToProfileCharge())
            ->setIncidentToProfileId($entry->{IncidentToProfileCharge::COLUMN_INCIDENT_TO_PROFILE_ID})
            ->setChargeId($entry->{IncidentToProfileCharge::COLUMN_CHARGE_ID});
    }
}
