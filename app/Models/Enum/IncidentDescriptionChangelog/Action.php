<?php
/**
 * Action
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.05.2021
 * Time: 15:05
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Enum\IncidentDescriptionChangelog;


class Action
{
    /**
     * @var string
     */
    const INSERT = 'INSERT';

    /**
     * @var string
     */
    const UPDATE = 'UPDATE';

    /**
     * @var string
     */
    const DELETE = 'DELETE';
}
