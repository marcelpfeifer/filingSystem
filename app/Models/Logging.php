<?php
/**
 * WebAdminLogging
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 10.04.2020
 * Time: 09:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class Logging extends Model
{

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_USER_ID = 'userId';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_ACTION = 'action';

    /**
     * @var string
     */
    const COLUMN_DATE_TIME = 'dateTime';

    /**
     * @var string
     */
    protected $table = 'logging';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $dates = [
        self::COLUMN_DATE_TIME
    ];

    /**
     * @param Dto\Logging $dto
     */
    public function insertEntry(Dto\Logging $dto)
    {
        return $this->insertGetId(
            [
                self::COLUMN_USER_ID   => $dto->getUserId(),
                self::COLUMN_NAME      => $dto->getName(),
                self::COLUMN_ACTION    => $dto->getAction(),
                self::COLUMN_DATE_TIME => $dto->getDateTime(),
            ]
        );
    }

    /**
     * @param Dto\Logging $dto
     */
    public function updateEntry(Dto\Logging $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->update(
            [
                self::COLUMN_USER_ID   => $dto->getUserId(),
                self::COLUMN_NAME      => $dto->getName(),
                self::COLUMN_ACTION    => $dto->getAction(),
                self::COLUMN_DATE_TIME => $dto->getDateTime(),
            ]
        );
    }

    /**
     * @param Dto\Logging $dto
     */
    public function deleteEntry(Dto\Logging $dto)
    {
        $this->where(self::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param Logging $entry
     * @return Dto\Logging
     */
    public static function getEntryAsDto(Logging $entry): Dto\Logging
    {
        return (new Dto\Logging())
            ->setId($entry->{self::COLUMN_ID})
            ->setUserId($entry->{self::COLUMN_USER_ID})
            ->setName($entry->{self::COLUMN_NAME})
            ->setAction($entry->{self::COLUMN_ACTION})
            ->setDateTime($entry->{self::COLUMN_DATE_TIME});
    }
}
