<?php
/**
 * ListEntry
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 23.07.2021
 * Time: 15:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;


use App\Libary\Logging\ILogging;
use App\Libary\Logging\Traits\HasLogging;

class ListEntry extends ADto implements ILogging
{

    use HasLogging;

    /**
     * @var int
     */
    private $listId = 0;

    /**
     * @return int
     */
    public function getListId(): int
    {
        return $this->listId;
    }

    /**
     * @param int $listId
     * @return ListEntry
     */
    public function setListId(int $listId): ListEntry
    {
        $this->listId = $listId;
        return $this;
    }
}
