<?php
/**
 * ProfilePropertyValue
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 22.07.2021
 * Time: 10:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;


use App\Libary\Logging\ILogging;
use App\Libary\Logging\Traits\HasLogging;

class ProfilePropertyValue extends ADto implements ILogging
{

    use HasLogging;

    /**
     * @var int
     */
    private $profileId = 0;

    /**
     * @var int
     */
    private $profilePropertyId = 0;

    /**
     * @var string
     */
    private $value = '';

    /**
     * @return int
     */
    public function getProfileId(): int
    {
        return $this->profileId;
    }

    /**
     * @param int $profileId
     * @return ProfilePropertyValue
     */
    public function setProfileId(int $profileId): ProfilePropertyValue
    {
        $this->profileId = $profileId;
        return $this;
    }

    /**
     * @return int
     */
    public function getProfilePropertyId(): int
    {
        return $this->profilePropertyId;
    }

    /**
     * @param int $profilePropertyId
     * @return ProfilePropertyValue
     */
    public function setProfilePropertyId(int $profilePropertyId): ProfilePropertyValue
    {
        $this->profilePropertyId = $profilePropertyId;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return ProfilePropertyValue
     */
    public function setValue(string $value): ProfilePropertyValue
    {
        $this->value = $value;
        return $this;
    }
}
