<?php
/**
 * IncidentToGroup
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 12:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;


class IncidentToGroup extends ADto
{

    /**
     * @var int
     */
    private $incidentId = 0;

    /**
     * @var int
     */
    private $groupId = 0;

    /**
     * @return int
     */
    public function getIncidentId(): int
    {
        return $this->incidentId;
    }

    /**
     * @param int $incidentId
     * @return IncidentToGroup
     */
    public function setIncidentId(int $incidentId): IncidentToGroup
    {
        $this->incidentId = $incidentId;
        return $this;
    }

    /**
     * @return int
     */
    public function getGroupId(): int
    {
        return $this->groupId;
    }

    /**
     * @param int $groupId
     * @return IncidentToGroup
     */
    public function setGroupId(int $groupId): IncidentToGroup
    {
        $this->groupId = $groupId;
        return $this;
    }

}
