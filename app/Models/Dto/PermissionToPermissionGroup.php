<?php
/**
 * PermissionToPermissionGroup
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.04.2021
 * Time: 12:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;

class PermissionToPermissionGroup extends ADto
{
    /**
     * @var int
     */
    private $permissionId = 0;

    /**
     * @var int
     */
    private $permissionGroupId = 0;

    /**
     * @return int
     */
    public function getPermissionId(): int
    {
        return $this->permissionId;
    }

    /**
     * @param int $permissionId
     * @return PermissionToPermissionGroup
     */
    public function setPermissionId(int $permissionId): PermissionToPermissionGroup
    {
        $this->permissionId = $permissionId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPermissionGroupId(): int
    {
        return $this->permissionGroupId;
    }

    /**
     * @param int $permissionGroupId
     * @return PermissionToPermissionGroup
     */
    public function setPermissionGroupId(int $permissionGroupId): PermissionToPermissionGroup
    {
        $this->permissionGroupId = $permissionGroupId;
        return $this;
    }
}
