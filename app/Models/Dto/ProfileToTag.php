<?php
/**
 * IncidentToTag
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 16.07.2021
 * Time: 15:28
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;

use App\Libary\Logging\ILogging;
use App\Libary\Logging\Traits\HasLogging;

class ProfileToTag extends ADto implements ILogging
{

    use HasLogging;

    /**
     * @var int
     */
    private $profileId = 0;

    /**
     * @var int
     */
    private $tagId = 0;

    /**
     * @return int
     */
    public function getProfileId(): int
    {
        return $this->profileId;
    }

    /**
     * @param int $profileId
     * @return ProfileToTag
     */
    public function setProfileId(int $profileId): ProfileToTag
    {
        $this->profileId = $profileId;
        return $this;
    }

    /**
     * @return int
     */
    public function getTagId(): int
    {
        return $this->tagId;
    }

    /**
     * @param int $tagId
     * @return ProfileToTag
     */
    public function setTagId(int $tagId): ProfileToTag
    {
        $this->tagId = $tagId;
        return $this;
    }
}
