<?php
/**
 * ListHeader
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 23.07.2021
 * Time: 15:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;


use App\Libary\Logging\ILogging;
use App\Libary\Logging\Traits\HasLogging;

class ListHeader extends ADto implements ILogging
{

    use HasLogging;

    /**
     * @var int
     */
    private $listId = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var boolean
     */
    private $list = false;

    /**
     * @var int
     */
    private $order = 0;

    /**
     * @return int
     */
    public function getListId(): int
    {
        return $this->listId;
    }

    /**
     * @param int $listId
     * @return ListHeader
     */
    public function setListId(int $listId): ListHeader
    {
        $this->listId = $listId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ListHeader
     */
    public function setName(string $name): ListHeader
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isList(): bool
    {
        return $this->list;
    }

    /**
     * @param bool $list
     * @return ListHeader
     */
    public function setList(bool $list): ListHeader
    {
        $this->list = $list;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     * @return ListHeader
     */
    public function setOrder(int $order): ListHeader
    {
        $this->order = $order;
        return $this;
    }
}
