<?php
/**
 * ProfileProperty
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 22.07.2021
 * Time: 09:50
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;


use App\Libary\Logging\ILogging;
use App\Libary\Logging\Traits\HasLogging;

class ProfileProperty extends ADto implements ILogging
{

    use HasLogging;

    /**
     * @var int
     */
    private $profilePropertyGroupId = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @return int
     */
    public function getProfilePropertyGroupId(): int
    {
        return $this->profilePropertyGroupId;
    }

    /**
     * @param int $profilePropertyGroupId
     * @return ProfileProperty
     */
    public function setProfilePropertyGroupId(int $profilePropertyGroupId): ProfileProperty
    {
        $this->profilePropertyGroupId = $profilePropertyGroupId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ProfileProperty
     */
    public function setName(string $name): ProfileProperty
    {
        $this->name = $name;
        return $this;
    }
}
