<?php
/**
 * Permission
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.04.2021
 * Time: 12:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;


class Permission extends ADto
{

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var int|null
     */
    private $parentPermission = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Permission
     */
    public function setName(string $name): Permission
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Permission
     */
    public function setDescription(string $description): Permission
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getParentPermission(): ?int
    {
        return $this->parentPermission;
    }

    /**
     * @param int|null $parentPermission
     * @return Permission
     */
    public function setParentPermission(?int $parentPermission): Permission
    {
        $this->parentPermission = $parentPermission;
        return $this;
    }
}
