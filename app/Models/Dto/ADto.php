<?php
/**
 * ADto
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.04.2021
 * Time: 19:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;

use Carbon\Carbon;

abstract class ADto
{

    /**
     * @var null|int
     */
    private $id = null;

    /**
     * @var null|Carbon
     */
    private $createdAt = null;

    /**
     * @var null|Carbon
     */
    private $updatedAt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return ADto
     */
    public function setId(?int $id): ADto
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getCreatedAt(): ?Carbon
    {
        return $this->createdAt;
    }

    /**
     * @param Carbon|null $createdAt
     * @return ADto
     */
    public function setCreatedAt(?Carbon $createdAt): ADto
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Carbon|null
     */
    public function getUpdatedAt(): ?Carbon
    {
        return $this->updatedAt;
    }

    /**
     * @param Carbon|null $updatedAt
     * @return ADto
     */
    public function setUpdatedAt(?Carbon $updatedAt): ADto
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
