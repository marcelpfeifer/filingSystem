<?php
/**
 * IncidentToGroup
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 12:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;


class ListToGroup extends ADto
{

    /**
     * @var int
     */
    private $listId = 0;

    /**
     * @var int
     */
    private $groupId = 0;

    /**
     * @return int
     */
    public function getListId(): int
    {
        return $this->listId;
    }

    /**
     * @param int $listId
     * @return ListToGroup
     */
    public function setListId(int $listId): ListToGroup
    {
        $this->listId = $listId;
        return $this;
    }

    /**
     * @return int
     */
    public function getGroupId(): int
    {
        return $this->groupId;
    }

    /**
     * @param int $groupId
     * @return ListToGroup
     */
    public function setGroupId(int $groupId): ListToGroup
    {
        $this->groupId = $groupId;
        return $this;
    }
}
