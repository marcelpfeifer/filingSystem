<?php
/**
 * IncidentToProfile
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 12:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;


use App\Libary\Logging\ILogging;
use App\Libary\Logging\Traits\HasLogging;

class IncidentToProfile extends ADto implements ILogging
{

    use HasLogging;

    /**
     * @var int
     */
    private $incidentId = 0;

    /**
     * @var int
     */
    private $profileId = 0;

    /**
     * @var bool
     */
    private $warrant = false;

    /**
     * @return int
     */
    public function getIncidentId(): int
    {
        return $this->incidentId;
    }

    /**
     * @param int $incidentId
     * @return IncidentToProfile
     */
    public function setIncidentId(int $incidentId): IncidentToProfile
    {
        $this->incidentId = $incidentId;
        return $this;
    }

    /**
     * @return int
     */
    public function getProfileId(): int
    {
        return $this->profileId;
    }

    /**
     * @param int $profileId
     * @return IncidentToProfile
     */
    public function setProfileId(int $profileId): IncidentToProfile
    {
        $this->profileId = $profileId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isWarrant(): bool
    {
        return $this->warrant;
    }

    /**
     * @param bool $warrant
     * @return IncidentToProfile
     */
    public function setWarrant(bool $warrant): IncidentToProfile
    {
        $this->warrant = $warrant;
        return $this;
    }
}
