<?php
/**
 * Resolution
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.06.2021
 * Time: 14:39
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;

use App\Libary\Logging\ILogging;
use App\Libary\Logging\Traits\HasLogging;

class Resolution extends ADto implements ILogging
{
    use HasLogging;

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var string
     */
    private $path = '';

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Resolution
     */
    public function setTitle(string $title): Resolution
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return Resolution
     */
    public function setPath(string $path): Resolution
    {
        $this->path = $path;
        return $this;
    }
}
