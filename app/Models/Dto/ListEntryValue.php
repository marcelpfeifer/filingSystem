<?php
/**
 * ListEntryValue
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 23.07.2021
 * Time: 15:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;

use App\Libary\Logging\ILogging;
use App\Libary\Logging\Traits\HasLogging;

class ListEntryValue extends ADto implements ILogging
{

    use HasLogging;

    /**
     * @var int
     */
    private $listEntryId = 0;

    /**
     * @var int
     */
    private $listHeaderId = 0;

    /**
     * @var string
     */
    private $value = '';

    /**
     * @return int
     */
    public function getListEntryId(): int
    {
        return $this->listEntryId;
    }

    /**
     * @param int $listEntryId
     * @return ListEntryValue
     */
    public function setListEntryId(int $listEntryId): ListEntryValue
    {
        $this->listEntryId = $listEntryId;
        return $this;
    }

    /**
     * @return int
     */
    public function getListHeaderId(): int
    {
        return $this->listHeaderId;
    }

    /**
     * @param int $listHeaderId
     * @return ListEntryValue
     */
    public function setListHeaderId(int $listHeaderId): ListEntryValue
    {
        $this->listHeaderId = $listHeaderId;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return ListEntryValue
     */
    public function setValue(string $value): ListEntryValue
    {
        $this->value = $value;
        return $this;
    }
}
