<?php
/**
 * ShareProfile
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 15:36
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;


use App\Libary\Logging\ILogging;
use App\Libary\Logging\Traits\HasLogging;

class ShareProfile extends ADto implements ILogging
{
    use HasLogging;

    /**
     * @var int
     */
    private $profileId = 0;

    /**
     * @var string
     */
    private $code = '';

    /**
     * @return int
     */
    public function getProfileId(): int
    {
        return $this->profileId;
    }

    /**
     * @param int $profileId
     * @return ShareProfile
     */
    public function setProfileId(int $profileId): ShareProfile
    {
        $this->profileId = $profileId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return ShareProfile
     */
    public function setCode(string $code): ShareProfile
    {
        $this->code = $code;
        return $this;
    }
}
