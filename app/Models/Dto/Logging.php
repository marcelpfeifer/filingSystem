<?php
/**
 * Logging
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 22.05.2021
 * Time: 16:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;


use Carbon\Carbon;

class Logging extends ADto
{

    /**
     * @var int
     */
    private $userId = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $action;

    /**
     * @var Carbon
     */
    private $dateTime;

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return Logging
     */
    public function setUserId(int $userId): Logging
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Logging
     */
    public function setName(string $name): Logging
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return Logging
     */
    public function setAction(string $action): Logging
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getDateTime(): Carbon
    {
        return $this->dateTime;
    }

    /**
     * @param Carbon $dateTime
     * @return Logging
     */
    public function setDateTime(Carbon $dateTime): Logging
    {
        $this->dateTime = $dateTime;
        return $this;
    }

}
