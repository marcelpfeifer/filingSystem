<?php
/**
 * User
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.04.2021
 * Time: 15:08
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;

use App\Libary\Logging\ILogging;
use App\Libary\Logging\Traits\HasLogging;

class User extends ADto implements ILogging
{

    use HasLogging;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $password = '';

    /**
     * @var int|null
     */
    private $permissionGroupId = null;

    /**
     * @var int|null
     */
    private $groupId = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPermissionGroupId(): ?int
    {
        return $this->permissionGroupId;
    }

    /**
     * @param int|null $permissionGroupId
     * @return User
     */
    public function setPermissionGroupId(?int $permissionGroupId): User
    {
        $this->permissionGroupId = $permissionGroupId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGroupId(): ?int
    {
        return $this->groupId;
    }

    /**
     * @param int|null $groupId
     * @return User
     */
    public function setGroupId(?int $groupId): User
    {
        $this->groupId = $groupId;
        return $this;
    }
}
