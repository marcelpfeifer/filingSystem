<?php
/**
 * IncidentDescription
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.05.2021
 * Time: 14:47
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;

use App\Libary\Logging\ILogging;
use App\Libary\Logging\Traits\HasLogging;

class IncidentDescription extends ADto implements ILogging
{
    use HasLogging;

    /**
     * @var int
     */
    private $incidentId = 0;

    /**
     * @var string
     */
    private $description = '';

    /**
     * @return int
     */
    public function getIncidentId(): int
    {
        return $this->incidentId;
    }

    /**
     * @param int $incidentId
     * @return IncidentDescription
     */
    public function setIncidentId(int $incidentId): IncidentDescription
    {
        $this->incidentId = $incidentId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return IncidentDescription
     */
    public function setDescription(string $description): IncidentDescription
    {
        $this->description = $description;
        return $this;
    }
}
