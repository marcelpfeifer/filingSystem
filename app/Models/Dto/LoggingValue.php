<?php
/**
 * LoggingValue
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 22.05.2021
 * Time: 16:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;


class LoggingValue extends ADto
{

    /**
     * @var int
     */
    private $loggingId = 0;

    /**
     * @var string
     */
    private $tableName = '';

    /**
     * @var string
     */
    private $columnName = '';

    /**
     * @var string
     */
    private $oldValue = '';

    /**
     * @var string
     */
    private $newValue = '';

    /**
     * @return int
     */
    public function getLoggingId(): int
    {
        return $this->loggingId;
    }

    /**
     * @param int $loggingId
     * @return LoggingValue
     */
    public function setLoggingId(int $loggingId): LoggingValue
    {
        $this->loggingId = $loggingId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }

    /**
     * @param string $tableName
     * @return LoggingValue
     */
    public function setTableName(string $tableName): LoggingValue
    {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getColumnName(): string
    {
        return $this->columnName;
    }

    /**
     * @param string $columnName
     * @return LoggingValue
     */
    public function setColumnName(string $columnName): LoggingValue
    {
        $this->columnName = $columnName;
        return $this;
    }

    /**
     * @return string
     */
    public function getOldValue(): string
    {
        return $this->oldValue;
    }

    /**
     * @param string $oldValue
     * @return LoggingValue
     */
    public function setOldValue(string $oldValue): LoggingValue
    {
        $this->oldValue = $oldValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getNewValue(): string
    {
        return $this->newValue;
    }

    /**
     * @param string $newValue
     * @return LoggingValue
     */
    public function setNewValue(string $newValue): LoggingValue
    {
        $this->newValue = $newValue;
        return $this;
    }
}
