<?php
/**
 * ShareIncident
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 15:36
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;


use App\Libary\Logging\ILogging;
use App\Libary\Logging\Traits\HasLogging;

class ShareIncident extends ADto implements ILogging
{
    use HasLogging;

    /**
     * @var int
     */
    private $incidentId = 0;

    /**
     * @var string
     */
    private $code = '';

    /**
     * @return int
     */
    public function getIncidentId(): int
    {
        return $this->incidentId;
    }

    /**
     * @param int $incidentId
     * @return ShareIncident
     */
    public function setIncidentId(int $incidentId): ShareIncident
    {
        $this->incidentId = $incidentId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return ShareIncident
     */
    public function setCode(string $code): ShareIncident
    {
        $this->code = $code;
        return $this;
    }
}
