<?php
/**
 * Charge
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 06.05.2021
 * Time: 15:10
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;


use App\Libary\Logging\ILogging;
use App\Libary\Logging\Traits\HasLogging;

class Charge extends ADto implements ILogging
{

    use HasLogging;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var int|null
     */
    private $chargeGroupId = null;

    /**
     * @var int
     */
    private $priority = 0;

    /**
     * @var int
     */
    private $money = 0;

    /**
     * @var int
     */
    private $time = 0;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Charge
     */
    public function setName(string $name): Charge
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Charge
     */
    public function setDescription(string $description): Charge
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getChargeGroupId(): ?int
    {
        return $this->chargeGroupId;
    }

    /**
     * @param int|null $chargeGroupId
     * @return Charge
     */
    public function setChargeGroupId(?int $chargeGroupId): Charge
    {
        $this->chargeGroupId = $chargeGroupId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     * @return Charge
     */
    public function setPriority(int $priority): Charge
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return int
     */
    public function getMoney(): int
    {
        return $this->money;
    }

    /**
     * @param int $money
     * @return Charge
     */
    public function setMoney(int $money): Charge
    {
        $this->money = $money;
        return $this;
    }

    /**
     * @return int
     */
    public function getTime(): int
    {
        return $this->time;
    }

    /**
     * @param int $time
     * @return Charge
     */
    public function setTime(int $time): Charge
    {
        $this->time = $time;
        return $this;
    }
}
