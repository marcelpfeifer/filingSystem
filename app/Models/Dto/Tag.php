<?php
/**
 * Tag
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 16.07.2021
 * Time: 15:17
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;


use App\Libary\Logging\ILogging;
use App\Libary\Logging\Traits\HasLogging;

class Tag extends ADto implements ILogging
{
    use HasLogging;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var boolean
     */
    private $incident = false;

    /**
     * @var boolean
     */
    private $profile = false;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Tag
     */
    public function setName(string $name): Tag
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIncident(): bool
    {
        return $this->incident;
    }

    /**
     * @param bool $incident
     * @return Tag
     */
    public function setIncident(bool $incident): Tag
    {
        $this->incident = $incident;
        return $this;
    }

    /**
     * @return bool
     */
    public function isProfile(): bool
    {
        return $this->profile;
    }

    /**
     * @param bool $profile
     * @return Tag
     */
    public function setProfile(bool $profile): Tag
    {
        $this->profile = $profile;
        return $this;
    }
}
