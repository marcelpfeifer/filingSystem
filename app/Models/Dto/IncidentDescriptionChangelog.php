<?php
/**
 * IncidentDescriptionChangelog
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.05.2021
 * Time: 14:53
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;

class IncidentDescriptionChangelog extends ADto
{

    /**
     * @var int
     */
    private $incidentDescriptionId = 0;

    /**
     * @var int
     */
    private $userId = 0;

    /**
     * @var string
     */
    private $action = '';

    /**
     * @return int
     */
    public function getIncidentDescriptionId(): int
    {
        return $this->incidentDescriptionId;
    }

    /**
     * @param int $incidentDescriptionId
     * @return IncidentDescriptionChangelog
     */
    public function setIncidentDescriptionId(int $incidentDescriptionId): IncidentDescriptionChangelog
    {
        $this->incidentDescriptionId = $incidentDescriptionId;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return IncidentDescriptionChangelog
     */
    public function setUserId(int $userId): IncidentDescriptionChangelog
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return IncidentDescriptionChangelog
     */
    public function setAction(string $action): IncidentDescriptionChangelog
    {
        $this->action = $action;
        return $this;
    }
}
