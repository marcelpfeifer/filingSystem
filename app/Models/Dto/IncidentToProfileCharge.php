<?php
/**
 * IncidentToProfileCharge
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 12:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;

class IncidentToProfileCharge extends ADto
{

    /**
     * @var int
     */
    private $incidentToProfileId = 0;

    /**
     * @var int
     */
    private $chargeId = 0;

    /**
     * @return int
     */
    public function getIncidentToProfileId(): int
    {
        return $this->incidentToProfileId;
    }

    /**
     * @param int $incidentToProfileId
     * @return IncidentToProfileCharge
     */
    public function setIncidentToProfileId(int $incidentToProfileId): IncidentToProfileCharge
    {
        $this->incidentToProfileId = $incidentToProfileId;
        return $this;
    }

    /**
     * @return int
     */
    public function getChargeId(): int
    {
        return $this->chargeId;
    }

    /**
     * @param int $chargeId
     * @return IncidentToProfileCharge
     */
    public function setChargeId(int $chargeId): IncidentToProfileCharge
    {
        $this->chargeId = $chargeId;
        return $this;
    }
}
