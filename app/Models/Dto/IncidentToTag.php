<?php
/**
 * IncidentToTag
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 16.07.2021
 * Time: 15:28
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models\Dto;


use App\Libary\Logging\ILogging;
use App\Libary\Logging\Traits\HasLogging;

class IncidentToTag extends ADto implements ILogging
{

    use HasLogging;

    /**
     * @var int
     */
    private $incidentId = 0;

    /**
     * @var int
     */
    private $tagId = 0;

    /**
     * @return int
     */
    public function getIncidentId(): int
    {
        return $this->incidentId;
    }

    /**
     * @param int $incidentId
     * @return IncidentToTag
     */
    public function setIncidentId(int $incidentId): IncidentToTag
    {
        $this->incidentId = $incidentId;
        return $this;
    }

    /**
     * @return int
     */
    public function getTagId(): int
    {
        return $this->tagId;
    }

    /**
     * @param int $tagId
     * @return IncidentToTag
     */
    public function setTagId(int $tagId): IncidentToTag
    {
        $this->tagId = $tagId;
        return $this;
    }
}
