<?php
/**
 * Group
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 12:12
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_SHOW_ALL = 'showAll';

    /**
     * @var string
     */
    protected $table = 'group';

    /**
     * @var array
     */
    protected $dates = [
        Group::CREATED_AT,
        Group::UPDATED_AT,
    ];

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\Group
         */
        return $this->insertGetId(
            [
                Group::COLUMN_NAME     => $dto->getName(),
                Group::COLUMN_SHOW_ALL => $dto->isShowAll(),
                Group::CREATED_AT      => $dto->getCreatedAt(),
                Group::UPDATED_AT      => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\Group
         */
        return $this->where(Group::COLUMN_ID, $dto->getId())->update(
            [
                Group::COLUMN_NAME => $dto->getName(),
                Group::COLUMN_SHOW_ALL => $dto->isShowAll(),
                Group::UPDATED_AT  => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\Group
         */
        $this->where(Group::COLUMN_ID, $dto->getId())->delete();
    }


    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = Group::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(Group $entry): ADto
    {
        return (new Dto\Group())
            ->setId($entry->{Group::COLUMN_ID})
            ->setName($entry->{Group::COLUMN_NAME})
            ->setShowAll($entry->{Group::COLUMN_SHOW_ALL})
            ->setCreatedAt($entry->{Group::CREATED_AT})
            ->setUpdatedAt($entry->{Group::UPDATED_AT});
    }
}
