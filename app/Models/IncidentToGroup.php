<?php
/**
 * IncidentToGroup
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 12:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class IncidentToGroup extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_INCIDENT_ID = 'incidentId';

    /**
     * @var string
     */
    const COLUMN_GROUP_ID = 'groupId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'incidentToGroup';

    /**
     * @return HasOne
     */
    public function group(): HasOne
    {
        return $this->hasOne(Group::class, Group::COLUMN_ID, IncidentToGroup::COLUMN_GROUP_ID);
    }

    /**
     * @return HasOne
     */
    public function incident(): HasOne
    {
        return $this->hasOne(
            Incident::class,
            Incident::COLUMN_ID,
            IncidentToGroup::COLUMN_INCIDENT_ID
        );
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\IncidentToGroup
         */
        return $this->insert(
            [
                IncidentToGroup::COLUMN_INCIDENT_ID => $dto->getIncidentId(),
                IncidentToGroup::COLUMN_GROUP_ID    => $dto->getGroupId(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntries(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\IncidentToGroup
         */
        $this->where(IncidentToGroup::COLUMN_INCIDENT_ID, $dto->getIncidentId())->delete();
    }

    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = IncidentToGroup::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(IncidentToGroup $entry): ADto
    {
        return (new Dto\IncidentToGroup())
            ->setIncidentId($entry->{IncidentToGroup::COLUMN_INCIDENT_ID})
            ->setGroupId($entry->{IncidentToGroup::COLUMN_GROUP_ID});
    }
}
