<?php
/**
 * ListEntry
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 23.07.2021
 * Time: 15:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ListEntry extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_LIST_ID = 'listId';

    /**
     * @var string
     */
    protected $table = 'listEntry';

    /**
     * @var array
     */
    protected $dates = [
        ListEntry::CREATED_AT,
        ListEntry::UPDATED_AT,
    ];

    /**
     * @return HasOne
     */
    public function list(): HasOne
    {
        return $this->hasOne(ListModel::class, ListModel::COLUMN_ID, ListEntry::COLUMN_LIST_ID);
    }

    /**
     * @return HasMany
     */
    public function values(): HasMany
    {
        return $this->hasMany(ListEntryValue::class, ListEntryValue::COLUMN_LIST_ENTRY_ID, ListEntry::COLUMN_ID);
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\ListEntry
         */
        return $this->insertGetId(
            [
                ListEntry::COLUMN_LIST_ID => $dto->getListId(),
                ListEntry::CREATED_AT     => $dto->getCreatedAt() ?? Carbon::now(),
                ListEntry::UPDATED_AT     => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\ListEntry
         */
        return $this->where(ListEntry::COLUMN_ID, $dto->getId())->update(
            [
                ListEntry::COLUMN_LIST_ID => $dto->getListId(),
                ListEntry::UPDATED_AT     => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\ListEntry
         */
        $this->where(ListEntry::COLUMN_ID, $dto->getId())->delete();
    }


    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = ListEntry::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(ListEntry $entry): ADto
    {
        return (new Dto\ListEntry())
            ->setId($entry->{ListEntry::COLUMN_ID})
            ->setListId($entry->{ListEntry::COLUMN_LIST_ID})
            ->setCreatedAt($entry->{ListEntry::CREATED_AT})
            ->setUpdatedAt($entry->{ListEntry::UPDATED_AT});
    }
}
