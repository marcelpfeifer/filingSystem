<?php
/**
 * PermissionGroup
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.04.2021
 * Time: 12:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;

use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermissionGroup extends Model
{
    use HasFactory;
    
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    protected $table = 'permissionGroup';

    /**
     * @var array
     */
    protected $dates = [
        PermissionGroup::CREATED_AT,
        PermissionGroup::UPDATED_AT,
    ];

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\PermissionGroup
         */
        return $this->insertGetId(
            [
                PermissionGroup::COLUMN_NAME => $dto->getName(),
                PermissionGroup::CREATED_AT  => $dto->getCreatedAt(),
                PermissionGroup::UPDATED_AT  => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\PermissionGroup
         */
        return $this->where(PermissionGroup::COLUMN_ID, $dto->getId())->update(
            [
                PermissionGroup::COLUMN_NAME => $dto->getName(),
                PermissionGroup::UPDATED_AT  => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\PermissionGroup
         */
        $this->where(PermissionGroup::COLUMN_ID, $dto->getId())->delete();
    }


    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = PermissionGroup::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(PermissionGroup $entry): ADto
    {
        return (new Dto\PermissionGroup())
            ->setId($entry->{PermissionGroup::COLUMN_ID})
            ->setName($entry->{PermissionGroup::COLUMN_NAME})
            ->setCreatedAt($entry->{PermissionGroup::CREATED_AT})
            ->setUpdatedAt($entry->{PermissionGroup::UPDATED_AT});
    }
}
