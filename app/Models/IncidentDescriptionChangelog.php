<?php
/**
 * IncidentDescriptionChangelog
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.05.2021
 * Time: 14:55
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class IncidentDescriptionChangelog extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_INCIDENT_DESCRIPTION_ID = 'incidentDescriptionId';

    /**
     * @var string
     */
    const COLUMN_USER_ID = 'userId';

    /**
     * @var string
     */
    const COLUMN_ACTION = 'action';

    /**
     * @var array
     */
    protected $dates = [
        IncidentDescription::CREATED_AT,
        IncidentDescription::UPDATED_AT,
    ];

    /**
     * @var string
     */
    protected $table = 'incidentDescriptionChangelog';

    /**
     * @return HasOne
     */
    public function incidentDescription(): HasOne
    {
        return $this->hasOne(
            IncidentDescription::class,
            IncidentDescription::COLUMN_ID,
            IncidentDescriptionChangelog::COLUMN_INCIDENT_DESCRIPTION_ID
        );
    }

    /**
     * @return HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, User::COLUMN_ID, IncidentDescriptionChangelog::COLUMN_USER_ID);
    }


    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\IncidentDescriptionChangelog
         */
        return $this->insertGetId(
            [
                IncidentDescriptionChangelog::COLUMN_INCIDENT_DESCRIPTION_ID => $dto->getIncidentDescriptionId(),
                IncidentDescriptionChangelog::COLUMN_USER_ID                 => $dto->getUserId(),
                IncidentDescriptionChangelog::COLUMN_ACTION                  => $dto->getAction(),
                IncidentDescriptionChangelog::CREATED_AT                     => $dto->getCreatedAt(),
                IncidentDescriptionChangelog::UPDATED_AT                     => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\IncidentDescriptionChangelog
         */
        return $this->where(IncidentDescriptionChangelog::COLUMN_ID, $dto->getId())->update(
            [
                IncidentDescriptionChangelog::COLUMN_INCIDENT_DESCRIPTION_ID => $dto->getIncidentDescriptionId(),
                IncidentDescriptionChangelog::COLUMN_USER_ID                 => $dto->getUserId(),
                IncidentDescriptionChangelog::COLUMN_ACTION                  => $dto->getAction(),
                IncidentDescriptionChangelog::UPDATED_AT                     => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\IncidentDescriptionChangelog
         */
        $this->where(IncidentDescriptionChangelog::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = IncidentDescriptionChangelog::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(IncidentDescriptionChangelog $entry): ADto
    {
        return (new Dto\IncidentDescriptionChangelog())
            ->setId($entry->{IncidentDescriptionChangelog::COLUMN_ID})
            ->setIncidentDescriptionId($entry->{IncidentDescriptionChangelog::COLUMN_INCIDENT_DESCRIPTION_ID})
            ->setUserId($entry->{IncidentDescriptionChangelog::COLUMN_USER_ID})
            ->setAction($entry->{IncidentDescriptionChangelog::COLUMN_ACTION})
            ->setCreatedAt($entry->{IncidentDescriptionChangelog::CREATED_AT})
            ->setUpdatedAt($entry->{IncidentDescriptionChangelog::UPDATED_AT});
    }
}
