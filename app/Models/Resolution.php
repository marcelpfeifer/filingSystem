<?php
/**
 * Resolution
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.06.2021
 * Time: 14:36
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Resolution extends Model
{

    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_TITLE = 'title';

    /**
     * @var string
     */
    const COLUMN_PATH = 'path';

    /**
     * @var array
     */
    protected $dates = [
        Resolution::CREATED_AT,
        Resolution::UPDATED_AT,
    ];

    /**
     * @var string
     */
    protected $table = 'resolution';

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\Resolution
         */
        return $this->insertGetId(
            [
                Resolution::COLUMN_TITLE => $dto->getTitle(),
                Resolution::COLUMN_PATH  => $dto->getPath(),
                Resolution::CREATED_AT   => $dto->getCreatedAt(),
                Resolution::UPDATED_AT   => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\Resolution
         */
        return $this->where(Resolution::COLUMN_ID, $dto->getId())->update(
            [
                Resolution::COLUMN_TITLE => $dto->getTitle(),
                Resolution::COLUMN_PATH  => $dto->getPath(),
                Resolution::UPDATED_AT   => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\Resolution
         */
        $this->where(Resolution::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = Resolution::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(Resolution $entry): ADto
    {
        return (new Dto\Resolution())
            ->setId($entry->{Resolution::COLUMN_ID})
            ->setTitle($entry->{Resolution::COLUMN_TITLE})
            ->setPath($entry->{Resolution::COLUMN_PATH})
            ->setCreatedAt($entry->{Resolution::CREATED_AT})
            ->setUpdatedAt($entry->{Resolution::UPDATED_AT});
    }
}
