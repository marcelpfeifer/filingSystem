<?php
/**
 * ListToGroup
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.09.2021
 * Time: 14:47
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ListToGroup extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_LIST_ID = 'listId';

    /**
     * @var string
     */
    const COLUMN_GROUP_ID = 'groupId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'listToGroup';

    /**
     * @return HasOne
     */
    public function group(): HasOne
    {
        return $this->hasOne(Group::class, Group::COLUMN_ID, ListToGroup::COLUMN_GROUP_ID);
    }

    /**
     * @return HasOne
     */
    public function list(): HasOne
    {
        return $this->hasOne(
            ListModel::class,
            ListModel::COLUMN_ID,
            ListToGroup::COLUMN_LIST_ID
        );
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\ListToGroup
         */
        return $this->insert(
            [
                ListToGroup::COLUMN_LIST_ID  => $dto->getListId(),
                ListToGroup::COLUMN_GROUP_ID => $dto->getGroupId(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntries(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\ListToGroup
         */
        $this->where(ListToGroup::COLUMN_LIST_ID, $dto->getListId())->delete();
    }

    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = ListToGroup::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(ListToGroup $entry): ADto
    {
        return (new Dto\ListToGroup())
            ->setListId($entry->{ListToGroup::COLUMN_LIST_ID})
            ->setGroupId($entry->{ListToGroup::COLUMN_GROUP_ID});
    }
}
