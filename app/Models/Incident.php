<?php
/**
 * Incident
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 12:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Incident extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_TITLE = 'title';

    /**
     * @var array
     */
    protected $dates = [
        Incident::CREATED_AT,
        Incident::UPDATED_AT,
    ];

    /**
     * @var string
     */
    protected $table = 'incident';

    /**
     * @return HasMany
     */
    public function profilesToIncident(): HasMany
    {
        return $this->hasMany(IncidentToProfile::class, IncidentToProfile::COLUMN_INCIDENT_ID, Incident::COLUMN_ID);
    }

    /**
     * @return HasMany
     */
    public function tags(): HasMany
    {
        return $this->hasMany(IncidentToTag::class, IncidentToTag::COLUMN_INCIDENT_ID, Incident::COLUMN_ID);
    }

    /**
     * @return HasMany
     */
    public function groupsToIncident(): HasMany
    {
        return $this->hasMany(IncidentToGroup::class, IncidentToGroup::COLUMN_INCIDENT_ID, Incident::COLUMN_ID);
    }

    /**
     * @return HasMany
     */
    public function descriptions(): HasMany
    {
        return $this->hasMany(IncidentDescription::class, IncidentDescription::COLUMN_INCIDENT_ID, Incident::COLUMN_ID);
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\Incident
         */
        return $this->insertGetId(
            [
                Incident::COLUMN_TITLE => $dto->getTitle(),
                Incident::CREATED_AT   => $dto->getCreatedAt(),
                Incident::UPDATED_AT   => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\Incident
         */
        return $this->where(Incident::COLUMN_ID, $dto->getId())->update(
            [
                Incident::COLUMN_TITLE => $dto->getTitle(),
                Incident::UPDATED_AT   => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\Incident
         */
        $this->where(Incident::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param int $profileId
     * @param int $limit
     * @return ADto[]
     */
    public function getLatestEntriesByProfileId(int $profileId, int $limit = 5)
    {
        $entries = $this->with('profilesToIncident')
            ->whereHas(
                'profilesToIncident',
                function ($query) use ($profileId) {
                    return $query->where(IncidentToProfile::COLUMN_PROFILE_ID, '=', $profileId);
                }
            )->orderBy(self::CREATED_AT, 'DESC')
            ->limit(5)
            ->get();
        return self::getEntriesAsDto($entries);
    }

    /**
     * @param string $search
     * @param array $tags
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function search(string $search = '', array $tags = []): \Illuminate\Database\Eloquent\Collection
    {
        $query = $this
            ->with('tags')
            ->when(
                $search,
                function ($query, $search) {
                    return $query->where(Incident::COLUMN_TITLE, 'LIKE', "%$search%");
                }
            );

        if (count($tags) !== 0) {
            $query = $query->whereHas(
                'tags',
                function ($query) use ($tags) {
                    return $query->whereIn(IncidentToTag::COLUMN_TAG_ID, $tags);
                }
            );
        }

        $groupId = Auth::user()->{User::COLUMN_GROUP_ID};
        if ($groupId && !Auth::user()->group->{Group::COLUMN_SHOW_ALL}) {
            $query = $query->with('groupsToIncident')
                ->whereHas(
                    'groupsToIncident',
                    function ($query) use ($groupId) {
                        return $query->where(User::COLUMN_GROUP_ID, '=', $groupId);
                    }
                );
        }

        return $query
            ->orderBy(self::CREATED_AT, 'DESC')
            ->limit(20)
            ->get();
    }

    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = Incident::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(Incident $entry): ADto
    {
        return (new Dto\Incident())
            ->setId($entry->{Incident::COLUMN_ID})
            ->setTitle($entry->{Incident::COLUMN_TITLE})
            ->setCreatedAt($entry->{Incident::CREATED_AT})
            ->setUpdatedAt($entry->{Incident::UPDATED_AT});
    }
}
