<?php
/**
 * ShareIncident
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 15:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;

use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Model;

class ShareIncident extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_INCIDENT_ID = 'incidentId';

    /**
     * @var string
     */
    const COLUMN_CODE = 'code';

    /**
     * @var string
     */
    protected $table = 'shareIncident';

    /**
     * @var array
     */
    protected $dates = [
        ShareIncident::CREATED_AT,
        ShareIncident::UPDATED_AT,
    ];

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\ShareIncident
         */
        return $this->insertGetId(
            [
                ShareIncident::COLUMN_CODE        => $dto->getCode(),
                ShareIncident::COLUMN_INCIDENT_ID => $dto->getIncidentId(),
                ShareIncident::CREATED_AT         => $dto->getCreatedAt(),
                ShareIncident::UPDATED_AT         => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\ShareIncident
         */
        return $this->where(ShareIncident::COLUMN_ID, $dto->getId())->update(
            [
                ShareIncident::COLUMN_CODE        => $dto->getCode(),
                ShareIncident::COLUMN_INCIDENT_ID => $dto->getIncidentId(),
                ShareIncident::UPDATED_AT         => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param string $code
     * @return Dto\ShareIncident|null
     */
    public function getEntryByCode(string $code): ?\App\Models\Dto\ShareIncident
    {
        $entry = $this->where(self::COLUMN_CODE, $code)->first();
        return $entry ? ShareIncident::getEntryAsDto($entry) : null;
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\ShareIncident
         */
        $this->where(ShareIncident::COLUMN_ID, $dto->getId())->delete();
    }


    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = ShareIncident::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(ShareIncident $entry): ADto
    {
        return (new Dto\ShareIncident())
            ->setId($entry->{ShareIncident::COLUMN_ID})
            ->setIncidentId($entry->{ShareIncident::COLUMN_INCIDENT_ID})
            ->setCode($entry->{ShareIncident::COLUMN_CODE})
            ->setCreatedAt($entry->{ShareIncident::CREATED_AT})
            ->setUpdatedAt($entry->{ShareIncident::UPDATED_AT});
    }
}
