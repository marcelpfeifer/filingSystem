<?php
/**
 * ShareIncident
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 15:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;

use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Model;

class ShareProfile extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_PROFILE_ID = 'profileId';

    /**
     * @var string
     */
    const COLUMN_CODE = 'code';

    /**
     * @var string
     */
    protected $table = 'shareProfile';

    /**
     * @var array
     */
    protected $dates = [
        ShareProfile::CREATED_AT,
        ShareProfile::UPDATED_AT,
    ];

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\ShareProfile
         */
        return $this->insertGetId(
            [
                ShareProfile::COLUMN_CODE       => $dto->getCode(),
                ShareProfile::COLUMN_PROFILE_ID => $dto->getProfileId(),
                ShareProfile::CREATED_AT        => $dto->getCreatedAt(),
                ShareProfile::UPDATED_AT        => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\ShareProfile
         */
        return $this->where(ShareProfile::COLUMN_ID, $dto->getId())->update(
            [
                ShareProfile::COLUMN_CODE       => $dto->getCode(),
                ShareProfile::COLUMN_PROFILE_ID => $dto->getProfileId(),
                ShareProfile::UPDATED_AT        => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param string $code
     * @return Dto\ShareProfile|null
     */
    public function getEntryByCode(string $code): ?\App\Models\Dto\ShareProfile
    {
        $entry = $this->where(self::COLUMN_CODE, $code)->first();
        return $entry ? ShareProfile::getEntryAsDto($entry) : null;
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\ShareProfile
         */
        $this->where(ShareProfile::COLUMN_ID, $dto->getId())->delete();
    }


    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = ShareProfile::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(ShareProfile $entry): ADto
    {
        return (new Dto\ShareProfile())
            ->setId($entry->{ShareProfile::COLUMN_ID})
            ->setProfileId($entry->{ShareProfile::COLUMN_PROFILE_ID})
            ->setCode($entry->{ShareProfile::COLUMN_CODE})
            ->setCreatedAt($entry->{ShareProfile::CREATED_AT})
            ->setUpdatedAt($entry->{ShareProfile::UPDATED_AT});
    }
}
