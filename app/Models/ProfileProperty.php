<?php
/**
 * ProfileProperty
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 22.07.2021
 * Time: 09:50
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ProfileProperty extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_PROFILE_PROPERTY_GROUP_ID = 'profilePropertyGroupId';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    protected $table = 'profileProperty';

    /**
     * @var array
     */
    protected $dates = [
        ProfileProperty::CREATED_AT,
        ProfileProperty::UPDATED_AT,
    ];

    /**
     * @return HasOne
     */
    public function group(): HasOne
    {
        return $this->hasOne(
            ProfilePropertyGroup::class,
            ProfilePropertyGroup::COLUMN_ID,
            ProfileProperty::COLUMN_PROFILE_PROPERTY_GROUP_ID
        );
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\ProfileProperty
         */
        return $this->insertGetId(
            [
                ProfileProperty::COLUMN_PROFILE_PROPERTY_GROUP_ID => $dto->getProfilePropertyGroupId(),
                ProfileProperty::COLUMN_NAME                      => $dto->getName(),
                ProfileProperty::CREATED_AT                       => $dto->getCreatedAt() ?? Carbon::now(),
                ProfileProperty::UPDATED_AT                       => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\ProfileProperty
         */
        return $this->where(ProfileProperty::COLUMN_ID, $dto->getId())->update(
            [
                ProfileProperty::COLUMN_PROFILE_PROPERTY_GROUP_ID => $dto->getProfilePropertyGroupId(),
                ProfileProperty::COLUMN_NAME                      => $dto->getName(),
                ProfileProperty::UPDATED_AT                       => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\ProfileProperty
         */
        $this->where(ProfileProperty::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param string $name
     * @return ADto[]
     */
    public function search(string $name = ''): array
    {
        $entries = $this
            ->when(
                $name,
                function ($query, $name) {
                    return $query->where(self::COLUMN_NAME, 'LIKE', "%$name%");
                }
            )
            ->get();
        return ProfileProperty::getEntriesAsDto($entries);
    }

    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = ProfileProperty::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(ProfileProperty $entry): ADto
    {
        return (new Dto\ProfileProperty())
            ->setId($entry->{ProfileProperty::COLUMN_ID})
            ->setProfilePropertyGroupId($entry->{ProfileProperty::COLUMN_PROFILE_PROPERTY_GROUP_ID})
            ->setName($entry->{ProfileProperty::COLUMN_NAME})
            ->setCreatedAt($entry->{ProfileProperty::CREATED_AT})
            ->setUpdatedAt($entry->{ProfileProperty::UPDATED_AT});
    }
}
