<?php
/**
 * ListEntryValue
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 23.07.2021
 * Time: 15:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ListEntryValue extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_LIST_ENTRY_ID = 'listEntryId';

    /**
     * @var string
     */
    const COLUMN_LIST_HEADER_ID = 'listHeaderId';

    /**
     * @var string
     */
    const COLUMN_VALUE = 'value';

    /**
     * @var string
     */
    protected $table = 'listEntryValue';

    /**
     * @var array
     */
    protected $dates = [
        ListEntryValue::CREATED_AT,
        ListEntryValue::UPDATED_AT,
    ];

    /**
     * @return HasOne
     */
    public function entry(): HasOne
    {
        return $this->hasOne(ListEntry::class, ListEntry::COLUMN_ID, ListEntryValue::COLUMN_LIST_ENTRY_ID);
    }

    /**
     * @return HasOne
     */
    public function header(): HasOne
    {
        return $this->hasOne(ListHeader::class, ListHeader::COLUMN_ID, ListEntryValue::COLUMN_LIST_HEADER_ID);
    }


    /**
     * @param int $listId
     * @param string $search
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function search(int $listId, string $search = ''): \Illuminate\Database\Eloquent\Collection
    {
        return $this
            ->whereHas(
                'header',
                function ($query) {
                    return $query
                        ->where(ListHeader::COLUMN_LIST, true);
                }
            )->whereHas(
                'entry',
                function ($query) use ($listId) {
                    return $query->where(ListEntry::COLUMN_LIST_ID, $listId);
                }
            )->when(
                $search,
                function ($query, $search) {
                    return $query->where(ListEntryValue::COLUMN_VALUE, 'LIKE', '%' . $search . '%');
                }
            )
            ->get();
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\ListEntryValue
         */
        return $this->insertGetId(
            [
                ListEntryValue::COLUMN_LIST_ENTRY_ID  => $dto->getListEntryId(),
                ListEntryValue::COLUMN_LIST_HEADER_ID => $dto->getListHeaderId(),
                ListEntryValue::COLUMN_VALUE          => $dto->getValue(),
                ListEntryValue::CREATED_AT            => $dto->getCreatedAt() ?? Carbon::now(),
                ListEntryValue::UPDATED_AT            => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\ListEntryValue
         */
        return $this->where(ListEntryValue::COLUMN_ID, $dto->getId())->update(
            [
                ListEntryValue::COLUMN_LIST_ENTRY_ID  => $dto->getListEntryId(),
                ListEntryValue::COLUMN_LIST_HEADER_ID => $dto->getListHeaderId(),
                ListEntryValue::COLUMN_VALUE          => $dto->getValue(),
                ListEntryValue::UPDATED_AT            => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\ListEntryValue
         */
        $this->where(ListEntryValue::COLUMN_ID, $dto->getId())->delete();
    }


    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = ListEntryValue::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(ListEntryValue $entry): ADto
    {
        return (new Dto\ListEntryValue())
            ->setId($entry->{ListEntryValue::COLUMN_ID})
            ->setListEntryId($entry->{ListEntryValue::COLUMN_LIST_ENTRY_ID})
            ->setListHeaderId($entry->{ListEntryValue::COLUMN_LIST_HEADER_ID})
            ->setValue($entry->{ListEntryValue::COLUMN_VALUE})
            ->setCreatedAt($entry->{ListEntryValue::CREATED_AT})
            ->setUpdatedAt($entry->{ListEntryValue::UPDATED_AT});
    }
}
