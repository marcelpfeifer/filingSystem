<?php
/**
 * IncidentToTag
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 16.07.2021
 * Time: 15:28
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class IncidentToTag extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_INCIDENT_ID = 'incidentId';

    /**
     * @var string
     */
    const COLUMN_TAG_ID = 'tagId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'incidentToTag';

    /**
     * @return HasOne
     */
    public function tag(): HasOne
    {
        return $this->hasOne(Tag::class, Tag::COLUMN_ID, IncidentToTag::COLUMN_TAG_ID);
    }

    /**
     * @return HasOne
     */
    public function incident(): HasOne
    {
        return $this->hasOne(Incident::class, Incident::COLUMN_ID, IncidentToTag::COLUMN_INCIDENT_ID);
    }


    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\IncidentToTag
         */
        return $this->insertGetId(
            [
                IncidentToTag::COLUMN_INCIDENT_ID => $dto->getIncidentId(),
                IncidentToTag::COLUMN_TAG_ID      => $dto->getTagId(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\IncidentToTag
         */
        return $this->where(IncidentToTag::COLUMN_ID, $dto->getId())->update(
            [
                IncidentToTag::COLUMN_INCIDENT_ID => $dto->getIncidentId(),
                IncidentToTag::COLUMN_TAG_ID      => $dto->getTagId(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\IncidentToTag
         */
        $this->where(IncidentToTag::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntryByIncidentIdAndTagId(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\IncidentToTag
         */
        $this->where(IncidentToTag::COLUMN_INCIDENT_ID, $dto->getIncidentId())
            ->where(IncidentToTag::COLUMN_TAG_ID, $dto->getTagId())
            ->delete();
    }


    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = IncidentToTag::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(IncidentToTag $entry): ADto
    {
        return (new Dto\IncidentToTag())
            ->setId($entry->{IncidentToTag::COLUMN_ID})
            ->setIncidentId($entry->{IncidentToTag::COLUMN_INCIDENT_ID})
            ->setTagId($entry->{IncidentToTag::COLUMN_TAG_ID});
    }
}
