<?php
/**
 * Permission
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.04.2021
 * Time: 12:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_DESCRIPTION = 'description';

    /**
     * @var string
     */
    const COLUMN_PARENT_PERMISSION = 'parentPermission';

    /**
     * @var string
     */
    protected $table = 'permission';

    /**
     * @var array
     */
    protected $dates = [
        Permission::CREATED_AT,
        Permission::UPDATED_AT,
    ];

    /**
     * @param string $name
     * @return ADto|null
     */
    public function getEntryByName(string $name)
    {
        $entry = $this->where(self::COLUMN_NAME, $name)->first();
        return ($entry) ? Permission::getEntryAsDto($entry) : null;
    }

    /**
     * @param int[] $ids
     * @return string[]
     */
    public function getNamesByIds(array $ids): array
    {
        return $this
            ->whereIn(self::COLUMN_ID, $ids)
            ->pluck(self::COLUMN_NAME)
            ->toArray();
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\Permission
         */
        return $this->insertGetId(
            [
                Permission::COLUMN_NAME              => $dto->getName(),
                Permission::COLUMN_DESCRIPTION       => $dto->getDescription(),
                Permission::COLUMN_PARENT_PERMISSION => $dto->getParentPermission(),
                Permission::CREATED_AT               => $dto->getCreatedAt(),
                Permission::UPDATED_AT               => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\Permission
         */
        return $this->where(Permission::COLUMN_ID, $dto->getId())->update(
            [
                Permission::COLUMN_NAME              => $dto->getName(),
                Permission::COLUMN_DESCRIPTION       => $dto->getDescription(),
                Permission::COLUMN_PARENT_PERMISSION => $dto->getParentPermission(),
                Permission::UPDATED_AT               => $dto->getUpdatedAt(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\Permission
         */
        $this->where(Permission::COLUMN_ID, $dto->getId())->delete();
    }


    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $dto = Permission::getEntryAsDto($entry);
            $result[$dto->getId()] = $dto;
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(Permission $entry): ADto
    {
        return (new Dto\Permission())
            ->setId($entry->{Permission::COLUMN_ID})
            ->setName($entry->{Permission::COLUMN_NAME})
            ->setDescription($entry->{Permission::COLUMN_DESCRIPTION})
            ->setParentPermission($entry->{Permission::COLUMN_PARENT_PERMISSION})
            ->setCreatedAt($entry->{Permission::CREATED_AT})
            ->setUpdatedAt($entry->{Permission::UPDATED_AT});
    }
}
