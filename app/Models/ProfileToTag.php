<?php
/**
 * IncidentToTag
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 16.07.2021
 * Time: 15:28
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ProfileToTag extends Model
{
    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_PROFILE_ID = 'profileId';

    /**
     * @var string
     */
    const COLUMN_TAG_ID = 'tagId';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'profileToTag';

    /**
     * @return HasOne
     */
    public function tag(): HasOne
    {
        return $this->hasOne(Tag::class, Tag::COLUMN_ID, ProfileToTag::COLUMN_TAG_ID);
    }

    /**
     * @return HasOne
     */
    public function profile(): HasOne
    {
        return $this->hasOne(Profile::class, Profile::COLUMN_ID, ProfileToTag::COLUMN_PROFILE_ID);
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\ProfileToTag
         */
        return $this->insertGetId(
            [
                ProfileToTag::COLUMN_PROFILE_ID => $dto->getProfileId(),
                ProfileToTag::COLUMN_TAG_ID     => $dto->getTagId(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\ProfileToTag
         */
        return $this->where(ProfileToTag::COLUMN_ID, $dto->getId())->update(
            [
                ProfileToTag::COLUMN_PROFILE_ID => $dto->getProfileId(),
                ProfileToTag::COLUMN_TAG_ID     => $dto->getTagId(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\ProfileToTag
         */
        $this->where(ProfileToTag::COLUMN_ID, $dto->getId())->delete();
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntryByProfileIdAndTagId(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\ProfileToTag
         */
        $this->where(ProfileToTag::COLUMN_PROFILE_ID, $dto->getProfileId())
            ->where(ProfileToTag::COLUMN_TAG_ID, $dto->getTagId())
            ->delete();
    }

    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = ProfileToTag::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(ProfileToTag $entry): ADto
    {
        return (new Dto\ProfileToTag())
            ->setId($entry->{ProfileToTag::COLUMN_ID})
            ->setProfileId($entry->{ProfileToTag::COLUMN_PROFILE_ID})
            ->setTagId($entry->{ProfileToTag::COLUMN_TAG_ID});
    }
}
