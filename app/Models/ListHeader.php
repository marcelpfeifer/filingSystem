<?php
/**
 * ListHeader
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 23.07.2021
 * Time: 15:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Models;


use App\Models\Dto\ADto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class ListHeader extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    const COLUMN_ID = 'id';

    /**
     * @var string
     */
    const COLUMN_LIST_ID = 'listId';

    /**
     * @var string
     */
    const COLUMN_NAME = 'name';

    /**
     * @var string
     */
    const COLUMN_LIST = 'list';

    /**
     * @var string
     */
    const COLUMN_ORDER = 'order';

    /**
     * @var string
     */
    protected $table = 'listHeader';

    /**
     * @var array
     */
    protected $dates = [
        ListHeader::CREATED_AT,
        ListHeader::UPDATED_AT,
    ];

    /**
     * @return HasOne
     */
    public function list(): HasOne
    {
        return $this->hasOne(ListModel::class, ListModel::COLUMN_ID, ListHeader::COLUMN_LIST_ID);
    }

    /**
     * @param int $listId
     * @return ADto[]
     */
    public function getEntriesByListId(int $listId, bool $listOnly = false): array
    {
        $entries = $this
            ->where(self::COLUMN_LIST_ID, $listId)
            ->when(
                $listOnly,
                function ($query, $listOnly) {
                    return $query->where(self::COLUMN_LIST, $listOnly);
                }
            )
            ->orderBy(self::COLUMN_ORDER)
            ->get();

        return self::getEntriesAsDto($entries);
    }

    /**
     * @param ADto $dto
     * @return int
     */
    public function insertEntry(ADto $dto): int
    {
        /**
         * @var $dto \App\Models\Dto\ListHeader
         */
        return $this->insertGetId(
            [
                ListHeader::COLUMN_LIST_ID => $dto->getListId(),
                ListHeader::COLUMN_NAME    => $dto->getName(),
                ListHeader::COLUMN_LIST    => $dto->isList(),
                ListHeader::COLUMN_ORDER   => $dto->getOrder(),
                ListHeader::CREATED_AT     => $dto->getCreatedAt() ?? Carbon::now(),
                ListHeader::UPDATED_AT     => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     * @return bool
     */
    public function updateEntry(ADto $dto): bool
    {
        /**
         * @var $dto \App\Models\Dto\ListHeader
         */
        return $this->where(ListHeader::COLUMN_ID, $dto->getId())->update(
            [
                ListHeader::COLUMN_LIST_ID => $dto->getListId(),
                ListHeader::COLUMN_NAME    => $dto->getName(),
                ListHeader::COLUMN_LIST    => $dto->isList(),
                ListHeader::COLUMN_ORDER   => $dto->getOrder(),
                ListHeader::UPDATED_AT     => $dto->getUpdatedAt() ?? Carbon::now(),
            ]
        );
    }

    /**
     * @param ADto $dto
     */
    public function deleteEntry(ADto $dto)
    {
        /**
         * @var $dto \App\Models\Dto\ListHeader
         */
        $this->where(ListHeader::COLUMN_ID, $dto->getId())->delete();
    }


    /**
     * @param object $entries
     * @return Dto\ADto[]
     */
    public static function getEntriesAsDto(object $entries): array
    {
        $result = [];
        foreach ($entries as $entry) {
            $result[] = ListHeader::getEntryAsDto($entry);
        }

        return $result;
    }

    /**
     * @param self $entry
     * @return Dto\ADto
     */
    public static function getEntryAsDto(ListHeader $entry): ADto
    {
        return (new Dto\ListHeader())
            ->setId($entry->{ListHeader::COLUMN_ID})
            ->setListId($entry->{ListHeader::COLUMN_LIST_ID})
            ->setName($entry->{ListHeader::COLUMN_NAME})
            ->setList($entry->{ListHeader::COLUMN_LIST})
            ->setOrder($entry->{ListHeader::COLUMN_ORDER})
            ->setCreatedAt($entry->{ListHeader::CREATED_AT})
            ->setUpdatedAt($entry->{ListHeader::UPDATED_AT});
    }
}
