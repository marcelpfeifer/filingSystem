<?php
/**
 * CreateController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 15:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Incident;

use App\Http\Controllers\Controller;
use App\Http\Request\Incident\Create\Save;
use App\Http\Response\Error;
use App\Http\Response\Incident\Create\Index;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Inertia\Response;

class CreateController extends Controller
{

    /**
     * @return Response
     */
    public function index(): Response
    {
        return (new Index([]))->inertia('Incident/Entry');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new Save($request));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Incident\Create\Save
         */
        $dto = $validatorClass->toDto();

        // Save the Data
        $responseDto = \App\Libary\View\Incident\Create\Save::save($dto);

        return (new \App\Http\Response\Incident\Create\Save($responseDto))->json();
    }
}
