<?php
/**
 * ShowController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 13:04
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Incident;

use App\Http\Controllers\Controller;
use App\Http\Response\AResponse;
use App\Http\Response\Incident\Show\Index;
use App\Http\Response\NotFound;
use Illuminate\Http\Request;
use Inertia\Response;

class ShowController extends Controller
{
    /**
     * @return Response
     */
    public function index(Request $request, int $id): Response
    {
        $viewDto = \App\Libary\View\Incident\Show\Index::getViewDto($id);
        if (!($viewDto)) {
            return (new NotFound([]))->inertia('404');
        }

        return (new Index($viewDto))->inertia('Incident/Show');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function share(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        $responseDto = \App\Libary\View\Incident\Show\Share::share($id);
        return (new \App\Http\Response\Incident\Show\Share($responseDto))->json();
    }
}
