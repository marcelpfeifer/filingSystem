<?php
/**
 * EditController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 15:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Incident;


use App\Http\Controllers\Controller;
use App\Http\Request\AValidator;
use App\Http\Request\Incident\Edit\AddTag;
use App\Http\Request\Incident\Edit\AddUser;
use App\Http\Request\Incident\Edit\DeleteTag;
use App\Http\Request\Incident\Edit\DeleteUser;
use App\Http\Request\Incident\Edit\Save;
use App\Http\Request\Incident\Edit\Warrant;
use App\Http\Response\AResponse;
use App\Http\Response\Error;
use App\Http\Response\Incident\Edit\Index;
use App\Http\Response\NotFound;
use App\Http\Response\Success;
use App\Models\Incident;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Inertia\Response;

class EditController extends Controller
{
    /**
     * @return Response
     */
    public function index(Request $request, int $id): Response
    {
        $viewDto = \App\Libary\View\Incident\Edit\Index::getViewDto($id);
        if (!($viewDto)) {
            return (new NotFound([]))->inertia('404');
        }

        return (new Index($viewDto))->inertia('Incident/Entry');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new Save($request, $id));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Incident\Edit\Save
         */
        $dto = $validatorClass->toDto();

        // Save the Data
        $responseDto = \App\Libary\View\Incident\Edit\Save::save($dto);

        return (new \App\Http\Response\Incident\Edit\Save($responseDto))->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addUser(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new AddUser($request, $id));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Incident\Edit\AddUser
         */
        $dto = $validatorClass->toDto();

        \App\Libary\View\Incident\Edit\AddUser::save($dto);

        return (new \App\Http\Response\Incident\Edit\AddUser([]))->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteUser(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new DeleteUser($request, $id));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Incident\Edit\DeleteUser
         */
        $dto = $validatorClass->toDto();

        \App\Libary\View\Incident\Edit\DeleteUser::delete($dto);

        return (new \App\Http\Response\Incident\Edit\DeleteUser([]))->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addTag(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new AddTag($request, $id));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Incident\Edit\AddTag
         */
        $dto = $validatorClass->toDto();

        \App\Libary\View\Incident\Edit\AddTag::save($dto);

        return (new \App\Http\Response\Incident\Edit\AddTag([]))->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTag(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new DeleteTag($request, $id));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Incident\Edit\DeleteTag
         */
        $dto = $validatorClass->toDto();

        \App\Libary\View\Incident\Edit\DeleteTag::delete($dto);

        return (new \App\Http\Response\Incident\Edit\DeleteTag([]))->json();
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function warrant(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new Warrant($request, $id));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Incident\Edit\Warrant
         */
        $dto = $validatorClass->toDto();

        \App\Libary\View\Incident\Edit\Warrant::update($dto);

        return (new \App\Http\Response\Incident\Edit\Warrant([]))->json();
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        // Lösche den Eintrag
        $dto = (new \App\Models\Dto\Incident())
            ->setId($id);
        (new Incident())->deleteEntry($dto);

        return (new Success(
            [
                'Erfolgreich gelöscht!'
            ]
        ))->json();
    }
}
