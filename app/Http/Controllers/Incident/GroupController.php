<?php
/**
 * GroupController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 16:14
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Incident;


use App\Http\Controllers\Controller;
use App\Http\Request\AValidator;
use App\Http\Request\Incident\Group\Save;
use App\Http\Response\AResponse;
use App\Http\Response\Error;
use App\Http\Response\Incident\Group\Index;
use Illuminate\Http\Request;

class GroupController extends Controller
{

    /**
     * @param Request $request
     * @param int $incidentId
     * @return \Inertia\Response
     */
    public function index(Request $request, int $incidentId): \Inertia\Response
    {
        $viewDto = \App\Libary\View\Incident\Group\Index::getViewDto($incidentId);
        return (new Index($viewDto))->inertia('Incident/Group');
    }

    /**
     * @param Request $request
     * @param int $incidentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request, int $incidentId): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new Save($request, $incidentId));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Incident\Group\Save
         */
        $dto = $validatorClass->toDto();

        \App\Libary\View\Incident\Group\Save::save($dto);

        return (new \App\Http\Response\Incident\Group\Save([]))->json();
    }
}
