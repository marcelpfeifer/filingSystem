<?php
/**
 * ChargeController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 17.05.2021
 * Time: 14:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Incident;


use App\Http\Controllers\Controller;
use App\Http\Request\Incident\Charge\Save;
use App\Http\Response\Error;
use App\Http\Response\Incident\Charge\Index;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ChargeController extends Controller
{

    /**
     * @param Request $request
     * @param int $incidentId
     * @param int $profileId
     */
    public function index(Request $request, int $incidentId, int $profileId)
    {
        $viewDto = \App\Libary\View\Incident\Charge\Index::getViewDto($incidentId, $profileId);
        return (new Index($viewDto))->inertia('Incident/Charge');
    }

    /**
     * @param Request $request
     */
    public function save(Request $request)
    {
        $validatorClass = (new Save($request));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Incident\Charge\Save
         */
        $dto = $validatorClass->toDto();

        \App\Libary\View\Incident\Charge\Save::save($dto);

        return (new \App\Http\Response\Incident\Charge\Save([]))->json();
    }
}
