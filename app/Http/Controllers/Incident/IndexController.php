<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 15:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Incident;

use App\Http\Controllers\Controller;
use App\Http\Response\Error;
use App\Http\Response\Incident\Index\Index;
use App\Http\Response\Incident\Index\Search;
use App\Models\Incident;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Inertia\Response;

class IndexController extends Controller
{
    /**
     * @return Response
     */
    public function index(): Response
    {
        $viewDto = \App\Libary\View\Incident\Index\Index::getViewDto();
        return (new Index($viewDto))->inertia('Incident/Index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new \App\Http\Request\Incident\Index\Search($request));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Incident\Index\Search
         */
        $dto = $validatorClass->toDto();
        $tags = array_map(
            function ($entry) {
                return $entry->getId();
            },
            $dto->getTags()
        );
        $incidents = (new Incident())->search($dto->getSearch(), $tags);

        return (new Search($incidents))->json();
    }

}
