<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.07.2021
 * Time: 12:59
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\ListDir;

use App\Http\Controllers\Controller;
use App\Http\Response\ListDir\Index\Index;

class IndexController extends Controller
{

    /**
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        $viewDto = \App\Libary\View\ListDir\Index\Index::getViewDto();
        return (new Index($viewDto))->inertia('List/Index');
    }
}
