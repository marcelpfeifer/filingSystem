<?php
/**
 * CreateController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.07.2021
 * Time: 13:06
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\ListDir\Entry;


use App\Http\Controllers\Controller;
use App\Http\Response\AResponse;
use App\Http\Response\Dto\Message;
use App\Http\Response\Enum\Message\Status;
use App\Http\Response\Error;
use App\Http\Response\ListDir\Entry\Create\Index;
use App\Http\Response\ListDir\Entry\Create\Save;
use App\Http\Response\NotFound;
use App\Libary\ListDir\Permission;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CreateController extends Controller
{
    /**
     * @param Request $request
     * @param int $listId
     * @return \Inertia\Response
     */
    public function index(Request $request, int $listId): \Inertia\Response
    {
        if (!(Permission::hasPermission($listId))) {
            return (new NotFound())->inertia('404');
        }
        $viewDto = \App\Libary\View\ListDir\Entry\Create\Index::getViewDto($listId);
        return (new Index($viewDto))->inertia('List/Entry/Create');
    }

    /**
     * @param Request $request
     * @param int $listId
     * @return JsonResponse
     */
    public function save(Request $request, int $listId): JsonResponse
    {
        if (!(Permission::hasPermission($listId))) {
            return (new Error())->json();
        }

        $validatorClass = (new \App\Http\Request\ListDir\Entry\Create\Save($request, $listId));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\ListDir\Entry\Create\Save
         */
        $dto = $validatorClass->toDto();

        \App\Libary\View\ListDir\Entry\Create\Save::save($dto);

        return (new Save())
            ->setMessages(
                [
                    (new Message())
                        ->setText('Erfolgreich gespeichert!')
                        ->setStatus(Status::SUCCESS)
                ]
            )
            ->json();
    }
}
