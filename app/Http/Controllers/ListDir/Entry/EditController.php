<?php
/**
 * EditController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.07.2021
 * Time: 13:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\ListDir\Entry;


use App\Http\Controllers\Controller;
use App\Http\Response\AResponse;
use App\Http\Response\Dto\Message;
use App\Http\Response\Enum\Message\Status;
use App\Http\Response\Error;
use App\Http\Response\ListDir\Entry\Edit\Index;
use App\Http\Response\ListDir\Entry\Edit\Save;
use App\Http\Response\NotFound;
use App\Http\Response\Success;
use App\Libary\ListDir\Exception\NoPermission;
use App\Libary\ListDir\Permission;
use App\Models\ListEntry;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EditController extends Controller
{
    /**
     * @param Request $request
     * @param int $entryId
     * @return \Inertia\Response
     */
    public function index(Request $request, int $entryId): \Inertia\Response
    {
        $viewDto = \App\Libary\View\ListDir\Entry\Edit\Index::getViewDto($entryId);
        if (!(Permission::hasPermission($viewDto->getListId()))) {
            return (new NotFound())->inertia('404');
        }
        return (new Index($viewDto))->inertia('List/Entry/Edit');
    }

    /**
     * @param Request $request
     * @param int $entryId
     * @return JsonResponse
     */
    public function save(Request $request, int $entryId): JsonResponse
    {
        $validatorClass = (new \App\Http\Request\ListDir\Entry\Edit\Save($request, $entryId));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\ListDir\Entry\Edit\Save
         */
        $dto = $validatorClass->toDto();

        try {
            \App\Libary\View\ListDir\Entry\Edit\Save::save($dto);
        } catch (NoPermission $exception) {
            return (new Error())->json();
        }

        return (new Save())
            ->setMessages(
                [
                    (new Message())
                        ->setText('Erfolgreich gespeichert!')
                        ->setStatus(Status::SUCCESS)
                ]
            )
            ->json();
    }

    /**
     * @param Request $request
     * @param int $entryId
     * @return JsonResponse
     */
    public function delete(Request $request, int $entryId): JsonResponse
    {
        $listDto = ListEntry::getEntryAsDto(ListEntry::find($entryId));
        if (!(Permission::hasPermission($listDto->getListId()))) {
            return (new Error())->json();
        }

        $dto = (new \App\Models\Dto\ListEntry())
            ->setId($entryId);
        (new ListEntry())->deleteEntry($dto);

        return (new Success(
            [
                'Erfolgreich gelöscht!'
            ]
        ))->json();
    }
}
