<?php
/**
 * ShowController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.07.2021
 * Time: 13:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\ListDir\Entry;


use App\Http\Controllers\Controller;
use App\Http\Response\AResponse;
use App\Http\Response\ListDir\Entry\Show\Index;
use App\Http\Response\NotFound;
use App\Libary\ListDir\Permission;
use Illuminate\Http\Request;

class ShowController extends Controller
{
    /**
     * @param Request $request
     * @param int $entryId
     * @return \Inertia\Response
     */
    public function index(Request $request, int $entryId): \Inertia\Response
    {
        $viewDto = \App\Libary\View\ListDir\Entry\Show\Index::getViewDto($entryId);
        if (!(Permission::hasPermission($viewDto->getListId()))) {
            return (new NotFound())->inertia('404');
        }
        return (new Index($viewDto))->inertia('List/Entry/Show');
    }
}
