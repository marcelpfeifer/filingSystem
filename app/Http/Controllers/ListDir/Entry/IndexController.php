<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.07.2021
 * Time: 13:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\ListDir\Entry;


use App\Http\Controllers\Controller;
use App\Http\Response\AResponse;
use App\Http\Response\Error;
use App\Http\Response\ListDir\Entry\Index\Index;
use App\Http\Response\ListDir\Entry\Index\Search;
use App\Http\Response\NotFound;
use App\Libary\ListDir\Permission;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * @param Request $request
     * @param int $listId
     * @return \Inertia\Response
     */
    public function index(Request $request, int $listId): \Inertia\Response
    {
        if (!(Permission::hasPermission($listId))) {
            return (new NotFound())->inertia('404');
        }
        $viewDto = \App\Libary\View\ListDir\Entry\Index\Index::getViewDto($listId);
        return (new Index($viewDto))->inertia('List/Entry/Index');
    }

    /**
     * @param Request $request
     * @param int $listId
     * @return JsonResponse
     */
    public function search(Request $request, int $listId): JsonResponse
    {
        if (!(Permission::hasPermission($listId))) {
            return (new Error())->json();
        }

        $validatorClass = (new \App\Http\Request\ListDir\Entry\Index\Search($request, $listId));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\ListDir\Entry\Index\Search
         */
        $dto = $validatorClass->toDto();

        $viewDto = \App\Libary\View\ListDir\Entry\Index\Search::getViewDto($dto);

        return (new Search($viewDto))->json();
    }
}
