<?php
/**
 * CreateController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.06.2021
 * Time: 14:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Resolution;

use App\Http\Request\Resolution\Create\Save;
use App\Http\Response\Error;
use App\Http\Response\Resolution\Create\Index;
use App\Http\Response\Success;
use Illuminate\Http\Request;

class CreateController
{
    /**
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        return (new Index())->inertia('Resolution/Create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new Save($request));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Resolution\Create\Save
         */
        $dto = $validatorClass->toDto();

        // Save the Data
        \App\Libary\View\Resolution\Create\Save::save($dto);

        return (new Success(
            [
                'Erfolgreich gespeichert!'
            ]
        ))->json();
    }
}
