<?php
/**
 * ShowController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.06.2021
 * Time: 14:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Resolution;


use App\Http\Response\AResponse;
use App\Http\Response\Resolution\Show\Index;
use App\Http\Response\Success;
use App\Libary\Logging\Logging;
use App\Models\Resolution;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ShowController
{
    /**
     * @return \Inertia\Response
     */
    public function index(Request $request, int $id): \Inertia\Response
    {
        $result = \App\Libary\View\Resolution\Show\Index::getViewDto($id);
        return (new Index($result))->inertia('Resolution/Show');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        // Lösche den Eintrag
        $dto = (new \App\Models\Dto\Resolution())
            ->setId($id);
        (new Resolution())->deleteEntry($dto);

        Logging::delete(\App\Models\Resolution::class, $dto);
        return (new Success(
            [
                'Erfolgreich gelöscht!'
            ]
        ))->json();
    }
}
