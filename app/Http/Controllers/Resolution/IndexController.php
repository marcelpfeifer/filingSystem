<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.06.2021
 * Time: 14:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Resolution;


use App\Http\Response\Resolution\Index\Index;

class IndexController
{

    /**
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        $results = \App\Libary\View\Resolution\Index\Index::getViewDto();
        return (new Index($results))->inertia('Resolution/Index');
    }
}
