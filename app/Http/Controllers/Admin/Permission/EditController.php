<?php
/**
 * EditController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 22.04.2021
 * Time: 16:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\Permission;


use App\Http\Request\Admin\Permission\Edit\Save;
use App\Http\Response\Admin\Permission\Edit\Index;
use App\Http\Response\Error;
use App\Http\Response\Success;
use App\Models\PermissionGroup;
use Illuminate\Http\Request;

class EditController
{
    /**
     * @return \Inertia\Response
     */
    public function index(Request $request, int $id)
    {
        $dto = \App\Libary\View\Admin\Permission\Edit\Index::getViewDto($id);
        return (new Index($dto))->inertia('Admin/Permission/Edit');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request, int $id)
    {
        $validatorClass = (new Save($request, $id));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Admin\Permission\Edit\Save
         */
        $dto = $validatorClass->toDto();

        \App\Libary\View\Admin\Permission\Edit\Save::save($dto);

        return (new Success(
            [
                'Eintrag wurde erfolgreich gespeichert',
            ]
        ))->json();
    }
}
