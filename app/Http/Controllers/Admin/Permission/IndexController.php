<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 22.04.2021
 * Time: 16:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\Permission;

use App\Http\Request\Admin\Permission\Index\Create;
use App\Http\Response\Admin\Permission\Index\Index;
use App\Http\Response\Error;
use App\Http\Response\Success;
use App\Models\PermissionGroup;
use Illuminate\Http\Request;

class IndexController
{

    /**
     * @return \Inertia\Response
     */
    public function index()
    {
        $groups = PermissionGroup::getEntriesAsDto(PermissionGroup::all());
        return (new Index($groups))->inertia('Admin/Permission/Index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $validatorClass = (new Create($request));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Admin\Permission\Index\Create
         */
        $dto = $validatorClass->toDto();

        $group = \App\Libary\View\Admin\Permission\Index\Create::create($dto);
        if ($group) {
            return (new \App\Http\Response\Admin\Permission\Index\Create($group))->json();
        } else {
            return (new Error(
                [
                    'Gruppe konnte nicht angelegt werden',
                ]
            ))->json();
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        // Lösche den Eintrag
        $dto = (new \App\Models\Dto\PermissionGroup())
            ->setId($id);
        (new PermissionGroup())->deleteEntry($dto);

        return (new Success(
            [
                'Erfolgreich gelöscht!'
            ]
        ))->json();
    }
}
