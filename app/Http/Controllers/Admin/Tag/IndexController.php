<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.04.2021
 * Time: 22:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\Tag;

use App\Http\Controllers\Controller;
use App\Http\Response\Admin\Tag\Index\Save;
use App\Http\Response\Admin\Tag\Index\Search;
use App\Http\Response\Error;
use App\Http\Response\Success;
use App\Libary\Logging\Logging;
use App\Models\Tag;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    /**
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        return (new Success([]))->inertia('Admin/Tag/Index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new \App\Http\Request\Admin\Tag\Index\Search($request));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Admin\Tag\Index\Search
         */
        $dto = $validatorClass->toDto();

        $searchResult = (new Tag())->search($dto->getSearch());
        return (new Search($searchResult))->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new \App\Http\Request\Admin\Tag\Index\Save($request));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Admin\Tag\Index\Save
         */
        $dto = $validatorClass->toDto();

        \App\Libary\View\Admin\Tag\Index\Save::save($dto);
        return (new Save())->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        // Lösche den Eintrag
        $dto = (new \App\Models\Dto\Tag())
            ->setId($id);
        (new Tag())->deleteEntry($dto);
        Logging::delete(\App\Models\Tag::class, $dto);
        return (new Success(
            [
                'Erfolgreich gelöscht!'
            ]
        ))->json();
    }
}
