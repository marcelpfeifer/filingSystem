<?php
/**
 * CreateController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 05.06.2021
 * Time: 19:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\News;


use App\Http\Controllers\Controller;
use App\Http\Request\AValidator;
use App\Http\Response\Admin\News\Create\Index;
use App\Http\Response\Admin\News\Create\Save;
use App\Http\Response\AResponse;
use App\Http\Response\Dto\Message;
use App\Http\Response\Enum\Message\Status;
use App\Http\Response\Error;
use Illuminate\Http\Request;

class CreateController extends Controller
{

    /**
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        return (new Index([]))->inertia('Admin/News/Create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new \App\Http\Request\Admin\News\Create\Save($request));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Admin\News\Create\Save
         */
        $dto = $validatorClass->toDto();

        // Save the Data
        \App\Libary\View\Admin\News\Create\Save::save($dto);

        return (new Save())->setMessages(
            [
                (new Message())
                    ->setStatus(Status::SUCCESS)
                    ->setText('Erfolgreich gespeichert!')
            ]
        )->json();
    }
}
