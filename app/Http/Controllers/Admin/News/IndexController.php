<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 05.06.2021
 * Time: 19:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\News;


use App\Http\Controllers\Controller;
use App\Http\Response\Admin\News\Index\Index;
use App\Http\Response\AResponse;

class IndexController extends Controller
{

    /**
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        $data = \App\Libary\View\Admin\News\Index\Index::getViewDto();
        return (new Index($data))->inertia('Admin/News/Index');
    }
}
