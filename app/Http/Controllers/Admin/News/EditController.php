<?php
/**
 * EditController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 05.06.2021
 * Time: 19:25
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\News;


use App\Http\Controllers\Controller;
use App\Http\Response\Admin\News\Edit\Index;
use App\Http\Response\Admin\News\Edit\Save;
use App\Http\Response\AResponse;
use App\Http\Response\Dto\Message;
use App\Http\Response\Enum\Message\Status;
use App\Http\Response\Error;
use Illuminate\Http\Request;

class EditController extends Controller
{

    /**
     * @param Request $request
     * @param int $id
     * @return \Inertia\Response
     */
    public function index(Request $request, int $id): \Inertia\Response
    {
        $data = \App\Libary\View\Admin\News\Edit\Index::getViewDto($id);
        return (new Index($data))->inertia('Admin/News/Edit');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new \App\Http\Request\Admin\News\Edit\Save($request, $id));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Admin\News\Edit\Save
         */
        $dto = $validatorClass->toDto();

        // Save the Data
        \App\Libary\View\Admin\News\Edit\Save::save($dto);

        return (new Save())->setMessages(
            [
                (new Message())
                    ->setStatus(Status::SUCCESS)
                    ->setText('Erfolgreich gespeichert!')
            ]
        )->json();
    }
}
