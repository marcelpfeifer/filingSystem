<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 10.05.2021
 * Time: 14:18
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\ProfilePropertyGroup;


use App\Http\Request\Admin\ProfilePropertyGroup\Index\Save;
use App\Http\Response\Admin\ProfilePropertyGroup\Index\Groups;
use App\Http\Response\Error;
use App\Http\Response\Success;
use App\Libary\Logging\Logging;
use App\Models\ProfilePropertyGroup;
use Illuminate\Http\Request;

class IndexController
{
    /**
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        return (new Success([]))->inertia('Admin/ProfilePropertyGroup/Index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function groups(): \Illuminate\Http\JsonResponse
    {
        return (new Groups(ProfilePropertyGroup::getEntriesAsDto(ProfilePropertyGroup::all())))->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new Save($request));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Admin\ProfilePropertyGroup\Index\Save
         */
        $dto = $validatorClass->toDto();

        // Save the Data
        \App\Libary\View\Admin\ProfilePropertyGroup\Index\Save::save($dto);

        return (new Success(
            [
                'Erfolgreich gespeichert!'
            ]
        ))->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        // Lösche den Eintrag
        $dto = (new \App\Models\Dto\ProfilePropertyGroup())
            ->setId($id);
        (new ProfilePropertyGroup())->deleteEntry($dto);
        Logging::delete(\App\Models\ProfilePropertyGroup::class, $dto);
        return (new Success(
            [
                'Erfolgreich gelöscht!'
            ]
        ))->json();
    }
}
