<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 13:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\Group;


use App\Http\Request\Admin\Group\Index\Save;
use App\Http\Request\AValidator;
use App\Http\Response\Admin\Group\Index\Groups;
use App\Http\Response\Admin\Group\Index\Index;
use App\Http\Response\AResponse;
use App\Http\Response\Error;
use App\Http\Response\Success;
use App\Libary\Logging\Logging;
use App\Models\Group;
use Illuminate\Http\Request;

class IndexController
{
    /**
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        return (new Index([]))->inertia('Admin/Group/Index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function groups(): \Illuminate\Http\JsonResponse
    {
        return (new Groups(Group::getEntriesAsDto(Group::all())))->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new Save($request));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Admin\Group\Index\Save
         */
        $dto = $validatorClass->toDto();

        // Save the Data
        \App\Libary\View\Admin\Group\Index\Save::save($dto);

        return (new Success(
            [
                'Erfolgreich gespeichert!'
            ]
        ))->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        // Lösche den Eintrag
        $dto = (new \App\Models\Dto\Group())
            ->setId($id);
        (new Group())->deleteEntry($dto);
        Logging::delete(\App\Models\ChargeGroup::class, $dto);
        return (new Success(
            [
                'Erfolgreich gelöscht!'
            ]
        ))->json();
    }
}
