<?php
/**
 * EditController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.04.2021
 * Time: 22:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\User;


use App\Http\Controllers\Controller;
use App\Http\Request\Admin\User\Edit\Save;
use App\Http\Response\Admin\User\Edit\Index;
use App\Http\Response\Error;
use App\Http\Response\Success;
use Illuminate\Http\Request;

class EditController extends Controller
{
    /**
     * @return \Inertia\Response
     */
    public function index(Request $request, int $id): \Inertia\Response
    {

        $dto = \App\Libary\View\Admin\User\Edit\Index::getViewDto($id);
        return (new Index($dto))->inertia('Admin/User/Edit');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new Save($request, $id));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Admin\User\Edit\Save
         */
        $dto = $validatorClass->toDto();

        // Save the Data
        \App\Libary\View\Admin\User\Edit\Save::save($dto);

        return (new Success(
            [
                'Erfolgreich gespeichert!'
            ]
        ))->json();
    }
}
