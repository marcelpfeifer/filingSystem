<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.04.2021
 * Time: 22:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Response\Admin\User\Index\Search;
use App\Http\Response\Error;
use App\Http\Response\Success;
use App\Models\User;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    /**
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        return (new Success([]))->inertia('Admin/User/Index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new \App\Http\Request\Admin\User\Index\Search($request));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Admin\User\Index\Search
         */
        $dto = $validatorClass->toDto();

        $result = (new User())->search($dto->getName(), $dto->getOffset(), $dto->getLimit());
        return (new Search($result))->json();
    }
}
