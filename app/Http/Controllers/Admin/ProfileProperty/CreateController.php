<?php
/**
 * CreateController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.04.2021
 * Time: 14:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\ProfileProperty;


use App\Http\Controllers\Controller;
use App\Http\Request\Admin\ProfileProperty\Create\Save;
use App\Http\Response\Admin\ProfileProperty\Create\Index;
use App\Http\Response\Error;
use App\Http\Response\Success;
use App\Models\ChargeGroup;
use App\Models\ProfilePropertyGroup;
use Illuminate\Http\Request;

class CreateController extends Controller
{

    /**
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        return (new Index(ProfilePropertyGroup::getEntriesAsDto(ProfilePropertyGroup::all())))->inertia('Admin/ProfileProperty/Create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new Save($request));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Admin\ProfileProperty\Create\Save
         */
        $dto = $validatorClass->toDto();

        // Save the Data
        \App\Libary\View\Admin\ProfileProperty\Create\Save::save($dto);

        return (new Success(
            [
                'Erfolgreich gespeichert!'
            ]
        ))->json();
    }
}
