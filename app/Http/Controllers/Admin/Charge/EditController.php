<?php
/**
 * EditController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.04.2021
 * Time: 22:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\Charge;


use App\Http\Controllers\Controller;
use App\Http\Request\Admin\Charge\Edit\Save;
use App\Http\Response\Admin\Charge\Edit\Index;
use App\Http\Response\Error;
use App\Http\Response\Success;
use Illuminate\Http\Request;

class EditController extends Controller
{
    /**
     * @return \Inertia\Response
     */
    public function index(Request $request, int $id): \Inertia\Response
    {
        $dto = \App\Libary\View\Admin\Charge\Edit\Index::getViewDto($id);
        return (new Index($dto))->inertia('Admin/Charge/Edit');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new Save($request, $id));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Admin\Charge\Edit\Save
         */
        $dto = $validatorClass->toDto();

        // Save the Data
        \App\Libary\View\Admin\Charge\Edit\Save::save($dto);

        return (new Success(
            [
                'Erfolgreich gespeichert!'
            ]
        ))->json();
    }
}
