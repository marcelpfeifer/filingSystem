<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 10.05.2021
 * Time: 14:18
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\ListDir;


use App\Http\Request\Admin\ListDir\Index\Save;
use App\Http\Response\Admin\ListDir\Index\Lists;
use App\Http\Response\Admin\ListDir\Index\Index;
use App\Http\Response\Error;
use App\Http\Response\Success;
use App\Libary\Logging\Logging;
use App\Models\Group;
use App\Models\ListModel;
use Illuminate\Http\Request;

class IndexController
{
    /**
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        $groups = Group::getEntriesAsDto(Group::all());
        return (new Index($groups))->inertia('Admin/List/Index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function lists(): \Illuminate\Http\JsonResponse
    {
        return (new Lists(ListModel::getEntriesAsDto(ListModel::all())))->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new Save($request));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Admin\ListDir\Index\Save
         */
        $dto = $validatorClass->toDto();

        // Save the Data
        \App\Libary\View\Admin\ListDir\Index\Save::save($dto);

        return (new Success(
            [
                'Erfolgreich gespeichert!'
            ]
        ))->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        // Lösche den Eintrag
        $dto = (new \App\Models\Dto\ListModel())
            ->setId($id);
        (new ListModel())->deleteEntry($dto);
        Logging::delete(\App\Models\ListModel::class, $dto);
        return (new Success(
            [
                'Erfolgreich gelöscht!'
            ]
        ))->json();
    }
}
