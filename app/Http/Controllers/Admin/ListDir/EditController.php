<?php
/**
 * EditController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.07.2021
 * Time: 15:38
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Admin\ListDir;

use App\Http\Controllers\Controller;
use App\Http\Request\Admin\ListDir\Edit\Create;
use App\Http\Request\Admin\ListDir\Edit\Save;
use App\Http\Request\AValidator;
use App\Http\Response\Admin\ListDir\Edit\Index;
use App\Http\Response\AResponse;
use App\Http\Response\Error;
use App\Http\Response\Success;
use App\Libary\Logging\Logging;
use App\Models\ListHeader;
use Illuminate\Http\Request;

class EditController extends Controller
{

    /**
     * @param Request $request
     * @param int $listId
     * @return \Inertia\Response
     */
    public function index(Request $request, int $listId): \Inertia\Response
    {
        $data = \App\Libary\View\Admin\ListDir\Edit\Index::getViewDto($listId);
        return (new Index($data))->inertia('Admin/List/Edit');
    }

    /**
     * @param Request $request
     * @param int $listId
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request, int $listId): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new Create($request, $listId));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Admin\ListDir\Edit\Create
         */
        $dto = $validatorClass->toDto();

        // Save the Data
        $entry = \App\Libary\View\Admin\ListDir\Edit\Create::save($dto);

        return (new \App\Http\Response\Admin\ListDir\Edit\Create($entry))
            ->json();
    }

    /**
     * @param Request $request
     * @param int $headerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request, int $headerId): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new Save($request, $headerId));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Admin\ListDir\Edit\Save
         */
        $dto = $validatorClass->toDto();

        // Save the Data
        \App\Libary\View\Admin\ListDir\Edit\Save::save($dto);

        return (new Success(
            [
                'Erfolgreich gespeichert!'
            ]
        ))->json();
    }

    /**
     * @param Request $request
     * @param int $headerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, int $headerId): \Illuminate\Http\JsonResponse
    {
        // Lösche den Eintrag
        $dto = (new \App\Models\Dto\ListHeader())
            ->setId($headerId);
        (new ListHeader())->deleteEntry($dto);
        Logging::delete(\App\Models\ListHeader::class, $dto);
        return (new Success(
            [
                'Erfolgreich gelöscht!'
            ]
        ))->json();
    }
}
