<?php
/**
 * EditController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.04.2021
 * Time: 11:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Request\Profile\Edit\AddTag;
use App\Http\Request\Profile\Edit\DeleteTag;
use App\Http\Request\Profile\Edit\Save;
use App\Http\Response\Error;
use App\Http\Response\NotFound;
use App\Http\Response\Profile\Edit\Index;
use App\Http\Response\Success;
use App\Libary\Logging\Logging;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EditController extends Controller
{
    /**
     * @return \Inertia\Response
     */
    public function index(Request $request, int $id): \Inertia\Response
    {
        $viewDto = \App\Libary\View\Profile\Edit\Index::getViewDto($id);
        if (!($viewDto)) {
            return (new NotFound([]))->inertia('404');
        }

        return (new Index($viewDto))->inertia('Profile/Edit');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new Save($request, $id));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Profile\Edit\Save
         */
        $dto = $validatorClass->toDto();

        // Save the Data
        \App\Libary\View\Profile\Edit\Save::save($dto);

        return (new Success(
            [
                'Erfolgreich gespeichert!'
            ]
        ))->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        // Lösche den Eintrag
        $dto = (new \App\Models\Dto\Profile())
            ->setId($id);
        (new Profile())->deleteEntry($dto);

        Logging::delete(\App\Models\Profile::class, $dto);
        return (new Success(
            [
                'Erfolgreich gelöscht!'
            ]
        ))->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addTag(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new AddTag($request, $id));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Profile\Edit\AddTag
         */
        $dto = $validatorClass->toDto();

        \App\Libary\View\Profile\Edit\AddTag::save($dto);

        return (new \App\Http\Response\Profile\Edit\AddTag([]))->json();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTag(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new DeleteTag($request, $id));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Profile\Edit\DeleteTag
         */
        $dto = $validatorClass->toDto();

        \App\Libary\View\Profile\Edit\DeleteTag::delete($dto);

        return (new \App\Http\Response\Profile\Edit\DeleteTag([]))->json();
    }
}
