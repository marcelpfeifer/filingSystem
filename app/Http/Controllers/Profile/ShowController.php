<?php
/**
 * ShowController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 12:47
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Profile;


use App\Http\Response\AResponse;
use App\Http\Response\NotFound;
use App\Http\Response\Profile\Show\Index;
use Illuminate\Http\Request;

class ShowController
{
    /**
     * @return \Inertia\Response
     */
    public function index(Request $request, int $id): \Inertia\Response
    {
        $viewDto = \App\Libary\View\Profile\Show\Index::getViewDto($id);
        if (!($viewDto)) {
            return (new NotFound([]))->inertia('404');
        }

        return (new Index($viewDto))->inertia('Profile/Show');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function share(Request $request, int $id): \Illuminate\Http\JsonResponse
    {
        $responseDto = \App\Libary\View\Profile\Show\Share::share($id);
        return (new \App\Http\Response\Profile\Show\Share($responseDto))->json();
    }
}
