<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.04.2021
 * Time: 11:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Response\Error;
use App\Http\Response\Profile\Index\Index;
use App\Http\Response\Profile\Index\Search;
use App\Models\Profile;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    /**
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        $viewDto = \App\Libary\View\Profile\Index\Index::getViewDto();
        return (new Index($viewDto))->inertia('Profile/Index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request): \Illuminate\Http\JsonResponse
    {
        $validatorClass = (new \App\Http\Request\Profile\Index\Search($request));
        $validator = $validatorClass->validator();
        if ($validator->fails()) {
            return (new Error($validator->errors()->all()))->json();
        }

        /**
         * @var $dto \App\Http\Request\Dto\Profile\Index\Search
         */
        $dto = $validatorClass->toDto();
        $tags = array_map(
            function ($entry) {
                return $entry->getId();
            },
            $dto->getTags()
        );
        $profiles = (new Profile())->search($dto->getSearch(),$tags);

        return (new Search($profiles))->json();
    }
}
