<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 20:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Share;

use App\Http\Controllers\Controller;
use App\Http\Response\AResponse;
use App\Http\Response\Share\Incident;
use App\Http\Response\Share\Index;
use App\Http\Response\Share\Profile;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    /**
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        return (new Index([]))->inertia('Share/Index');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Inertia\Response
     */
    public function incident(Request $request, string $code): \Inertia\Response
    {
        $viewDto = \App\Libary\View\Share\Incident::getViewDto($code);
        if (!($viewDto)) {
            return (new Index([]))->inertia('Share/Index');
        }
        return (new Incident($viewDto))->inertia('Share/Incident');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Inertia\Response
     */
    public function profile(Request $request, string $code): \Inertia\Response
    {
        $viewDto = \App\Libary\View\Share\Profile::getViewDto($code);
        if (!($viewDto)) {
            return (new Index([]))->inertia('Share/Index');
        }
        return (new Profile($viewDto))->inertia('Share/Profile');
    }
}
