<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.05.2021
 * Time: 10:17
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers\Warrant;

use App\Http\Controllers\Controller;
use App\Http\Response\Warrant\Index;

class IndexController extends Controller
{

    public function index()
    {
        $viewDto = \App\Libary\View\Warrant\Index::getViewDto();
        return (new Index($viewDto))->inertia('Warrant/Index');
    }
}
