<?php
/**
 * IndexController
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.05.2021
 * Time: 10:12
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Controllers;

use App\Http\Response\AResponse;
use App\Http\Response\Index\Index;
use Inertia\Inertia;

class IndexController extends Controller
{
    /**
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        $data = \App\Libary\View\Index\Index::getViewDto();
        return (new Index($data))->inertia('Index');
    }
}
