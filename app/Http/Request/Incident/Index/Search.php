<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.05.2021
 * Time: 13:24
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Incident\Index;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\Incident\Index\Search\Tag;
use App\Http\Request\IValidator;

class Search extends AValidator implements IValidator
{

    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'search'      => 'string|nullable',
            'tags'        => 'array',
            'tags.*.id'   => 'int',
            'tags.*.name' => 'string',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'search.string'      => 'Der Name darf nur Buchstaben enthalten',
            'tags.array'         => 'Die Tags müssen als Array übergeben werden',
            'tags.*.id.int'      => 'Die TagId muss ein Int sein',
            'tags.*.name.string' => 'Der TagName muss ein String sein',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Incident\Index\Search())
            ->setSearch($this->data['search'] ?? '')
            ->setTags($this->buildTags());
    }

    /**
     * @return Tag[]
     */
    private function buildTags(): array
    {
        $tags = [];
        foreach ($this->data['tags'] ?? [] as $tag) {
            $tags[] = (new Tag())
                ->setId($tag['id'])
                ->setName($tag['name']);
        }

        return $tags;
    }
}

