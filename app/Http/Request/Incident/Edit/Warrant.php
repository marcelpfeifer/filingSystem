<?php
/**
 * Warrant
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 18.05.2021
 * Time: 18:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Incident\Edit;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Warrant extends AValidator implements IValidator
{

    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'profileId' => 'int|required',
            'warrant'   => 'bool|required',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'profileId.required' => 'Die ProfileID muss übergeben werden',
            'profileId.int'      => 'Die ProfileID darf nur Zahlen enthalten',
            'warrant.required'   => 'Der Warrant Status muss übergeben werden',
            'warrant.bool'       => 'Der Warrant Status muss ein Bool sein ',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Incident\Edit\Warrant())
            ->setId($this->id)
            ->setProfileId($this->data['profileId'])
            ->setWarrant($this->data['warrant']);
    }
}
