<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.05.2021
 * Time: 13:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Incident\Edit;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\Dto\Incident\Edit\Save\Description;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{

    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'title'                      => 'string|required',
            'descriptions'               => 'array',
            'descriptions.*.description' => 'string|required',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'title.required'                      => 'Der Title muss übergeben werden',
            'title.string'                        => 'Der Titel darf nur Buchstaben enthalten',
            'descriptions.array'                  => 'Die Beschreibungen müssen übergeben werden',
            'descriptions.*.description.required' => 'Die Beschreibung muss übergeben werden.',
            'descriptions.*.description.string'   => 'Die Beschreibung darf nur Buchstaben enthalten',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        $descriptions = [];
        foreach ($this->data['descriptions'] as $description) {
            $descriptions[] = (new Description())
                ->setDescription($description['description']);
        }
        return (new \App\Http\Request\Dto\Incident\Edit\Save())
            ->setId($this->id)
            ->setTitle($this->data['title'] ?? '')
            ->setDescriptions($descriptions);
    }
}
