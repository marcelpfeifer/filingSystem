<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 18.05.2021
 * Time: 17:45
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Incident\Charge;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;
use App\Models\Dto\Charge;

class Save extends AValidator implements IValidator
{

    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'charges'               => 'array|required',
            'charges.*.id'          => 'int|required',
            'charges.*.name'        => 'string|required',
            'charges.*.description' => 'string|required',
            'charges.*.time'        => 'int|required',
            'charges.*.money'       => 'int|required',
            'profileId'             => 'int|required',
            'incidentId'            => 'int|required',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'charges.required'               => 'Die Charges müssen übergeben werden',
            'charges.array'                  => 'Die Charges müssen ein Array sein',
            'charges.*.id.required'          => 'Der Charge muss eine ID haben',
            'charges.*.id.int'               => 'Die Charge ID muss ein Int sein',
            'charges.*.name.required'        => 'Der Charge muss einen Namen haben',
            'charges.*.name.string'          => 'Der Charge Name muss ein String sein',
            'charges.*.description.required' => 'Die Charge Beschreibung muss übergeben werden',
            'charges.*.description.string'   => 'Die Charge Beschreibung muss ein String sein',
            'charges.*.time.required'        => 'Charge Time muss übergeben werden',
            'charges.*.time.int'             => 'Charge Time muss ein Integer sein',
            'charges.*.money.required'       => 'Charge Money muss übergeben werden',
            'charges.*.money.int'            => 'Charge Money muss ein Int sein',
            'profileId.required'             => 'Die profileId muss übergeben werden',
            'profileId.int'                  => 'Die profileId muss ein Integer sein',
            'incidentId.required'            => 'Die incidentId muss übergeben werden',
            'incidentId.int'                 => 'Die incidentId muss ein Integer sein',

        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Incident\Charge\Save())
            ->setIncidentId($this->data['incidentId'])
            ->setProfileId($this->data['profileId'])
            ->setCharges($this->buildCharges());
    }

    /**
     * @return Charge[]
     */
    private function buildCharges():array
    {
        $charges = [];
        foreach ($this->data['charges'] as $charge) {
            $charges[] = (new Charge())
                ->setId($charge['id'])
                ->setName($charge['name'])
                ->setDescription($charge['description'])
                ->setMoney($charge['money'])
                ->setTime($charge['time']);
        }
        return $charges;
    }
}
