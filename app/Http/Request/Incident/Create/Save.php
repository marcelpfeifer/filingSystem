<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.05.2021
 * Time: 13:30
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Incident\Create;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{

    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'title'       => 'string|required',
            'description' => 'string|nullable',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'title.required'     => 'Der Title muss übergeben werden',
            'title.string'       => 'Der Titel darf nur Buchstaben enthalten',
            'description.string' => 'Die Beschreibung darf nur Buchstaben enthalten',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Incident\Create\Save())
            ->setTitle($this->data['title'] ?? '')
            ->setDescription($this->data['description'] ?? '');
    }
}

