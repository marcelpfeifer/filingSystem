<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 10.05.2021
 * Time: 09:00
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\Charge\Index;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Search extends AValidator implements IValidator
{
    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'name'   => 'string|nullable',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'name.string'     => 'Der Name darf nur Buchstaben enthalten',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\Charge\Index\Search())
            ->setName($this->data['name'] ?? '');
    }
}
