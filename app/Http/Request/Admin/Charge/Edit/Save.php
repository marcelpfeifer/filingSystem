<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 01:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\Charge\Edit;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{
    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'name'          => 'string|required',
            'description'   => 'string|required',
            'chargeGroupId' => 'int|required',
            'money'         => 'int|nullable',
            'time'          => 'int|nullable',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'name.required'          => 'Der Name muss übergeben werden',
            'name.string'            => 'Der Name darf nur Buchstaben enthalten',
            'description.required'   => 'Die Beschreibung muss übergeben werden',
            'description.string'     => 'Die Beschreibung darf nur Buchstaben enthalten',
            'chargeGroupId.required' => 'Die Gruppe muss übergeben werden',
            'chargeGroupId.int'      => 'Die Gruppe darf nur Zahlen enthalten',
            'money.int'              => 'Der Geldbetrag darf nur Zahlen enthalten',
            'time.int'               => 'Die Zeit darf nur Zahlen enthalten',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\Charge\Edit\Save())
            ->setId($this->id)
            ->setName($this->data['name'] ?? '')
            ->setDescription($this->data['description'] ?? '')
            ->setChargeGroupId($this->data['chargeGroupId'] ?? 0)
            ->setMoney($this->data['money'] ?? null)
            ->setTime($this->data['time'] ?? null);
    }
}
