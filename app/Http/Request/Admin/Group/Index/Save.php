<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 13:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\Group\Index;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{
    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'name'    => 'string|required',
            'showAll' => 'bool|required',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'name.required'    => 'Der Name muss übergeben werden',
            'name.string'      => 'Der Name darf nur Buchstaben enthalten',
            'showAll.required' => 'Show All muss übergeben werden',
            'showAll.bool'     => 'ShowAll muss ein Bool sein',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\Group\Index\Save())
            ->setName($this->data['name'] ?? '')
            ->setShowAll($this->data['showAll'] ?? '');
    }
}
