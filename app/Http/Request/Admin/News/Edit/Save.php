<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.06.2021
 * Time: 12:57
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\News\Edit;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{

    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'title'   => 'string|required',
            'content' => 'string|required',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'title.required'   => 'Der Title muss übergeben werden',
            'title.string'     => 'Der Title darf nur Buchstaben enthalten',
            'content.required' => 'Der Content muss übergeben werden',
            'content.string'   => 'Der Content darf nur Buchstaben enthalten',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\News\Edit\Save())
            ->setId($this->id)
            ->setTitle($this->data['title'] ?? '')
            ->setContent($this->data['content'] ?? '');
    }
}
