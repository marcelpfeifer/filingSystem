<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.04.2021
 * Time: 14:59
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\User\Index;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Search extends AValidator implements IValidator
{

    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'name'   => 'string|nullable',
            'offset' => 'int|required',
            'limit'  => 'int|required',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'name.string'     => 'Der Name darf nur Buchstaben enthalten',
            'offset.required' => 'Das Offset muss übergeben werden',
            'offset.int'      => 'Das Offset darf nur Zahlen enthalten',
            'limit.required'  => 'Das Limit muss übergeben werden',
            'limit.int'       => 'Das Limit darf nur Zahlen enthalten',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\User\Index\Search())
            ->setName($this->data['name'] ?? '')
            ->setOffset($this->data['offset'] ?? 0)
            ->setLimit($this->data['limit'] ?? 15);
    }
}
