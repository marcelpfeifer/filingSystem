<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.04.2021
 * Time: 15:35
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\User\Edit;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{

    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'name'              => 'string|required',
            'password'          => 'string|nullable',
            'permissionGroupId' => 'int|required',
            'groupId'           => 'int|required',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'name.required'              => 'Der Name muss übergeben werden',
            'name.string'                => 'Der Name darf nur Buchstaben enthalten',
            'password.string'            => 'Das Passwort darf nur Buchstaben enthalten',
            'permissionGroupId.required' => 'Die Berechtigungsgruppe muss übergeben werden',
            'permissionGroupId.int'      => 'Die Berechtigungsgruppe darf nur Zahlen enthalten',
            'groupId.required'           => 'Die Gruppe muss übergeben werden',
            'groupId.int'                => 'Die Gruppe darf nur Zahlen enthalten',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\User\Edit\Save())
            ->setId($this->id)
            ->setName($this->data['name'] ?? '')
            ->setPassword($this->data['password'] ?? '')
            ->setPermissionGroupId($this->data['permissionGroupId'] ?? 0)
            ->setGroupId($this->data['groupId'] ?? 0);
    }
}
