<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 01:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\ProfileProperty\Edit;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{
    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'name'                   => 'string|required',
            'profilePropertyGroupId' => 'int|required',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'name.required'                   => 'Der Name muss übergeben werden',
            'name.string'                     => 'Der Name darf nur Buchstaben enthalten',
            'profilePropertyGroupId.required' => 'Die Gruppe muss übergeben werden',
            'profilePropertyGroupId.int'      => 'Die Gruppe darf nur Zahlen enthalten',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\ProfileProperty\Edit\Save())
            ->setId($this->id)
            ->setName($this->data['name'] ?? '')
            ->setProfilePropertyGroupId($this->data['profilePropertyGroupId'] ?? 0);
    }
}
