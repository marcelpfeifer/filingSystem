<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.07.2021
 * Time: 11:16
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\Tag\Index;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{
    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'name'     => 'string|required',
            'incident' => 'bool|required',
            'profile'  => 'bool|required',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'name.required'     => 'Der Name muss übergeben werden',
            'name.string'       => 'Der Name darf nur Buchstaben enthalten',
            'incident.required' => 'Ob der Tag für einen Vorfall ist muss übergeben werden',
            'incident.bool'     => 'Der Tag für einen Vorfall muss ein bool sein',
            'profile.required'  => 'Ob der Tag für ein Profil ist muss übergeben werden',
            'profile.bool'      => 'Der Tag für ein Profil muss ein bool sein',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\Tag\Index\Save())
            ->setName($this->data['name'])
            ->setIncident($this->data['incident'])
            ->setProfile($this->data['profile']);
    }
}
