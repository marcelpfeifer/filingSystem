<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 13:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\ListDir\Index;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{
    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'name'       => 'string|required',
            'groupIds'   => 'array|required',
            'groupIds.*' => [
                'int',
                'exists:group,id',
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'name.required'     => 'Der Name muss übergeben werden.',
            'name.string'       => 'Der Name darf nur Buchstaben enthalten.',
            'groupIds.required' => 'Die Gruppe muss übergeben werden.',
            'groupIds.*.int'    => 'Die Gruppe darf nur Zahlen enthalten.',
            'groupIds.*.exists' => 'Die Gruppe existiert nicht.',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\ListDir\Index\Save())
            ->setName($this->data['name'] ?? '')
            ->setGroupIds($this->data['groupIds']);
    }
}
