<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 13:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Admin\ListDir\Edit;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{
    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'name'  => 'string|required',
            'list'  => 'bool|required',
            'order' => 'int|required',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'name.required'  => 'Der Name muss übergeben werden',
            'name.string'    => 'Der Name darf nur Buchstaben enthalten',
            'list.required'  => 'Die Liste muss übergeben werden',
            'list.bool'      => 'Die Liste muss ein Bool sein',
            'order.required' => 'Die Reihenfolge muss übergeben werden',
            'order.int'      => 'Die Reihenfolge darf nur Zahlen enthalten',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Admin\ListDir\Edit\Save())
            ->setHeaderId($this->id)
            ->setName($this->data['name'])
            ->setList($this->data['list'])
            ->setOrder($this->data['order']);
    }
}
