<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.07.2021
 * Time: 13:11
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\ListDir\Entry\Edit;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{

    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'values'   => 'array',
            'values.*' => 'string|nullable',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'values.array'    => 'Die Werte müssen als Array übergeben werden',
            'values.*.string' => 'Die Werte dürfen nur Buchstaben enthalten',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\ListDir\Entry\Edit\Save())
            ->setEntryId($this->id)
            ->setValues($this->data['values'] ?? []);
    }
}
