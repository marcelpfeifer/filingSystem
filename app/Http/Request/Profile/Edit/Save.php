<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.04.2021
 * Time: 11:21
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Profile\Edit;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{

    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'name'         => 'required|string',
            'image'        => [
                'nullable',
                'string',
                'active_url',
                'url',
            ],
            'description'  => [
                'nullable',
                'string',
            ],
            'properties'   => 'array',
            'properties.*' => 'string|nullable',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'name.required'       => 'Der Name muss angegeben werden',
            'name.string'         => 'Der Name darf nur Buchstaben enthalten',
            'image.string'        => 'Das Bild-URL darf nur Buchstaben enthalten',
            'image.active_url'    => 'Die Bild-URL muss erreichbar sein',
            'image.url'           => 'Das Bild-URL muss eine URL sein',
            'description.string'  => 'Die Beschreibung darf nur Buchstaben enthalten',
            'properties.array'    => 'Die Eigenschaften müssen als Array übergeben werden',
            'properties.*.string' => 'Die Eigenschaften dürfen nur Buchstaben enthalten',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Profile\Edit\Save())
            ->setId($this->id)
            ->setName($this->data['name'] ?? '')
            ->setDescription($this->data['description'] ?? '')
            ->setImageUrl($this->data['image'] ?? '')
            ->setProperties($this->data['properties'] ?? []);
    }
}
