<?php
/**
 * AddTag
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.07.2021
 * Time: 12:41
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Profile\Edit;


use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class AddTag extends AValidator implements IValidator
{

    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'tagId' => 'int|required',
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'tagId.required' => 'Die tagId muss übergeben werden',
            'tagId.int'      => 'Die tagId darf nur Zahlen enthalten',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Profile\Edit\AddTag())
            ->setId($this->id)
            ->setTagId($this->data['tagId']);
    }
}
