<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.07.2021
 * Time: 13:14
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\ListDir\Entry\Create;


use App\Http\Request\Dto\ADto;

class Save extends ADto
{
    /**
     * @var int
     */
    private $listId = 0;

    /**
     * @var string[]
     */
    private $values = [];

    /**
     * @return int
     */
    public function getListId(): int
    {
        return $this->listId;
    }

    /**
     * @param int $listId
     * @return Save
     */
    public function setListId(int $listId): Save
    {
        $this->listId = $listId;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @param string[] $values
     * @return Save
     */
    public function setValues(array $values): Save
    {
        $this->values = $values;
        return $this;
    }
}
