<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.07.2021
 * Time: 15:37
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\ListDir\Entry\Index;


use App\Http\Request\Dto\ADto;

class Search extends ADto
{
    /**
     * @var int
     */
    private $listId = 0;

    /**
     * @var string
     */
    private $search = '';

    /**
     * @return int
     */
    public function getListId(): int
    {
        return $this->listId;
    }

    /**
     * @param int $listId
     * @return Search
     */
    public function setListId(int $listId): Search
    {
        $this->listId = $listId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSearch(): string
    {
        return $this->search;
    }

    /**
     * @param string $search
     * @return Search
     */
    public function setSearch(string $search): Search
    {
        $this->search = $search;
        return $this;
    }
}
