<?php
/**
 * Create
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 25.04.2021
 * Time: 12:54
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\Permission\Index;

use App\Http\Request\Dto\ADto;

class Create extends ADto
{

    /**
     * @var string
     */
    private $name = '';

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Create
     */
    public function setName(string $name): Create
    {
        $this->name = $name;
        return $this;
    }
}
