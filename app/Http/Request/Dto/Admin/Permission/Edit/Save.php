<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.04.2021
 * Time: 22:28
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\Permission\Edit;

use App\Http\Request\Dto\ADto;

class Save extends ADto
{

    /**
     * @var int
     */
    private $groupId = 0;

    /**
     * @var int[]
     */
    private $permissions = [];

    /**
     * @return int
     */
    public function getGroupId(): int
    {
        return $this->groupId;
    }

    /**
     * @param int $groupId
     * @return Save
     */
    public function setGroupId(int $groupId): Save
    {
        $this->groupId = $groupId;
        return $this;
    }

    /**
     * @return int[]
     */
    public function getPermissions(): array
    {
        return $this->permissions;
    }

    /**
     * @param int[] $permissions
     * @return Save
     */
    public function setPermissions(array $permissions): Save
    {
        $this->permissions = $permissions;
        return $this;
    }
}
