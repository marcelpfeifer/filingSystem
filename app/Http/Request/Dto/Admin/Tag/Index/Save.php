<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.07.2021
 * Time: 11:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\Tag\Index;


use App\Http\Request\Dto\ADto;

class Save extends ADto
{

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var bool
     */
    private $incident = false;

    /**
     * @var bool
     */
    private $profile = false;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Save
     */
    public function setName(string $name): Save
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIncident(): bool
    {
        return $this->incident;
    }

    /**
     * @param bool $incident
     * @return Save
     */
    public function setIncident(bool $incident): Save
    {
        $this->incident = $incident;
        return $this;
    }

    /**
     * @return bool
     */
    public function isProfile(): bool
    {
        return $this->profile;
    }

    /**
     * @param bool $profile
     * @return Save
     */
    public function setProfile(bool $profile): Save
    {
        $this->profile = $profile;
        return $this;
    }
}
