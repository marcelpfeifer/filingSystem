<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.05.2021
 * Time: 15:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\ProfileProperty\Create;


use App\Http\Request\Dto\ADto;

class Save extends ADto
{

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var int
     */
    private $profilePropertyGroupId = 0;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Save
     */
    public function setName(string $name): Save
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getProfilePropertyGroupId(): int
    {
        return $this->profilePropertyGroupId;
    }

    /**
     * @param int $profilePropertyGroupId
     * @return Save
     */
    public function setProfilePropertyGroupId(int $profilePropertyGroupId): Save
    {
        $this->profilePropertyGroupId = $profilePropertyGroupId;
        return $this;
    }
}
