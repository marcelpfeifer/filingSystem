<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 01:21
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\Charge\Edit;


use App\Http\Request\Dto\ADto;

class Save extends ADto
{

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $description = '';

    /**
     * @var int
     */
    private $chargeGroupId = 0;

    /**
     * @var int|null
     */
    private $money = null;

    /**
     * @var int|null
     */
    private $time = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Save
     */
    public function setId(int $id): Save
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Save
     */
    public function setName(string $name): Save
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Save
     */
    public function setDescription(string $description): Save
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getChargeGroupId(): int
    {
        return $this->chargeGroupId;
    }

    /**
     * @param int $chargeGroupId
     * @return Save
     */
    public function setChargeGroupId(int $chargeGroupId): Save
    {
        $this->chargeGroupId = $chargeGroupId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMoney(): ?int
    {
        return $this->money;
    }

    /**
     * @param int|null $money
     * @return Save
     */
    public function setMoney(?int $money): Save
    {
        $this->money = $money;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTime(): ?int
    {
        return $this->time;
    }

    /**
     * @param int|null $time
     * @return Save
     */
    public function setTime(?int $time): Save
    {
        $this->time = $time;
        return $this;
    }
}
