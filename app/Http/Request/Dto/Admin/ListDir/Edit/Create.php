<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 13:36
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\ListDir\Edit;

use App\Http\Request\Dto\ADto;

class Create extends ADto
{

    /**
     * @var int
     */
    private $listId = 0;
    /**
     * @var string
     */
    private $name = '';

    /**
     * @return int
     */
    public function getListId(): int
    {
        return $this->listId;
    }

    /**
     * @param int $listId
     * @return Create
     */
    public function setListId(int $listId): Create
    {
        $this->listId = $listId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Create
     */
    public function setName(string $name): Create
    {
        $this->name = $name;
        return $this;
    }
}
