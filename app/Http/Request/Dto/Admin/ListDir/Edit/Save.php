<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 13:36
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\ListDir\Edit;

use App\Http\Request\Dto\ADto;

class Save extends ADto
{

    /**
     * @var int
     */
    private $headerId = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var bool
     */
    private $list = false;

    /**
     * @var int
     */
    private $order = 0;

    /**
     * @return int
     */
    public function getHeaderId(): int
    {
        return $this->headerId;
    }

    /**
     * @param int $headerId
     * @return Save
     */
    public function setHeaderId(int $headerId): Save
    {
        $this->headerId = $headerId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Save
     */
    public function setName(string $name): Save
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isList(): bool
    {
        return $this->list;
    }

    /**
     * @param bool $list
     * @return Save
     */
    public function setList(bool $list): Save
    {
        $this->list = $list;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     * @return Save
     */
    public function setOrder(int $order): Save
    {
        $this->order = $order;
        return $this;
    }
}
