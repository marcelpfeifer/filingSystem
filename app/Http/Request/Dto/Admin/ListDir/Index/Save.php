<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 13:36
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\ListDir\Index;

use App\Http\Request\Dto\ADto;

class Save extends ADto
{

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var int[]
     */
    private $groupIds = [];

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Save
     */
    public function setName(string $name): Save
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int[]
     */
    public function getGroupIds(): array
    {
        return $this->groupIds;
    }

    /**
     * @param int[] $groupIds
     * @return Save
     */
    public function setGroupIds(array $groupIds): Save
    {
        $this->groupIds = $groupIds;
        return $this;
    }
}
