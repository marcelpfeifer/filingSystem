<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 06.05.2021
 * Time: 14:36
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\User\Edit;

use App\Http\Request\Dto\ADto;

class Save extends ADto
{
    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $password = '';

    /**
     * @var int
     */
    private $permissionGroupId = 0;

    /**
     * @var int
     */
    private $groupId = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Save
     */
    public function setId(int $id): Save
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Save
     */
    public function setName(string $name): Save
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Save
     */
    public function setPassword(string $password): Save
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return int
     */
    public function getPermissionGroupId(): int
    {
        return $this->permissionGroupId;
    }

    /**
     * @param int $permissionGroupId
     * @return Save
     */
    public function setPermissionGroupId(int $permissionGroupId): Save
    {
        $this->permissionGroupId = $permissionGroupId;
        return $this;
    }

    /**
     * @return int
     */
    public function getGroupId(): int
    {
        return $this->groupId;
    }

    /**
     * @param int $groupId
     * @return Save
     */
    public function setGroupId(int $groupId): Save
    {
        $this->groupId = $groupId;
        return $this;
    }
}
