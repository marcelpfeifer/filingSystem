<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.04.2021
 * Time: 15:38
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Admin\User\Create;

use App\Http\Request\Dto\ADto;

class Save extends ADto
{

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $password = '';

    /**
     * @var int
     */
    private $permissionGroupId = 0;

    /**
     * @var int
     */
    private $groupId = 0;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Save
     */
    public function setName(string $name): Save
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Save
     */
    public function setPassword(string $password): Save
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return int
     */
    public function getPermissionGroupId(): int
    {
        return $this->permissionGroupId;
    }

    /**
     * @param int $permissionGroupId
     * @return Save
     */
    public function setPermissionGroupId(int $permissionGroupId): Save
    {
        $this->permissionGroupId = $permissionGroupId;
        return $this;
    }

    /**
     * @return int
     */
    public function getGroupId(): int
    {
        return $this->groupId;
    }

    /**
     * @param int $groupId
     * @return Save
     */
    public function setGroupId(int $groupId): Save
    {
        $this->groupId = $groupId;
        return $this;
    }
}
