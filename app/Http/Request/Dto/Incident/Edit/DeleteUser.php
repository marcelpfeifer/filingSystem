<?php
/**
 * AddUser
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 17.05.2021
 * Time: 13:01
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Incident\Edit;

use App\Http\Request\Dto\ADto;

class DeleteUser extends ADto
{

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var int
     */
    private $profileId = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return DeleteUser
     */
    public function setId(int $id): DeleteUser
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getProfileId(): int
    {
        return $this->profileId;
    }

    /**
     * @param int $profileId
     * @return DeleteUser
     */
    public function setProfileId(int $profileId): DeleteUser
    {
        $this->profileId = $profileId;
        return $this;
    }
}
