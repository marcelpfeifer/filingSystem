<?php
/**
 * Warrant
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 18.05.2021
 * Time: 18:47
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Incident\Edit;


use App\Http\Request\Dto\ADto;

class Warrant extends ADto
{

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var int
     */
    private $profileId = 0;

    /**
     * @var bool
     */
    private $warrant = false;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Warrant
     */
    public function setId(int $id): Warrant
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getProfileId(): int
    {
        return $this->profileId;
    }

    /**
     * @param int $profileId
     * @return Warrant
     */
    public function setProfileId(int $profileId): Warrant
    {
        $this->profileId = $profileId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isWarrant(): bool
    {
        return $this->warrant;
    }

    /**
     * @param bool $warrant
     * @return Warrant
     */
    public function setWarrant(bool $warrant): Warrant
    {
        $this->warrant = $warrant;
        return $this;
    }
}
