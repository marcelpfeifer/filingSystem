<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.05.2021
 * Time: 13:34
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Incident\Edit;


use App\Http\Request\Dto\ADto;
use App\Http\Request\Dto\Incident\Edit\Save\Description;

class Save extends ADto
{

    /**
     * @var int
     */
    private $id = 0;
    /**
     * @var string
     */
    private $title = '';

    /**
     * @var Description[]
     */
    private $descriptions = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Save
     */
    public function setId(int $id): Save
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Save
     */
    public function setTitle(string $title): Save
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return Description[]
     */
    public function getDescriptions(): array
    {
        return $this->descriptions;
    }

    /**
     * @param Description[] $descriptions
     * @return Save
     */
    public function setDescriptions(array $descriptions): Save
    {
        $this->descriptions = $descriptions;
        return $this;
    }
}
