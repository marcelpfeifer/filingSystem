<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 09.07.2021
 * Time: 14:09
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Incident\Group;


use App\Http\Request\Dto\ADto;

class Save extends ADto
{
    /**
     * @var int
     */
    private $incidentId = 0;

    /**
     * @var int[]
     */
    private $selectedGroups = [];

    /**
     * @return int
     */
    public function getIncidentId(): int
    {
        return $this->incidentId;
    }

    /**
     * @param int $incidentId
     * @return Save
     */
    public function setIncidentId(int $incidentId): Save
    {
        $this->incidentId = $incidentId;
        return $this;
    }

    /**
     * @return int[]
     */
    public function getSelectedGroups(): array
    {
        return $this->selectedGroups;
    }

    /**
     * @param int[] $selectedGroups
     * @return Save
     */
    public function setSelectedGroups(array $selectedGroups): Save
    {
        $this->selectedGroups = $selectedGroups;
        return $this;
    }
}
