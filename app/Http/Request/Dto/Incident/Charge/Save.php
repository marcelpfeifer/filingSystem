<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 18.05.2021
 * Time: 17:46
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Incident\Charge;


use App\Http\Request\Dto\ADto;
use App\Models\Dto\Charge;

class Save extends ADto
{

    /**
     * @var Charge[]
     */
    private $charges = [];

    /**
     * @var int
     */
    private $profileId = 0;

    /**
     * @var int
     */
    private $incidentId = 0;

    /**
     * @return Charge[]
     */
    public function getCharges(): array
    {
        return $this->charges;
    }

    /**
     * @param Charge[] $charges
     * @return Save
     */
    public function setCharges(array $charges): Save
    {
        $this->charges = $charges;
        return $this;
    }

    /**
     * @return int
     */
    public function getProfileId(): int
    {
        return $this->profileId;
    }

    /**
     * @param int $profileId
     * @return Save
     */
    public function setProfileId(int $profileId): Save
    {
        $this->profileId = $profileId;
        return $this;
    }

    /**
     * @return int
     */
    public function getIncidentId(): int
    {
        return $this->incidentId;
    }

    /**
     * @param int $incidentId
     * @return Save
     */
    public function setIncidentId(int $incidentId): Save
    {
        $this->incidentId = $incidentId;
        return $this;
    }
}
