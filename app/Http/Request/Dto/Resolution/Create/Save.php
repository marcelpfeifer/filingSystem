<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 30.06.2021
 * Time: 13:14
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Resolution\Create;


use App\Http\Request\Dto\ADto;
use Illuminate\Http\UploadedFile;

class Save extends ADto
{

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var UploadedFile|null
     */
    private $file = null;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Save
     */
    public function setTitle(string $title): Save
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @param UploadedFile|null $file
     * @return Save
     */
    public function setFile(?UploadedFile $file): Save
    {
        $this->file = $file;
        return $this;
    }
}
