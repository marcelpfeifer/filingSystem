<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 20.04.2021
 * Time: 13:02
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Profile\Index;

use App\Http\Request\Dto\ADto;
use App\Http\Request\Incident\Index\Search\Tag;

class Search extends ADto
{

    /**
     * @var string
     */
    private $search = '';

    /**
     * @var Tag[]
     */
    private $tags = [];

    /**
     * @return string
     */
    public function getSearch(): string
    {
        return $this->search;
    }

    /**
     * @param string $search
     * @return Search
     */
    public function setSearch(string $search): Search
    {
        $this->search = $search;
        return $this;
    }

    /**
     * @return Tag[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param Tag[] $tags
     * @return Search
     */
    public function setTags(array $tags): Search
    {
        $this->tags = $tags;
        return $this;
    }
}
