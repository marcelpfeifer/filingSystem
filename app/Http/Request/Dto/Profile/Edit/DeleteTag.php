<?php
/**
 * DeleteTag
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.07.2021
 * Time: 12:43
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Dto\Profile\Edit;

use App\Http\Request\Dto\ADto;

class DeleteTag extends ADto
{

    /**
     * @var int
     */
    private $id = 0;

    /**
     * @var int
     */
    private $tagId = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return DeleteTag
     */
    public function setId(int $id): DeleteTag
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getTagId(): int
    {
        return $this->tagId;
    }

    /**
     * @param int $tagId
     * @return DeleteTag
     */
    public function setTagId(int $tagId): DeleteTag
    {
        $this->tagId = $tagId;
        return $this;
    }
}
