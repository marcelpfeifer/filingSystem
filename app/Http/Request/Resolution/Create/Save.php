<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 30.06.2021
 * Time: 13:24
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Request\Resolution\Create;

use App\Http\Request\AValidator;
use App\Http\Request\Dto\ADto;
use App\Http\Request\IValidator;

class Save extends AValidator implements IValidator
{

    /**
     * @return \string[][]
     */
    public function getRules(): array
    {
        return [
            'title'  => 'required|string',
            'file' => [
                'mimes:pdf',
                'required',
                'max:20000'
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function getMessages(): array
    {
        return [
            'title.required' => 'Der Titel muss angegeben werden',
            'title.string'   => 'Der Titel darf nur Buchstaben enthalten',
            'file.required'  => 'Die Datei muss übergeben werden',
            'file.max'       => 'Die Datei ist zu groß',
            'file.mimes'     => 'Die Datei muss eine PDF sein',
        ];
    }

    /**
     * @return ADto
     */
    public function toDto(): ADto
    {
        return (new \App\Http\Request\Dto\Resolution\Create\Save())
            ->setTitle($this->data['title'])
            ->setFile($this->data['file']);
    }
}
