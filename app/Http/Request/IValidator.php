<?php

namespace App\Http\Request;


use App\Http\Request\Dto\ADto;

interface IValidator
{
    /**
     * @return array
     */
    public function getRules(): array;

    /**
     * @return array
     */
    public function getMessages(): array;

    /**
     * @return ADto
     */
    public function toDto(): ADto;
}
