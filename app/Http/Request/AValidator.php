<?php

namespace App\Http\Request;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

abstract class AValidator implements IValidator
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var null|int
     */
    protected $id = null;

    /**
     * AValidator constructor.
     * @param Request $request
     * @param int|null $id
     */
    public function __construct(Request $request, int $id = null)
    {
        $this->request = $request;
        $this->data = $this->request->all();
        $this->id = $id;
    }

    /**
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(): \Illuminate\Contracts\Validation\Validator
    {
        return Validator::make(
            $this->data,
            $this->getRules(),
            $this->getMessages()
        );
    }
}
