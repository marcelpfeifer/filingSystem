<?php
/**
 * Permission
 * Copyright (C) Marcel Pfeifer 2020 <webmaster@m-pfeifer.de>
 * Date: 25.07.2020
 * Time: 13:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class Permission
{
    /**
     * @param $request
     * @param Closure $next
     * @param string $permission
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|mixed
     */
    public function handle($request, Closure $next, string $permission)
    {
        if (\App\Libary\Permission\Permission::allowed($permission)) {
            return $next($request);
        }

        return response(
            [
                'error' => [
                    'code'        => 'INSUFFICIENT_PERMISSION',
                    'description' => 'You are not authorized to access this resource.',
                ],
            ],
            401
        );
    }
}
