<?php
/**
 * Status
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.05.2021
 * Time: 13:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Enum\Message;


use App\Libary\Structure\AEnum;

class Status extends AEnum
{

    /**
     * @var string
     */
    const SUCCESS = 'success';

    /**
     * @var string
     */
    const INFO = 'info';

    /**
     * @var string
     */
    const ERROR = 'error';
}
