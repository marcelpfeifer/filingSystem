<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 30.06.2021
 * Time: 13:07
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Resolution\Show;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Models\Dto\Resolution;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $data Resolution
         */
        $data = $this->resource;


        return [
            'entry' => [
                'id'    => $data->getId(),
                'title' => $data->getTitle(),
                'path'  => url($data->getPath()),
            ]
        ];
    }
}
