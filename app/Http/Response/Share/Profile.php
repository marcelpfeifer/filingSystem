<?php
/**
 * Profile
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 20:51
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Share;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Profile extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Share\Profile
         */
        $dto = $this->resource;

        $charges = [];

        foreach ($dto->getCharges() as $charge) {
            $charges[$charge->getId()] = [
                'id'    => $charge->getId(),
                'name'  => $charge->getName(),
                'count' => $charge->getCount(),
            ];
        }
        return [
            'id'          => $dto->getProfile()->getId(),
            'name'        => $dto->getProfile()->getName(),
            'image'       => $dto->getProfile()->getImageUrl(),
            'description' => $dto->getProfile()->getDescription(),
            'charges'     => $charges,
        ];
    }
}
