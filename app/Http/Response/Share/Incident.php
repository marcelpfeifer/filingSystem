<?php
/**
 * Incident
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 20:51
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Share;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Incident extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Share\Incident
         */
        $dto = $this->resource;

        $descriptions = [];
        foreach ($dto->getIncident()->getDescriptions() as $description) {
            $descriptions[] = [
                'description' => $description->getDescription(),
            ];
        }

        $profiles = [];
        foreach ($dto->getProfiles() as $profile) {
            $profiles[$profile->getId()] = [
                'id'      => $profile->getId(),
                'name'    => $profile->getName(),
                'image'   => $profile->getImage(),
                'warrant' => $profile->isWarrant(),
            ];
        }

        return [
            'incident' => [
                'id'           => $dto->getIncident()->getId(),
                'title'        => $dto->getIncident()->getTitle(),
                'descriptions' => $descriptions,
            ],
            'profiles' => $profiles,
        ];
    }
}
