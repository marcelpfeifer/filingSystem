<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.05.2021
 * Time: 10:26
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Warrant;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Warrant\Index
         */
        $dto = $this->resource;

        $warrants = [];

        foreach ($dto->getWarrants() as $warrant) {
            $warrants[] = [
                'incident' => [
                    'id' => $warrant->getIncidentToProfile()->getIncidentId()
                ],
                'profile'  => [
                    'id'    => $warrant->getProfile()->getId(),
                    'name'  => $warrant->getProfile()->getName(),
                    'image' => $warrant->getProfile()->getImageUrl(),
                ]
            ];
        }

        return [
            'warrants' => $warrants
        ];
    }
}
