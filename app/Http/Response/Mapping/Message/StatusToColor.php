<?php
/**
 * StatusToColor
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.05.2021
 * Time: 13:56
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Mapping\Message;


use App\Http\Response\Enum\Message\Status;

class StatusToColor
{

    /**
     * @param string $status
     * @return string
     */
    public static function map(string $status): string
    {
        $color = 'blue';
        switch ($status) {
            case Status::SUCCESS:
                $color = 'green';
                break;
            case Status::INFO:
                $color = 'yellow';
                break;
            case Status::ERROR:
                $color = 'red';
                break;
        }
        return $color;
    }
}
