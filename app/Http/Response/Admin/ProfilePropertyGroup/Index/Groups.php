<?php
/**
 * Groups
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 10.05.2021
 * Time: 14:24
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\ProfilePropertyGroup\Index;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Models\Dto\ProfilePropertyGroup;

class Groups extends AResponse implements IResponse
{

    public function getData(): array
    {
        /**
         * @var $data ProfilePropertyGroup[]
         */
        $data = $this->resource;
        $result = [];

        foreach ($data as $entry) {
            $result[] = [
                'id'   => $entry->getId(),
                'name' => $entry->getName(),
            ];
        }

        return [
            'result'      => $result,
        ];
    }
}
