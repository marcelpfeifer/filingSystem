<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.04.2021
 * Time: 15:06
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\User\Index;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Search extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $data \App\Models\Dto\User[]
         */
        $data = $this->resource;
        $result = [];

        foreach ($data as $entry) {
            $result[] = [
                'id'    => $entry->getId(),
                'name'  => $entry->getName(),
            ];
        }

        return [
            'result' => $result,
        ];
    }
}
