<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 29.04.2021
 * Time: 15:40
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\User\Create;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Admin\User\Create\Index
         */
        $dto = $this->resource;

        $permissionGroups = [];
        $groups = [];

        foreach ($dto->getPermissionGroups() as $entry) {
            $permissionGroups[] = [
                'id'   => $entry->getId(),
                'name' => $entry->getName(),
            ];
        }

        foreach ($dto->getGroups() as $entry) {
            $groups[] = [
                'id'   => $entry->getId(),
                'name' => $entry->getName(),
            ];
        }

        return [
            'permissionGroups' => $permissionGroups,
            'groups'           => $groups,
        ];
    }
}
