<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 06.05.2021
 * Time: 13:55
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\User\Edit;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Admin\User\Edit\Index
         */
        $dto = $this->resource;
        $permissionGroups = [];
        $groups = [];

        foreach ($dto->getPermissionGroups() as $entry) {
            $permissionGroups[] = [
                'id'   => $entry->getId(),
                'name' => $entry->getName(),
            ];
        }
        foreach ($dto->getGroups() as $entry) {
            $groups[] = [
                'id'   => $entry->getId(),
                'name' => $entry->getName(),
            ];
        }

        return [
            'userData'         => [
                'id'                => $dto->getUser()->getId(),
                'name'              => $dto->getUser()->getName(),
                'permissionGroupId' => $dto->getUser()->getPermissionGroupId(),
                'groupId'           => $dto->getUser()->getGroupId(),
            ],
            'permissionGroups' => $permissionGroups,
            'groups'           => $groups,
        ];
    }
}
