<?php
/**
 * Create
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 25.04.2021
 * Time: 11:58
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\Permission\Index;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Create extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        $group = [
            'id'   => $this->resource->getId(),
            'name' => $this->resource->getName(),
        ];

        $messages = [
            [
                'text'  => 'Gruppe wurde angelegt',
                'color' => 'green',
            ]
        ];

        return [
            'group'    => $group,
            'messages' => $messages,
        ];
    }
}
