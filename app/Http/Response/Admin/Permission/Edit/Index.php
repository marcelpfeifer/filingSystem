<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 25.04.2021
 * Time: 17:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\Permission\Edit;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Libary\View\Dto\Admin\Permission\Edit\Index\Permission;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Admin\Permission\Edit\Index
         */
        $dto = $this->resource;

        return [
            "group"               => [
                'id'   => $dto->getGroup()->getId(),
                'name' => $dto->getGroup()->getName(),
            ],
            "permissions"         => $this->buildPermissions($dto->getPermissions()),
            "selectedPermissions" => $dto->getSelectedPermissions(),
        ];
    }

    /**
     * @param Permission[] $permissions
     * @return array
     */
    private function buildPermissions(array $permissions): array
    {
        $result = [];
        foreach ($permissions as $permission) {
            $children = $this->buildPermissions($permission->getChildren());
            $result[] = [
                'id'          => $permission->getId(),
                'name'        => $permission->getName(),
                'description' => $permission->getDescription(),
                'children'    => $children,
            ];
        }
        return $result;
    }
}
