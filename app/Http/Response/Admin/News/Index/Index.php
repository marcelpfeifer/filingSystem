<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 05.06.2021
 * Time: 19:32
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\News\Index;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Admin\News\Index\Index
         */
        $dto = $this->resource;
        $news = [];

        foreach ($dto->getNews() as $entry) {
            $news[] = [
                'id'        => $entry->getId(),
                'title'     => $entry->getTitle(),
                'content'   => $entry->getContent(),
                'createdAt' => $entry->getCreatedAt()->format('Y-m-d H:i:s'),
                'updatedAt' => $entry->getUpdatedAt()->format('Y-m-d H:i:s'),
            ];
        }

        return [
            'news' => $news,
        ];
    }
}
