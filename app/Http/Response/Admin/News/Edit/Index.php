<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 07.06.2021
 * Time: 13:15
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\News\Edit;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Admin\News\Edit\Index
         */
        $dto = $this->resource;

        return [
            'news' => [
                'id'      => $dto->getNews()->getId(),
                'title'   => $dto->getNews()->getTitle(),
                'content' => htmlspecialchars_decode($dto->getNews()->getContent()),
            ]
        ];
    }
}
