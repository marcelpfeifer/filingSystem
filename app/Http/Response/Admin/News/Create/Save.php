<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 05.06.2021
 * Time: 19:38
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\News\Create;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Save extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        return [];
    }
}
