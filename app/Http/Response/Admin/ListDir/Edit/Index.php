<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.07.2021
 * Time: 16:18
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\ListDir\Edit;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Models\Dto\ListModel;

class Index extends AResponse implements IResponse
{
    public function getData(): array
    {
        /**
         * @var $data \App\Libary\View\Dto\Admin\ListDir\Edit\Index
         */
        $data = $this->resource;
        $headers = [];

        foreach ($data->getHeaders() as $entry) {
            $headers[] = [
                'id'    => $entry->getId(),
                'name'  => $entry->getName(),
                'list'  => $entry->isList(),
                'order' => $entry->getOrder(),
            ];
        }

        return [
            'id'      => $data->getListId(),
            'headers' => $headers,
        ];
    }
}
