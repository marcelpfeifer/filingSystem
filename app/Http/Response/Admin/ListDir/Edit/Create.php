<?php
/**
 * Create
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 26.07.2021
 * Time: 16:41
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\ListDir\Edit;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Models\Dto\ListHeader;

class Create extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $data ListHeader
         */
        $data = $this->resource;

        return [
            'entry' => [
                'id'    => $data->getId(),
                'name'  => $data->getName(),
                'list'  => $data->isList(),
                'order' => $data->getOrder(),
            ],
        ];
    }
}
