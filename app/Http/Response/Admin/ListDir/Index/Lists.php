<?php
/**
 * Groups
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 13:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\ListDir\Index;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Models\Dto\ListModel;

class Lists extends AResponse implements IResponse
{

    public function getData(): array
    {
        /**
         * @var $data ListModel[]
         */
        $data = $this->resource;
        $result = [];

        foreach ($data as $entry) {
            $result[] = [
                'id'      => $entry->getId(),
                'name'    => $entry->getName(),
            ];
        }

        return [
            'entries' => $result,
        ];
    }
}
