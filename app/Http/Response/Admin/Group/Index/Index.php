<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 13:23
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\Group\Index;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Index extends AResponse implements IResponse
{
    public function getData(): array
    {
        return [];
    }
}
