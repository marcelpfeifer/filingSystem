<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.07.2021
 * Time: 11:11
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\Tag\Index;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Models\Dto\Tag;

class Search extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $data Tag[]
         */
        $data = $this->resource;
        $result = [];

        foreach ($data as $entry) {
            $result[] = [
                'id'       => $entry->getId(),
                'name'     => $entry->getName(),
                'incident' => $entry->isIncident(),
                'profile'  => $entry->isProfile(),
            ];
        }

        return [
            'result'       => $result,
        ];
    }
}
