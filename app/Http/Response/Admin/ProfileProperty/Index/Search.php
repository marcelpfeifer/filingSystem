<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 10.05.2021
 * Time: 09:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\ProfileProperty\Index;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Search extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $data \App\Libary\View\Dto\Admin\ProfileProperty\Index\Search
         */
        $data = $this->resource;
        $result = [];
        foreach ($data->getProfileProperties() as $entry) {
            $result[] = [
                'id'   => $entry->getId(),
                'name' => $entry->getName(),
            ];
        }

        return [
            'result' => $result,
        ];
    }
}
