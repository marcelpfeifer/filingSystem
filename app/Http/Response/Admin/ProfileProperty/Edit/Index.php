<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 01:14
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\ProfileProperty\Edit;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $data \App\Libary\View\Dto\Admin\ProfileProperty\Edit\Index
         */
        $data = $this->resource;
        $groups = [];

        $entry = [
            'id'                     => $data->getProfileProperty()->getId(),
            'name'                   => $data->getProfileProperty()->getName(),
            'profilePropertyGroupId' => $data->getProfileProperty()->getProfilePropertyGroupId(),
        ];

        foreach ($data->getGroups() as $group) {
            $groups[] = [
                'id'   => $group->getId(),
                'name' => $group->getName(),
            ];
        }

        return [
            'entry'  => $entry,
            'groups' => $groups,
        ];
    }
}
