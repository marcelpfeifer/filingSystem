<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 11.05.2021
 * Time: 15:26
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\Charge\Create;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Models\Dto\ChargeGroup;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $data ChargeGroup[]
         */
        $data = $this->resource;
        $chargeGroups = [];
        foreach ($data as $group) {
            $chargeGroups[] = [
                'id'   => $group->getId(),
                'name' => $group->getName(),
            ];
        }

        return [
            'chargeGroups' => $chargeGroups,
        ];
    }
}
