<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.05.2021
 * Time: 01:14
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\Charge\Edit;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $data \App\Libary\View\Dto\Admin\Charge\Edit\Index
         */
        $data = $this->resource;
        $chargeGroups = [];

        $charge = [
            'id'            => $data->getCharge()->getId(),
            'name'          => $data->getCharge()->getName(),
            'description'   => $data->getCharge()->getDescription(),
            'chargeGroupId' => $data->getCharge()->getChargeGroupId(),
            'money'         => $data->getCharge()->getMoney(),
            'time'          => $data->getCharge()->getTime(),
        ];

        foreach ($data->getChargeGroups() as $group) {
            $chargeGroups[] = [
                'id'   => $group->getId(),
                'name' => $group->getName(),
            ];
        }

        return [
            'charge'       => $charge,
            'chargeGroups' => $chargeGroups,
        ];
    }
}
