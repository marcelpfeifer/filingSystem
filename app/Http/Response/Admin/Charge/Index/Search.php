<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 10.05.2021
 * Time: 09:03
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Admin\Charge\Index;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Search extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $data \App\Libary\View\Dto\Admin\Charge\Index\Search
         */
        $data = $this->resource;
        $charges = [];
        $chargeGroups = [];

        foreach ($data->getCharges() as $entry) {
            $charges[] = [
                'id'   => $entry->getId(),
                'name' => $entry->getName(),
            ];
        }

        foreach ($data->getChargeGroups() as $group) {
            $chargeGroup[] = [
                'id'   => $group->getId(),
                'name' => $group->getName(),
            ];
        }

        return [
            'result'      => $charges,
            'chargeGroups' => $chargeGroups,
        ];
    }
}
