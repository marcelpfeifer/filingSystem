<?php
/**
 * DeleteTag
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.07.2021
 * Time: 12:45
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Profile\Edit;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class DeleteTag extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        return [];
    }
}
