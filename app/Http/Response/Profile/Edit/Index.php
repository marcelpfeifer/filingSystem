<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 20.04.2021
 * Time: 13:22
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Profile\Edit;


namespace App\Http\Response\Profile\Edit;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Libary\View\Dto\Profile\Edit\Index\PropertyGroup;
use App\Models\Dto\Tag;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Profile\Edit\Index
         */
        $dto = $this->resource;

        return [
            'id'             => $dto->getProfile()->getId(),
            'name'           => $dto->getProfile()->getName(),
            'image'          => $dto->getProfile()->getImageUrl(),
            'description'    => $dto->getProfile()->getDescription(),
            'tags'           => $this->buildTags($dto->getTags()),
            'profileTags'    => $this->buildTags($dto->getProfileTags()),
            'propertyGroups' => $this->buildProperties($dto->getPropertyGroups()),
            'properties'     => $dto->getProperties(),
        ];
    }

    /**
     * @param Tag[] $tags
     * @return array
     */
    private function buildTags(array $tags): array
    {
        $result = [];
        foreach ($tags as $tag) {
            $result[] = [
                'id'   => $tag->getId(),
                'name' => $tag->getName(),
            ];
        }

        return $result;
    }

    /**
     * @param PropertyGroup[] $propertyGroups
     * @return array
     */
    private function buildProperties(array $propertyGroups): array
    {
        $result = [];
        foreach ($propertyGroups as $propertyGroup) {
            $properties = [];
            foreach ($propertyGroup->getProperties() as $property) {
                $properties[] = [
                    'id'   => $property->getId(),
                    'name' => $property->getName()
                ];
            }
            $result[] = [
                'id'         => $propertyGroup->getId(),
                'name'       => $propertyGroup->getName(),
                'properties' => $properties,
            ];
        }

        return $result;
    }
}
