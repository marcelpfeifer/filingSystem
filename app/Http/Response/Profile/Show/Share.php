<?php
/**
 * Share
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 22.05.2021
 * Time: 21:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Profile\Show;


use App\Http\Response\AResponse;
use App\Http\Response\Dto\Message;
use App\Http\Response\Enum\Message\Status;
use App\Http\Response\IResponse;

class Share extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $data \App\Libary\View\Dto\Profile\Show\Share
         */
        $data = $this->resource;

        $messages = [
            (new Message())
                ->setText($data->getCode())
                ->setStatus(Status::SUCCESS)
        ];

        $this->setMessages($messages);

        return [];
    }
}
