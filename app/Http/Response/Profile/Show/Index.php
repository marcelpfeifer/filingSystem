<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 12:51
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Profile\Show;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Libary\View\Dto\Profile\Show\Index\PropertyGroup;
use App\Models\Dto\Incident;
use App\Models\Dto\Tag;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Profile\Show\Index
         */
        $dto = $this->resource;

        $charges = [];

        foreach ($dto->getCharges() as $charge) {
            $charges[$charge->getId()] = [
                'id'    => $charge->getId(),
                'name'  => $charge->getName(),
                'count' => $charge->getCount(),
            ];
        }
        return [
            'id'              => $dto->getProfile()->getId(),
            'name'            => $dto->getProfile()->getName(),
            'image'           => $dto->getProfile()->getImageUrl(),
            'description'     => $dto->getProfile()->getDescription(),
            'charges'         => $charges,
            'tags'            => $this->buildTags($dto->getTags()),
            'propertyGroups'  => $this->buildProperties($dto->getPropertyGroups()),
            'latestIncidents' => $this->buildIncidents($dto->getLatestIncidents()),
        ];
    }

    /**
     * @param Incident[] $incidents
     * @return array
     */
    private function buildIncidents(array $incidents): array
    {
        $result = [];

        foreach ($incidents as $incident) {
            $result[] = [
                'id'    => $incident->getId(),
                'title' => $incident->getTitle(),
                'date'  => $this->formatDate($incident->getCreatedAt()),
            ];
        }
        return $result;
    }

    /**
     * @param PropertyGroup[] $propertyGroups
     * @return array
     */
    private function buildProperties(array $propertyGroups): array
    {
        $result = [];
        foreach ($propertyGroups as $propertyGroup) {
            $properties = [];
            foreach ($propertyGroup->getProperties() as $property) {
                $properties[] = [
                    'id'    => $property->getId(),
                    'name'  => $property->getName(),
                    'value' => $property->getValue(),
                ];
            }
            $result[] = [
                'id'         => $propertyGroup->getId(),
                'name'       => $propertyGroup->getName(),
                'properties' => $properties,
            ];
        }

        return $result;
    }

    /**
     * @param Tag[] $tags
     * @return array
     */
    private function buildTags(array $tags): array
    {
        $result = [];
        foreach ($tags as $tag) {
            $result[] = [
                'id'   => $tag->getId(),
                'name' => $tag->getName(),
            ];
        }

        return $result;
    }
}
