<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.04.2021
 * Time: 20:19
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Profile\Index;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Models\Dto\Profile;
use Illuminate\Database\Eloquent\Collection;

class Search extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $data Collection
         */
        $data = $this->resource;
        $profiles = [];

        foreach ($data as $profile) {
            $dto = \App\Models\Profile::getEntryAsDto($profile);
            $profiles[$dto->getId()] = [
                'id'    => $dto->getId(),
                'name'  => $dto->getName(),
                'image' => $dto->getImageUrl(),
                'tags'  => $this->buildTags($profile->tags),
            ];
        }

        return [
            'profiles' => $profiles,
        ];
    }

    /**
     * @param Collection $incidentToTags
     * @return array
     */
    private function buildTags(\Illuminate\Database\Eloquent\Collection $profileToTags)
    {
        $tags = [];
        foreach ($profileToTags as $profileToTag) {
            $dto = \App\Models\Tag::getEntryAsDto($profileToTag->tag);
            $tags[] = [
                'id'   => $dto->getId(),
                'name' => $dto->getName(),
            ];
        }
        return $tags;
    }
}
