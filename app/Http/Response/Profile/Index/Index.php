<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.04.2021
 * Time: 20:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Profile\Index;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Models\Dto\Profile;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Profile\Index\Index
         */
        $dto = $this->resource;

        $tags = [];
        foreach ($dto->getTags() as $tag) {
            $tags[] = [
                'id'   => $tag->getId(),
                'name' => $tag->getName(),
            ];
        }
        return [
            'tags' => $tags,
        ];
    }
}
