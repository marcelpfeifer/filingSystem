<?php
/**
 * Error
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.04.2021
 * Time: 15:40
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response;


class Error extends AResponse implements IResponse
{
    /**
     * @var int
     */
    protected $statusCode = 500;

    /**
     * @return array
     */
    public function getData(): array
    {
        $errors = [];
        foreach ($this->resource as $error) {
            $errors[] = [
                'text'  => $error,
                'color' => 'red',
            ];
        }

        return [
            'errors' => $errors
        ];
    }
}
