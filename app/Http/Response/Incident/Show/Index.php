<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 21.05.2021
 * Time: 13:07
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Incident\Show;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Libary\View\Dto\Incident\Show\Index\Profile\Charge;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Incident\Show\Index
         */
        $dto = $this->resource;

        $descriptions = [];
        foreach ($dto->getIncident()->getDescriptions() as $description) {
            $changelogs = [];
            foreach ($description->getChangelogs() as $changelog) {
                $changelogs[] = [
                    'user'      => $changelog->getUser(),
                    'action'    => $changelog->getAction(),
                    'timestamp' => $this->formatDate($changelog->getTimestamp()),
                ];
            }
            $descriptions[] = [
                'description' => htmlspecialchars_decode($description->getDescription(), ENT_QUOTES),
                'changelog'   => $changelogs,
            ];
        }

        $profiles = [];
        foreach ($dto->getProfiles() as $profile) {
            $charges = [];
            foreach ($profile->getCharges() as $charge) {
                $charges[] = $this->getCharge($charge);
            }
            $profiles[$profile->getId()] = [
                'id'      => $profile->getId(),
                'name'    => $profile->getName(),
                'image'   => $profile->getImage(),
                'warrant' => $profile->isWarrant(),
                'time'    => $profile->getTime(),
                'money'   => $profile->getMoney(),
                'charges' => $charges
            ];
        }

        $tags = [];
        foreach ($dto->getTags() as $tag) {
            $tags[] = [
                'id'   => $tag->getId(),
                'name' => $tag->getName(),
            ];
        }

        return [
            'incident' => [
                'id'           => $dto->getIncident()->getId(),
                'title'        => $dto->getIncident()->getTitle(),
                'descriptions' => $descriptions,
                'createdAt'    => $this->formatDate($dto->getIncident()->getCreatedAt()),
                'updatedAt'    => $this->formatDate($dto->getIncident()->getUpdatedAt()),
            ],
            'profiles' => $profiles,
            'tags'     => $tags
        ];
    }

    /**
     * @param Charge $charge
     * @return array
     */
    private function getCharge(Charge $charge): array
    {
        return [
            'id'    => $charge->getId(),
            'name'  => $charge->getName(),
            'count' => $charge->getCount(),
        ];
    }
}
