<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.05.2021
 * Time: 13:27
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Incident\Edit;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Models\Dto\Incident;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Incident\Edit\Index
         */
        $dto = $this->resource;

        $descriptions = [];
        foreach ($dto->getIncident()->getDescriptions() as $description) {
            $changelogs = [];
            foreach ($description->getChangelogs() as $changelog) {
                $changelogs[] = [
                    'user'      => $changelog->getUser(),
                    'action'    => $changelog->getAction(),
                    'timestamp' => $this->formatDate($changelog->getTimestamp()),
                ];
            }
            $descriptions[] = [
                'description' => $description->getDescription(),
                'changelog'   => $changelogs,
            ];
        }

        $profiles = [];
        foreach ($dto->getProfiles() as $profile) {
            $profiles[] = [
                'id'      => $profile->getId(),
                'name'    => $profile->getName(),
                'image'   => $profile->getImage(),
                'warrant' => $profile->hasWarrant(),
            ];
        }

        $tags = [];
        foreach ($dto->getTags() as $tag) {
            $tags[] = [
                'id'   => $tag->getId(),
                'name' => $tag->getName(),
            ];
        }

        $incidentTags = [];
        foreach ($dto->getIncidentTags() as $tag) {
            $incidentTags[] = [
                'id'   => $tag->getId(),
                'name' => $tag->getName(),
            ];
        }

        return [
            'incident'     => [
                'id'           => $dto->getIncident()->getId(),
                'title'        => $dto->getIncident()->getTitle(),
                'descriptions' => $descriptions,
            ],
            'profiles'     => $profiles,
            'tags'         => $tags,
            'incidentTags' => $incidentTags,
        ];
    }
}
