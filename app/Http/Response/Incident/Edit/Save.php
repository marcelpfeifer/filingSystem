<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.05.2021
 * Time: 14:08
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Incident\Edit;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Save extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $data \App\Libary\View\Dto\Incident\Edit\Save
         */
        $data = $this->resource;

        $this->setMessages($data->getMessages());

        return [];
    }
}
