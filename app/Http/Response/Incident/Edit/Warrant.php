<?php
/**
 * Warrant
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 18.05.2021
 * Time: 18:41
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Incident\Edit;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Warrant extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        return [];
    }
}
