<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.05.2021
 * Time: 13:15
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Incident\Index;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Incident\Index\Index
         */
        $dto = $this->resource;

        $tags = [];
        foreach ($dto->getTags() as $tag) {
            $tags[] = [
                'id'   => $tag->getId(),
                'name' => $tag->getName(),
            ];
        }
        return [
            'tags' => $tags,
        ];
    }
}
