<?php
/**
 * Search
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.05.2021
 * Time: 13:15
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Incident\Index;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Models\Dto\Incident;
use App\Models\Dto\Tag;
use App\Models\IncidentToTag;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class Search extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $data Collection
         */
        $data = $this->resource;
        $incidents = [];

        foreach ($data as $incident) {
            $dto = \App\Models\Incident::getEntryAsDto($incident);
            $incidents[] = [
                'id'        => $dto->getId(),
                'title'     => $dto->getTitle(),
                'tags'      => $this->buildTags($incident->tags),
                'createdAt' => $dto
                    ->getCreatedAt()
                    ->setTimezone('Europe/Berlin')
                    ->format('Y.m.d H:i:s'),

            ];
        }

        return [
            'result' => $incidents,
        ];
    }

    /**
     * @param Collection $incidentToTags
     * @return array
     */
    private function buildTags(\Illuminate\Database\Eloquent\Collection $incidentToTags)
    {
        $tags = [];
        foreach ($incidentToTags as $incidentToTag) {
            $dto = \App\Models\Tag::getEntryAsDto($incidentToTag->tag);
            $tags[] = [
                'id'   => $dto->getId(),
                'name' => $dto->getName(),
            ];
        }
        return $tags;
    }
}
