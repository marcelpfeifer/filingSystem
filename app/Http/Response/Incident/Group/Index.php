<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 08.07.2021
 * Time: 16:33
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Incident\Group;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Incident\Group\Index
         */
        $dto = $this->resource;

        $groups = [];
        foreach ($dto->getGroups() as $group) {
            $groups[] = [
                'id'   => $group->getId(),
                'name' => $group->getName(),
            ];
        }

        $selectedGroups = [];
        foreach ($dto->getSelectedGroups() as $group) {
            $selectedGroups[] = $group->getGroupId();
        }


        return [
            'incidentId'     => $dto->getIncidentId(),
            'groups'         => $groups,
            'selectedGroups' => $selectedGroups,
        ];
    }
}
