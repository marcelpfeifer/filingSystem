<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 18.05.2021
 * Time: 18:27
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Incident\Charge;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Save extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        return [];
    }
}

