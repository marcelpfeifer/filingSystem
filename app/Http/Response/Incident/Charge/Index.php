<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 17.05.2021
 * Time: 14:36
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Incident\Charge;

use App\Http\Response\AResponse;
use App\Http\Response\IResponse;
use App\Models\Dto\Charge;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\Incident\Charge\Index
         */
        $dto = $this->resource;

        $charges = [];
        foreach ($dto->getCharges() as $charge) {
            $chargesAsArray = [];
            foreach ($charge->getCharges() as $singleCharge) {
                if (isset($chargesAsArray[$singleCharge->getId()])) {
                    $chargesAsArray[$singleCharge->getId()]['count']++;
                } else {
                    $chargesAsArray[$singleCharge->getId()] = $this->getCharge($singleCharge);
                }
            }
            $charges[] = [
                'category' => $charge->getCategory(),
                'charges'  => $chargesAsArray
            ];
        }

        $selectedCharges = [];
        foreach ($dto->getSelectedCharges() as $charge) {
            $selectedCharges[] = $this->getCharge($charge);
        }

        return [
            'incidentId'      => $dto->getIncidentId(),
            'profileId'       => $dto->getProfileId(),
            'charges'         => $charges,
            'selectedCharges' => $selectedCharges,
        ];
    }

    /**
     * @param Charge $charge
     * @return array
     */
    private function getCharge(Charge $charge): array
    {
        return [
            'id'          => $charge->getId(),
            'name'        => $charge->getName(),
            'description' => $charge->getDescription(),
            'time'        => $charge->getTime(),
            'money'       => $charge->getMoney(),
        ];
    }
}
