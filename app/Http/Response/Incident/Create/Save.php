<?php
/**
 * Save
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 15.05.2021
 * Time: 13:37
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\Incident\Create;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Save extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $data \App\Libary\View\Dto\Incident\Create\Save
         */
        $data = $this->resource;

        $this->setMessages($data->getMessages());

        $descriptions = [];
        foreach ($data->getIncident()->getDescriptions() as $description) {
            $changelogs = [];
            foreach ($description->getChangelogs() as $changelog) {
                $changelogs[] = [
                    'user'      => $changelog->getUser(),
                    'action'    => $changelog->getAction(),
                    'timestamp' => $this->formatDate($changelog->getTimestamp()),
                ];
            }
            $descriptions[] = [
                'description' => $description->getDescription(),
                'changelog'   => $changelogs,
            ];
        }

        return [
            'incident' => [
                'id'           => $data->getIncident()->getId(),
                'title'        => $data->getIncident()->getTitle(),
                'descriptions' => $descriptions,
            ],
        ];
    }
}
