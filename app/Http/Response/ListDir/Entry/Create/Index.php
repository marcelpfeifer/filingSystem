<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.07.2021
 * Time: 13:07
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\ListDir\Entry\Create;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\ListDir\Entry\Create\Index
         */
        $dto = $this->resource;

        $headers = [];

        foreach ($dto->getHeaders() as $header) {
            $headers[] = [
                'id'   => $header->getId(),
                'name' => $header->getName(),
            ];
        }
        return [
            'listId'  => $dto->getListId(),
            'headers' => $headers,
        ];
    }
}
