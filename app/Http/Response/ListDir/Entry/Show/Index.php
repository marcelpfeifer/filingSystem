<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 28.07.2021
 * Time: 13:49
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\ListDir\Entry\Show;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\ListDir\Entry\Show\Index
         */
        $dto = $this->resource;

        $values = [];

        foreach ($dto->getValues() as $value) {
            $values[] = [
                'id'    => $value->getId(),
                'name'  => $value->getName(),
                'value' => $value->getValue(),
            ];
        }
        return [
            'entryId' => $dto->getEntryId(),
            'listId'  => $dto->getListId(),
            'values' => $values,
        ];
    }
}
