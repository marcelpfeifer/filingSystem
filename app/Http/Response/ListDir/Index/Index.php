<?php
/**
 * Index
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 27.07.2021
 * Time: 14:48
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response\ListDir\Index;


use App\Http\Response\AResponse;
use App\Http\Response\IResponse;

class Index extends AResponse implements IResponse
{
    /**
     * @return array
     */
    public function getData(): array
    {
        /**
         * @var $dto \App\Libary\View\Dto\ListDir\Index\Index
         */
        $dto = $this->resource;

        $lists = [];
        foreach ($dto->getLists() as $entry) {
            $lists[] = [
                'id'   => $entry->getId(),
                'name' => $entry->getName(),
            ];
        }

        return [
            'lists' => $lists
        ];
    }
}
