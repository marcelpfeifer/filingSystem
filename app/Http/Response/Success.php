<?php
/**
 * Success
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 19.04.2021
 * Time: 15:42
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response;

class Success extends AResponse implements IResponse
{
    /**
     * @var int
     */
    protected $statusCode = 200;

    /**
     * @return array
     */
    public function getData(): array
    {
        $messages = [];
        foreach ($this->resource as $message) {
            $messages[] = [
                'text'  => $message,
                'color' => 'green',
            ];
        }

        return [
            'messages' => $messages
        ];
    }
}
