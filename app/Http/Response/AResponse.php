<?php
/**
 * AResponse
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 14.04.2021
 * Time: 20:17
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response;

use App\Http\Response\Dto\Message;
use App\Http\Response\Mapping\Message\StatusToColor;
use Carbon\Carbon;
use Inertia\Inertia;

abstract class AResponse implements IResponse
{
    /**
     * @var mixed
     */
    protected $resource;

    /**
     * @var int
     */
    protected $statusCode = 200;

    /**
     * @var Message[]
     */
    private $messages = [];

    /**
     * AResponse constructor.
     * @param $resource
     */
    public function __construct($resource = null)
    {
        $this->resource = $resource;
    }

    /**
     * @return Message[]
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * @param Message[] $messages
     * @return AResponse
     */
    public function setMessages(array $messages): AResponse
    {
        $this->messages = $messages;
        return $this;
    }

    /**
     * @return array
     */
    private function getResponseData(): array
    {
        $data = $this->getData();
        $messages = $this->getMessageData();
        if (count($messages) !== 0) {
            $data['messages'] = $messages;
        }
        return $data;
    }

    /**
     * @return array
     */
    private function getMessageData(): array
    {
        $messages = [];
        foreach ($this->getMessages() as $message) {
            $messages[] = [
                'text'  => $message->getText(),
                'color' => StatusToColor::map($message->getStatus()),
            ];
        }

        return $messages;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function json(): \Illuminate\Http\JsonResponse
    {
        return response()->json($this->getResponseData(), $this->statusCode);
    }

    /**
     * @param string $componentName
     * @return \Inertia\Response
     */
    public function inertia(string $componentName): \Inertia\Response
    {
        return Inertia::render(
            $componentName,
            $this->getResponseData()
        );
    }

    /**
     * @param Carbon $carbon
     * @return string
     */
    public function formatDate(Carbon $carbon): string
    {
        return $carbon->setTimezone('Europe/Berlin')->format('Y-m-d H:i:s');
    }
}
