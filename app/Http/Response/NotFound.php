<?php
/**
 * NotFound
 * Copyright (C) Marcel Pfeifer 2021 <webmaster@m-pfeifer.de>
 * Date: 20.04.2021
 * Time: 13:20
 *
 *    ___
 * __/_  `.  .-"""-.
 * \_,` | \-'  /   )`-')
 *  "") `"`    \  ((`"`
 *  ___Y  ,    .'7 /|
 * (_,___/...-` (_/_/
 */

namespace App\Http\Response;

class NotFound extends AResponse implements IResponse
{
    /**
     * @var int
     */
    protected $statusCode = 404;

    /**
     * @return array
     */
    public function getData(): array
    {
        return [];
    }
}
